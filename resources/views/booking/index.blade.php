
@extends('layouts.app')

@section('title')
List Booking
@endsection

@section('content')

<div class="container-fluid mt-5">
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">Booking</h3>
            </div>
            <div class="col-4 text-right">
              <a href="{{ route('admin.booking.create')}}" class="btn btn-sm btn-default">Add Booking</a>
            </div>
          </div>
        </div>

        <div class="col-12">
          @if (session('status'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div>

        <div class="table-responsive pb-4">
          <table class="table align-items-center table-flush" id="datatable-basic">
            <thead class="thead-light">
              <tr>
                <th scope="col">S.N</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Date</th>
                <th scope="col">Time</th>
                <th scope="col">Status</th>
                <th scope="col">action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($bookings as $value)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$value['name']}}</td>
                <td>{{$value['email']}}</td>
                <td>{{$value['phone']}}</td>
                <td>{{$value['date']}}</td>
                <td>{{$value['time']}}</td>
                <td><span style="badge badge-pill badge-default">{{$value['status']}}</span></td>
                <td>
                  <a class="btn btn-sm btn-default px-1" href="{{ route('admin.booking.edit',$value['id'])}}"><i class="fas fa-edit"></i></a>
                  <a class="btn btn-sm btn-danger px-1" href="{{ route('admin.booking.destroy',$value['id'])}}"><i class="fas fa-trash-alt"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
