@extends('layouts.app')

@section('title')
Create Booking
@endsection

@section('content')

<div class="container-fluid mt-5">
  <div class="row">
    <div class="col-xl-12 order-xl-1">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">Create Booking</h3>
            </div>
            <div class="col-4 text-right">
              <a href="{{ route('admin.booking.index') }}" class="btn btn-sm btn-default">{{ __('Back to list') }}</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form method="post" action="{{ route('admin.booking.store') }}" autocomplete="on">
            @csrf

            <h6 class="heading-small text-muted mb-4">{{ __('Add New Booking') }}</h6>
            <div class="pl-lg-4">
              <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="name">Name</label>
                <input rows="3" type="text" name="name" id="name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('name') }}" value="{{ old('name') }}">

                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="email">Email</label>
                <input rows="3" type="text" name="email" id="email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('email') }}" value="{{ old('email') }}">

                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="phone">phone</label>
                <input rows="3" type="text" name="phone" id="phone" class="form-control form-control-alternative{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="{{ __('phone') }}" value="{{ old('phone') }}">

                @if ($errors->has('phone'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('date') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="date">Date</label>
                <input type="date" name="date" id="date" class="form-control form-control-alternative{{ $errors->has('date') ? ' is-invalid' : '' }}" placeholder="{{ __('Date') }}" value="{{ old('date') }}">

                @if ($errors->has('date'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('date') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('time') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="input-time">Select Category</label>
                <select type="text" name="time" id="time" class="select2-single form-control form-control-alternative{{ $errors->has('time') ? ' is-invalid' : '' }}" >
                  
                  <option class="form-control" value="06:00-08:00 AM" @if( old('time') == '06:00-08:00 AM') selected @endif>06:00-08:00 AM</option>
                  <option class="form-control" value="08:00-10:00 AM" @if( old('time') == '08:00-10:00 AM') selected @endif>08:00-10:00 AM</option>
                  <option class="form-control" value="10:00-12:00 AM" @if( old('time') == '10:00-12:00 AM') selected @endif>10:00-12:00 AM</option>
                  <option class="form-control" value="12:00-02:00 PM" @if( old('time') == '12:00-02:00 PM') selected @endif>12:00-02:00 PM</option>
                  <option class="form-control" value="02:00-04:00 PM" @if( old('time') == '02:00-04:00 PM') selected @endif>02:00-04:00 PM</option>
                  <option class="form-control" value="04:00-08:00 PM" @if( old('time') == '04:00-06:00 PM') selected @endif>04:00-06:00 PM</option>
                  
                </select>

                @if ($errors->has('time'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('time') }}</strong>
                </span>
                @endif
              </div>
              
              <div class="text-center">
                <button type="submit" class="btn btn-success mt-4">Create</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
