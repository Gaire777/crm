<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
            <!-- sidebar -->
            <div class="sidebar">
               <!-- sidebar menu -->
               <ul class="sidebar-menu">
                  <li class="active">
                     <a href="{{route('home')}}"><i class="fa fa-tachometer"></i><span>Dashboard</span>
                     <span class="pull-right-container">
                     </span>
                     </a>
                  </li>

                  <li class="treeview">
                     <a href="">
                     <i class="fa fa-id-card-o" aria-hidden="true" ></i>
                     <span>Services</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>
                     <ul class="treeview-menu">
                        <li><a href="{{route('admin.service.list')}}">Services List</a></li>
                        <li><a href="{{route('admin.service.form')}}">New Services</a></li>
                     </ul>
                  </li>

                  <li class="treeview">
                     <a href="index.html#">
                     <i class="fa fa-list" aria-hidden="true" ></i>
                      <span>Category</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>
                     <ul class="treeview-menu">
                        <li><a href="{{route('admin.categorylist')}}">Category List</a></li>
                        <li><a href="{{route('admin.category.form')}}">New Category</a></li>
                     </ul>
                  </li>


                  <li class="treeview">
                     <a href="index.html#">
                     <i class="fa fa-calendar-check-o" aria-hidden="true" ></i>
                     <span>Accounts</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>
                     <ul class="treeview-menu">
                        <li><a href="{{route('admin.cashin.list')}}">Cashins</a></li>
                        <li><a href="{{route('admin.cashout.list')}}">Cashouts</a></li>
                        <li><a href="{{route('admin.balances.browse')}}">Balances</a></li>

                     </ul>
                  </li>


                  <li>
                     <a href="{{route('admin.invoice.list')}}">
                     <i class="fa fa-usd" aria-hidden="true" ></i>
                     <span>Invoices</span>
                     <span class="pull-right-container">
                     </span>
                     </a>
                  </li>

                   <li>
                       <a href="{{route('viewmanpowerlist')}}">
                           <i class="fa fa-usd" aria-hidden="true" ></i>
                           <span>ManPower Management</span>
                           <span class="pull-right-container">
                     </span>
                       </a>
                   </li>

                  <li class="treeview">
                     <a href="index.html#">
                     <i class="fa fa-commenting-o" aria-hidden="true" ></i>
                     <span>Enquiries</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>
                     <ul class="treeview-menu">
                     <li><a href="{{route('admin.enquiries.list')}}">Enquiries List</a></li>
                        <li><a href="{{route('admin.enquiries.form')}}">New Enquiry</a></li>

                     </ul>
                  </li>

                  <li class="treeview">
                     <a href="index.html#">
                     <i class="fa fa-calendar-check-o" aria-hidden="true" ></i>
                     <span>Applicants</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>
                     <ul class="treeview-menu">
                     <li><a href="{{route('admin.applicant.list')}}">Applicants</a></li>
                        <li><a href="{{route('admin.checklist.list')}}">Document Checklist</a></li>
                        <li><a href="{{route('admin.education.list')}}">Education</a></li>
                        <li><a href="{{route('admin.healthlicense.list')}}">Health License</a></li>
                        <li><a href="{{route('admin.employment.list')}}">Employment</a></li>
                        <li><a href="{{route('admin.progressflow.list')}}">Progress Flow Report</a></li>


                     </ul>
                  </li>

                  <li class="treeview">
                     <a href="index.html#">
                     <i class="fa fa-calendar-check-o" aria-hidden="true"  ></i>
                     <span>Booking</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>
                     <ul class="treeview-menu">
                     <li><a href="{{route('admin.booking.index')}}">Booking List</a></li>
                        <li><a href="{{route('admin.booking.create')}}">New Booking</a></li>

                     </ul>
                  </li>


               </ul>
            </div>
            <!-- /.sidebar -->
         </aside>
         <!-- =============================================== -->
