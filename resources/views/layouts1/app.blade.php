<!DOCTYPE html>
<html lang="en">

@include('layouts1.header1.head1')

<body class="hold-transition sidebar-mini">
      <!--preloader-->
      <div id="preloader">
         <div id="status"></div>
      </div>
      <!-- Site wrapper -->
      <div class="wrapper">
         <header class="main-header">
            <a href="index.html" class="logo">
               <!-- Logo -->
               <span class="logo-mini">
               <img src="{{asset('public/assets/dist/img/mini-logo.png')}}" alt="">
               </span>
               <span class="logo-lg">
               <img src="{{asset('public/assets/dist/img/logo1.png')}}" alt="">
               </span>
            </a>
        @include('layouts1.navs1.navbar1')

        </header>
         <!-- =============================================== -->
       @include('layouts1.navs1.sidebar1')
         <!-- =============================================== -->
         <!-- Content Wrapper. Contains page content -->


          @yield('content')

         <!-- /.content-wrapper -->
   @include('layouts1.footers1.scripts1')
   
   </body>
</html>

