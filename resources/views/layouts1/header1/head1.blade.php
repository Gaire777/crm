<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>GCN | @yield('title')</title>
      <!-- Favicon and touch icons -->
      <link rel="shortcut icon" href="{{asset('public/assets/dist/img/ico/favicon.png.jpeg')}}" type="image/x-icon">
      <!-- Start Global Mandatory Style
         =====================================================================-->
      <!-- jquery-ui css -->
      <link href="{{asset('public/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Bootstrap -->
      <link href="{{asset('public/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Bootstrap rtl -->
      <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
      <!-- Lobipanel css -->
      <link href="{{asset('public/assets/plugins/lobipanel/lobipanel.min.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Pace css -->
      <link href="{{asset('public/assets/plugins/pace/flash.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Font Awesome -->
      <link href="{{asset('public/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Pe-icon -->
      <link href="{{asset('public/assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Themify icons -->
      <link href="{{asset('public/assets/themify-icons/themify-icons.css')}}" rel="stylesheet" type="text/css"/>
      <!-- End Global Mandatory Style
         =====================================================================-->
      <!-- Start page Label Plugins
         =====================================================================-->
      <!-- Emojionearea -->
      <link href="{{asset('public/assets/plugins/emojionearea/emojionearea.min.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Monthly css -->
      <link href="{{asset('public/assets/plugins/monthly/monthly.css')}}" rel="stylesheet" type="text/css"/>
      <!-- End page Label Plugins
         ============================`=========================================-->
      <!-- Start Theme Layout Style
         =====================================================================-->
      <!-- Theme style -->
      <link href="{{asset('public/assets/dist/css/stylecrm.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Theme style rtl -->
      <!--<link href="assets/dist/css/stylecrm-rtl.css" rel="stylesheet" type="text/css"/>-->
      <!-- End Theme Layout Style
         =====================================================================-->
@yield('css')

    <!--===============================Customized css Start======================================-->

    <style type="text/css">

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;

            color: #212529;
            background-color: #fff;
        }
    </style>

    <style type="text/css">
            .fa{
                color: white;
            }



        .main-sidebar  {

            background-image:
                linear-gradient(to bottom, rgba(35, 204, 239, 0.9), rgba(35, 204, 239, 0.5)),
                url("{{asset('public/assets/dist/img/ico/sidebar.jpg')}}");
            background-size: cover;
            border: 0;
            position: fixed;


        }
        .sidebar-menu .treeview-menu {

            background-color: #2a313538;

        }

        .sidebar-menu > li.active > a {
            color: #fff;
            background:rgba(255, 255, 255, 0.13);
        }
        .main-header .navbar {
            background-image:
                linear-gradient(to right, rgba(35, 204, 239, 0.90), rgba(35, 204, 239, 0.73));
            border: 0;



        }


        .btn-add {
            background: #6cabd8;
            color: #fbfbfb !important;

        }
        .panel-bd > .panel-heading {
            color: #010611;
            background-color: #6cabd8;

        }
        a.logo img {
            height: 100%;
            width: 100%;
            border: 0;
        }
        .main-header .logo {
            background: rgba(40, 197, 231, 0.90);
            border-bottom: 0;
            position: fixed;
            top: 0;
        }

        .pe-7s-angle-left-circle:before {

            color: white;
        }
        .header-icon .fa{
            color: rgba(35, 204, 239, 0.9);
        }

        .sidebar-menu > li:hover > a,
        .sidebar-menu > li.active > a {
            color: #fff;
            background:#1e93d4;
        }


        .btn-add {
            background: #6cabd8;

            border: 0;
            width: 100%;
        }
        .btn {

            text-align: left;
        }

            .lobipanel>.panel-heading .dropdown .dropdown-menu>li>a .control-title {
                display: none;
                margin-left: 15px;
            }

            .ti-close:before {
                content: "\e646";
                display: none;
            }
    </style>






    <!--===============================Customized css Start======================================-->





   </head>
