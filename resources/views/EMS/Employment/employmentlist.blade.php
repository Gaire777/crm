@extends('layouts.app')
@section('title')
Employment List
@endsection

@section('css')

<style type="text/css">
	.employment_table {
		overflow: hidden;
		overflow-x: scroll;

	}
</style>

@endsection


@section('page_heading')
<h1>
	Employments <small>Employment listing</small>
</h1>

@endsection

@section('button')

<a href="{{route('admin.employment.add')}}"><button class="btn btn-success btn-sm ">Add Employment</button></a>

@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="employment_table table-responsive">
			<table id="datatable-basic" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="example1_info" style="width: 1365px;">

				<thead>

					<tr class="success" role="row">
						<th>S.N</th>
						<th>applicant_id</th>
						<th>
							issuing_authority_name </th>
						<th>
							issuing_authority_address </th>
						<th>
							issuing_authority_country </th>
						<th>
							issuing_authority_state </th>
						<th>
							issuing_authority_city </th>
						<th>
							issuing_authority_country_code </th>
						<th>
							issuing_authority_phone </th>
						<th>
							reason_for_leaving </th>
						<th>
							issuing_authority_email </th>
						<th>
							issuing_authority_website </th>
						<th>
							nature_of_employment </th>
						<th>
							employment_from </th>
						<th>
							employment_to </th>
						<th>
							designation </th>
						<th>
							employee_code </th>
						<th>
							department </th>
						<th>
							experience_letter </th>
						<th>
							Action </th>


					</tr>
				</thead>
				<tbody>
					@foreach($employment as $value)


					<tr role="row" class="odd">
						<td class="sorting_1">{{$loop->iteration}}</td>
						<td>{{$value->applicants->first_name}} {{$value->applicants->surname}}</a></td>
						<td>{{$value->issuing_authority_name}}</td>
						<td>{{$value->issuing_authority_address}}</td>
						<td>{{$value->issuing_authority_country}}</td>
						<td>{{$value->issuing_authority_state}}</td>
						<td>{{$value->issuing_authority_city}}</td>
						<td>{{$value->issuing_authority_country_code}}</td>
						<td>{{$value->issuing_authority_phone}}</td>
						<td>{{$value->reason_for_leaving}}</td>
						<td>{{$value->issuing_authority_email}}</td>
						<td>{{$value->issuing_authority_website}}</td>
						<td>{{$value->nature_of_employment}}</td>
						<td>{{$value->employment_from}}</td>
						<td>{{$value->employment_to}}</td>
						<td>{{$value->designation}}</td>
						<td>{{$value->employee_code}}</td>
						<td>{{$value->department}}</td>
						<td>
							@if($value->experience_letter != null)
							<a href="{{url('/storage/app/public/'.$value->experience_letter)}}" target="_blank">Passport
								@else
								No file found
								@endif
						</td>


						<td><span>
								<a href="{{route('admin.employment.edit',$value->id)}}">Edit</a></span> ||
							<span>
								<a href="{{route('admin.employment.delete',$value->id)}}">Delete</a>
							</span>
					</tr>
					@endforeach
			</table>
		</div>
	</div>
	@endsection


	@section('js')



	@endsection