@extends('layouts.app')
@section('title')
Employment Edit
@endsection
@section('content')
<div class="container">
	<form method="POST" action="{{route('admin.employment.edited')}}" enctype="multipart/form-data" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
		@csrf
		<div class="modal-body">
			<div class="box-body">
				<input type="hidden" name="id" value="{{$employment->id}}">

				<div class="form-group"><label for="applicant_category">applicants_name

					</label>
					<select class="form-control" data-placeholder="Enter Applicant's type" rel="select2" name="applicant_id">

						@foreach($applicant as $value)

						<option value="{{$value->id}}" class="form-control">{{$value->first_name}} {{$value->surname}}

						</option>{{$value->first_name}}


						@endforeach



					</select>
				</div>

				<div class="form-group"><label for="applicant_category">designation </label>
					<select class="form-control" data-placeholder="Enter Applicant's type" rel="select2" name="designation">
						<option value="doctor" class="form-control">Mr.</option>
						<option value="nurse" class="form-control">Mrs.</option>
						<option value="nurse" class="form-control">Miss</option>
					</select>
				</div>

				<div class="form-group"><label for="nature_of_employment">nature_of_employment </label>
					<select class="form-control" data-placeholder="Enter Applicant's Category" rel="select2" name="nature_of_employment">
						<option value="Diploma" class="form-control">Full Time</option>
						<option value="Bachelor" class="form-control">Part Time</option>
						<option value="Master" class="form-control">Online</option>
						<select>
				</div>

				<div class="form-group"><label for="issuing_authority_country">issuing_authority_country </label>
					<select class="form-control" data-placeholder="Enter authority_country" rel="select2" name="issuing_authority_country">
						<option value="Bangladesh" class="form-control">Bangladesh</option>
						<option value="Nepal" class="form-control">Nepal</option>
						<option value="India" class="form-control">India</option>
						<option value="China" class="form-control">China</option>
						<option value="Australia" class="form-control">Australia</option>
					</select>
				</div>

				authority_name<input class="form-control" value="" type="text" name="issuing_authority_name">
				authority_address <input class="form-control" value="" type="text" name="issuing_authority_address">
				authority_city<input class="form-control" value="" type="text" name="issuing_authority_city">
				authority_state <input class="form-control" value=" " type="text" name="issuing_authority_state">

				authority_country_code <input class="form-control" value=" " type="text" name="issuing_authority_country_code">
				authority_phone <input class="form-control" value="" type="text" name="issuing_authority_phone">
				authority_email<input class="form-control" value="authority_email" type="email" name="authority_email">
				authority_website <input class="form-control" value="" type="text" name="issuing_authority_website">


				reason_for_leaving <input class="form-control" value="reason_for_leaving" type="text" name="reason_for_leaving">



				<!-- 					<div class="form-group"><label for="applicant_category">mode </label>
						<select class="form-control" data-placeholder="Enter Applicant's Mode" rel="select2" name="mode" >
							<option value="Active" class="form-control">Active Enrollment</option>
							<option value="Online" class="form-control">Online Enrollment</option>
						</select>
					</div> -->


				employment_from <input class="form-control" value="" name="employment_from" type="date">
				employment_to <input class="form-control" value="" name="employment_to" type="date">
				employee_code<input class="form-control" value="" name="employee_code" type="text">

				experience_letter <input class="form-control" id="up1" name="experience_letter" type="file">
				<a class="uploaded_file hide" onclick="document.getElementById('up1').value = '' "><i title="Remove File" class="fa fa-times" class=""></i></a><br>
			</div>

		</div>

		<div class="modal-footer">

			<input class="btn btn-success" type="submit" value="Submit">
		</div>

	</form>
</div>
@endsection