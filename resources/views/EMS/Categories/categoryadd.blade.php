@extends('layouts.app')
@section('title')
Add Category
@endsection
@section('content')
<div class="container">
	 @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
<form method="POST" action="{{route('admin.category.add')}}" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
	
                    <div class="form-group"><label for="fname">Category :</label>
                    	<input class="form-control" placeholder="Enter Category name " data-rule-maxlength="256" required="1" name="name" type="text" value="{{ old('name') }}" aria-required="true"></div>
                    	


					
					
			</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection