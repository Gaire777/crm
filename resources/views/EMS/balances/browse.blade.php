@extends('layouts.app')
@section('title')
Balances
@endsection

@section('page_heading')
            <h1>Balance Report</h1>
@endsection
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<section class="col-lg-12 connectedSortable">
    <div class="box box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <form method="POST" action="{{ route('admin.balances.report')}}">
                        @csrf
                        <label for="date1">From: <br>
                            <input type="date" name="date1" id="">
                        </label><br>
                        <label for="date2">To: <br>
                            <input type="date" name="date2" id="">
                        </label> <br>
                        <input class="btn btn-success" type="submit" value="Report">
                    </form>
                </div>
                <div class="col-md-6">
                    <table class="table ">
                        <thead>
                            <tr class="page-header ">
                                <th>Total</th>
                            </tr>
                            <tr class="alert ">
                                <th> Total Cash In</th>
                                <th>{{$cashins}}</th>


                            </tr>
                            <tr class="alert ">
                                <th> Total Cash out</th>
                                <th>{{$cashouts}}</th>
                            </tr>

                            <tr class="alert alert">
                                <th>Net Profit/Loss</th>
                                <th>{{$net_sum}}</th>

                            </tr>

                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-hover">
                        <thead>
                            <tr class="alert alert-success">
                                <th>Cash In Records</th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach($services as $service)

                            <tr>
                                <th>{{$service->name}}&nbsp&nbsp<span class="pull-right"> {{$service->service_sum_cashin}}</span></th>
                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-hover ">
                        <thead>
                            <tr class="alert alert-danger">
                                <th>Cash Out Records</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($expenditures as $expenditure)
                            <tr>
                                <th>Total Cash Out In {{$expenditure->expendituretype}} &nbsp&nbsp <span class="pull-right">{{$expenditure->expendituretype_sum_chashout}}</span></th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>




@endsection