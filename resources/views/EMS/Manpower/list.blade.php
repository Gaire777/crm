@extends('layouts1.app')


@section('title','Personnel List')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="header-icon">
                <i class="fa fa-users"></i>
            </div>
            <div class="header-title">
                <h1>Service </h1>
                <small>Service List</small>
            </div>
        </section>
    <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                                 <h4>Personnel List</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                            <divn class="row">
                           <div class=" col-md-6 col-6 col-sm-12">
                              <div class="buttonexport btn-group" id="buttonlist">
                                 <a class="btn btn-add" href="{{route('manpowerform')}}"> <i class="fa fa-plus"></i> New Personnel
                                 </a>
                              </div>
                           </div>
                                <div class="col-md-6 col-6 col-sm-12">

                                        <input type="text" class="form-control" placeholder="Search.vvv." name="search2"> <span>
                                        <button onclick="search()"><i class="fa fa-search"></i></button></span>

                                </div>
                            </divn>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Id</th>
                                       <th>Name</th>
                                       <th>Country</th>
                                        <th>Position Applied</th>
                                       <th>Action</th>

                                    </tr>
                                 </thead>
                                 <tbody>




                                 @foreach($personnel as $value)
                                    <tr>
                                       <td>{{$loop->iteration}}</td>

                                       <td>{{$value->name}}</td>
                                        <td>{{$value->finaldetails->country}}</td>

                                       <td>{{$value->finaldetails->position_applied }}</td>
                                       <td>
                                           <a href="{{route('manpoweredit',$value->id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil" style="color: black"></i></a>

                                           <a href="{{route('deletemanpower',$value->id)}}" class="btn btn-danger btn-xs deleteRecord" >
                                          <i class="fa fa-trash-o "></i>
                                          </a>
                                       </td>
                                    </tr>
                                @endforeach

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </section>
    </div>

@endsection

@section('scripts1')


    <script>
     $('#searchq').on('keyup',function () {
            var search = $("#searchq").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{ route('ajaxsearch') }}',
                data: {search: search},
            })
        });
    </script>







    </script>

<script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif

        @if(session('flash_error'))
        swal("Error", "{!! session('flash_error') !!}")
        @endif
    </script>

@endsection
