
@extends('layouts.app')
@section('title')
 Edit
@endsection

@section('content')


<form role="form" class="form-edit-add" action="{{route('admin.cashout.edit')}}" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        
                        @csrf

                        <div class="panel-body">

                            
                            <!-- Adding / Editing -->
                            
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                         <input type="hidden" name="id" value="{{$cashouts->id}}">                       
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Expenditure Type</label>
                                    <select class="form-control select2" name="expendituretype" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option value=" {{$cashouts->expendituretype}}">select</option>
                                            <option value="Basic life support" data-select2-id="3">Basic life support</option>

                            <option value="Internet">Internet</option>
                            <option value="Domain">Domain</option>
                            <option value="Rent">Rent</option>
                            <option value="Hosting">Hosting</option>
                            <option value="Electricity and Garbage">Electricity and Garbage</option>
                            <option value="Book Printing">Book Printing</option>
                            <option value="Advertising and Marketing">Advertising and Marketing</option>
                            <option value="Cancellation/Refund">Cancellation/Refund</option>
                            <option value="Telephone">Telephone</option>
                            <option value="Stationery">Stationery</option>
                            <option value="Lunch">Lunch</option>
                            <option value="Snacks and Tea">Snacks and Tea</option>
                            <option value="Staff Salary">Staff Salary</option>
                            <option value="Fuel and Travel Expenses">Fuel Travel Expenses</option>
                            <option value="Repair and Maintenance">Repair amd Maintenance</option>
                            <option value="Loan Payment">Loan Payment</option>
                            <option value="Basic Life Support">Basic Life Support</option>
                            <option value="Others">Others</option>
                        </select><span class="select2  " dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Amount</label>
                                                                 <input type="text" class="form-control" name="amount" placeholder="Amount" value="{{$cashouts->amount}}">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Withdrawn By</label>
                                        <select class="form-control " name="applicant_id" data-get-items-route="http://oms.prometricexamnepal.com/admin/cashouts/relation" data-get-items-field="cashout_belongsto_user_relationship" data-method="edit" data-select2-id="4" tabindex="-1" aria-hidden="true">
                                        <option value="{{$cashouts->withdrawnby}}">select</option>
                                     @foreach($applicants as $value)
                                   <option value= "{{$value->id}}">{{$value->first_name}} {{$value->surname}}</option>
                                   @endforeach

                    
                                    </select><span class="select2 " dir="ltr" data-select2-id="6" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

            
        
    
                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Date</label>
                                                                                                                <input type="date" class="form-control" name="date" placeholder="Date" value="{{$cashouts->date}}">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Remarks</label>
                                                                                                                <textarea class="form-control" name="remarks" rows="5"></textarea>

                                    
                                                                                                        </div>
                            
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                                                                                        <button type="submit" class="btn btn-primary save">Save</button>
                                                    </div>
                    </form>
                   
                   @endsection