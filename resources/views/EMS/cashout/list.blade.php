
@extends('layouts.app')
@section('title')
Cashouts List
@endsection
@section('page_heading')
    <h3> Cashouts List </h3>
@endsection





@section('content')

    <section class="content ">
            <!-- Your Page Content Here -->
            
 <a href="{{route('admin.cashout.form')}}"  class="btn btn-primary"><i class="fa fa-plus">  Add New</i></a>
</div>
<div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="example1_info" style="width: 1546px;">
        <thead>
        
         
                <th>Expenditure Type</th>
                <th>Amount</th>
                <th>Withdrawn By</th>
                <th>Date</th>              
                <th>Remarks</th>                
                <th>Action</th>
                
                </thead>
             <tbody>
            @foreach($cashouts as $value)
           <tr>
            <td>{{$value->expendituretype}}</td>              
            <td>{{$value->amount}}</td>
            <td>{{$value->applicants->first_name}} {{$value->applicants->surname}}</td>
            <td>{{$value->date}}</td>
            
            <td>{{$value->remarks}}</td>
            
            <td><a  href="{{route('admin.cashout_edit',$value->id)}}"> Edit</a>||  
                        <a  href="{{route('admin.cashout.delete',$value->id)}}">Delete</a></td>

           


            @endforeach
            </tbody>
        </section>




@endsection
@section('scripts')

    

@endsection