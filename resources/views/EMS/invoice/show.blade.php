
@extends('layouts.app')
@section('title')
Invoice
@endsection
@section('content')

    <div class="row helo">
        <div class="white-box">
            <table class="table table-bordered  " id="user-datatable">
                <thead>
                    {{--<tr>--}}
                        <th>S.N.</th>
                        <th>Client</th>
                        <th>Service</th>
                        <th>Price</th>
                        <th>Recieved By</th>
                        <th>Date</th>
                        <th>Paid</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    {{--</tr>--}}
                </thead>

                <tbody>

                @foreach($invoice as $value)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$value->client->name}}</td>
                        <td>{{$value->Service->name}}</td>
                        <td>{{$value->price}}</td>
                        <td>{{$value->receivedby}}</td>
                        <td>{{$value->date}}</td>
                        <td>{{$value->paid}}</td>
                        <td>{{$value->remarks}}</td>
                        <td><span><a href="">Edit</a></span> ||
                            <span>
                            <a  href="">Delete</a>
                            </span>
                            <!-- Modal -->
                        </td>


                    </tr>
                  
                @endforeach
                </tbody>
            </table>
        </div>
        @endsection