
@extends('layouts.app')
@section('title')
Invoice List
@endsection




@section('css')

<style type="text/css">
    
    .invoice_table{

    
            overflow: hidden;
            overflow-x: scroll;

    }

</style>

@endsection

@section('page_heading')
    <h3> Invoice List </h3>
@endsection





@section('content')

    <section class="content ">
            <!-- Your Page Content Here -->
            <div class="text-right">
            <a href="{{route('admin.cashin.form')}}" class="btn btn-primary"><i class="fa fa-plus">  Add New Cashin</i></a></div>
 
<div class="row"><div class="col-sm-12">
<div class="invoice_table table-responsive">
  <table id="datatable-basic" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="example1_info" style="width: 1546px;">
        <thead>
        
                <th>Applicant</th>
                <th>Date</th>                              
                           
                <th>Service</th> 
                
                <th>Received By</th>
                <th>Price</th>
                           
                <th>Action</th>
                
                </thead>
             <tbody>
            @foreach($invoices as $value)
           <tr>
            <td>{{$value->applicants->first_name}} {{$value->applicants->surname}}</td>              
            <td>{{$value->date}}</td>           
            <td>{{$value->Service->name}}</td>
           <td>{{$value->users->name}}</td>
           <td>{{$value->Service->price}}</td>
           
            <td> <a href="{{ route('admin.invoice.view',$value['id'])}}" class="btn btn-sm btn-success" ><i class="fa fa-plus"> View Invoice</i></a>||<a href="{{ route('admin.pdf.view',$value['id'])}}" class="btn btn-sm btn-primary">GENERATE PDF</a>||<a  href="{{route('admin.invoice.delete',$value->id)}}">Delete</a></td>

            @endforeach
            </tbody>
        
    </tr>
</tbody>
</table>
</div>
</div>
</div>
</section>




@endsection
@section('scripts')

    

@endsection