<!DOCTYPE html>
<html>
<head>
  <title>INVOICE</title>

  <style type="text/css">
    table {
  border-collapse: collapse;
  width: 100%;
}

table, th, td {
  border: 0.1px solid black;
  text-align: center;
}

  </style>
</head>
<body>
  <div class="page-content read container-fluid containe bg-white py-3 my-5">

  <div class="row pad-top-botm">

<span>
    <div class="col-lg-6 col-md-6 col-sm-6">
      <img src="https://apilayer-user-file-uploads.s3.amazonaws.com/87861c92e4c87a810d3ac766037b2498_Green-Computing-Nepal.png" style="padding-bottom:20px; height:100px; width:300px;">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 text-right" style="float: left;">
      <strong> Green Computing Nepal Pvt. Ltd.</strong>
      <p >
        Pragati Marg (Way to Liberty College)<br>
        Anamnnagar Hanumansthan<br>
        Kathmandu, Bagmati, 44600<br>
        Nepal<br>
        Tax ID: PAN NO : 603524499
      </p>
    </div>

  </div>
  </span>
  <div class="row text-center contact-info" style="text-align: center;">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <hr />
      <span style="text-align: center;">
        <strong>Email : </strong> greencomputingnepal@gmail.com
      </span><br>
      <span style="text-align: center;">
        <strong>Call : </strong> +95 - 890- 789- 9087
      </span>
      <hr />
    </div>
  </div>
  <div class="row pad-top-botm client-info">
    <div class="col-lg-6 col-md-6 col-sm-6">

      <h4> <strong>Client Information</strong></h4>
     
     <h4> Name :{{$applicant[0]->first_name}} {{$applicant[0]->surname}}</h4>
      <h4>Phone No : {{$applicant[0]->mobile_no}}</h4> 
     <h4>E-mail :{{$applicant[0]->email}}</h4>
    </div>
    <hr/>
    <div class="col-lg-6 col-md-6 col-sm-6 text-right">
      <h4 style="text-align: left;"> <strong>Payment Details </strong></h4>

  <h4>Invoice No : {{$invoicedata->id}}</h4>
    <h4>  Date :{{$invoicedata->updated_at}}</h4>
    </div>
  </div>
  </br>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="table-responsive">
        <table >
          <thead>
            <tr>
              <th>S. No.</th>
              <th>Service</th>
              <th>Amount</th>
              <th>Paid On</th>
            </tr>
          </thead>
          <tbody>
          @foreach($cashindata as $service)
            <tr>
             <td><h4>{{$loop->iteration}}</h4></td>
              <td><h4>{{$service->Service->name}}</h4></td>
              <td><h4>Rs. {{$service->amount_paid}}</h4></td>
              <td><h4>{{$service->date_paid}}</h4></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="text-right">
     
      <div class="ttl-amts">
      
      <hr />


      <div class="ttl-amts" style="text-align: right;">
        <h4 class="text-left">Sub Total : Rs. {{$invoicedata->price}} </h4>
        <h3>TotalPaid: Rs.
        
        {{$totalpaid}}
        </h3>
      </div>

      <div class="ttl-amts" style="text-align: right;">
      <h4>  Due: Rs.
        
        {{$due}}
        </h4>
      </div>
      <hr />
      <div class="ttl-amts" style="text-align: right;">
        <strong>Total: (<span data-currency="code">NPR</span>)</strong>
        <strong>

    {{$totalpaid}}
        </strong>
      </div>
      <hr />
    </div>
  </div>
  </div>
</div>

</body>
</html>
