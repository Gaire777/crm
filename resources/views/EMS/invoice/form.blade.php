
@extends('layouts.app')
@section('title')
Invoice Add
@endsection


@section('content')
<form role="form" class="form-edit-add" action="{{route('admin.invoice.add')}}" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    <div class="modal-body">     
                        <!-- CSRF TOKEN -->
               @csrf  
                
                             <div class="form-group"><label for="Appllicant">Applicant :</label><select class="form-control select2-hidden-accessible" data-placeholder="Enter apllicant" rel="select2" name="applicant" tabindex="-1" aria-hidden="true">
                        @foreach($applicants as $value)
                        
                            <option value="{{$value->id}}" class="form-control">{{$value->first_name}} {{$value->surname}}

                        </option>

                        @endforeach
                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></div>


                    <div class="form-group"><label for="service">Service* :</label><select class="form-control select2-hidden-accessible" required="1" data-placeholder="Enter Service" rel="select2" name="service" tabindex="-1" aria-hidden="true" aria-required="true"><option value="7">Prometric Exam Service Charge</option>
                        <option value="9">MCQ Online Exam Practice </option>
                        <option value="10">OET Preparation Class</option>
                        <option value="11">Prometric Exam Book</option>
                        <option value="14">Sasto Website (Basic) - 1st Year</option>
                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></div>

                    <div class="form-group"><label for="price">Price :</label><input class="form-control" placeholder="Enter Price" name="amount_paid" type="number" value=""></div>
                     <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Date </label>
                                    <input type="date" class="form-control" name="date" placeholder="Date " value="">

                                    
                                                                                                        </div>

                        <div class="form-group"><label for="receivedby">Received by: :</label><select class="form-control select2-hidden-accessible" data-placeholder="Enter Received by:" rel="select2" name="receivedby" tabindex="-1" aria-hidden="true">
                            <option value="1">Super Admin</option>
                            <option value="3">Rupesh Thakur</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></div>

                       


           
        
                        <div class="panel-footer">
                                                                                        <button type="submit" class="btn btn-primary save">Save</button>
                                                    </div>
                    </form>
     @endsection