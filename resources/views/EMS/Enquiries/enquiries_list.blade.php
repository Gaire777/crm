@extends('layouts.app')

@section('title')
Enquiries List
@endsection

@section('css')

<style>
  .enquiry_table {
    overflow: hidden;
    overflow-x: scroll;
  }
</style>

@endsection


@section('page_heading')
<h3> Enquiries List </h3>

@endsection

@section('content')

<div class="container-fluid mt-5">
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">Enquiries</h3>
            </div>
            <div class="col-4 text-right">
              <a href="{{ route('admin.enquiries.form')}}" class="btn btn-sm btn-default">Add Enquiries</a>
            </div>
          </div>
        </div>

        <div class="col-12">
          @if (session('status'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div>

        <div class="table-responsive pb-4">
          <table class="table align-items-center table-flush" id="datatable-basic">
            <thead class="thead-light">
              <tr>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Middle Name</th>
                <th scope="col">Email</th>
                <th scope="col">phone</th>
                <th scope="col">Address</th>
                <th scope="col">Subject</th>
                <th scope="col">Qualification Level</th>
                <th scope="col"> Experience</th>
                <th scope="col">Country Intrested</th>
                <th scope="col">Category</th>
                <th scope="col">Enquiry Form</th>
                <th scope="col"> Source</th>
                <th scope="col">Remarks</th>
                <th scope="col">Responded Through</th>
                <th scope="col">Eligibility</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($enquiries as $value)
              <tr>
                <td>{{$value->first_name}}</td>
                <td>{{$value->last_name}}</td>
                <td>{{$value->middle_name}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->phone}}</td>
                <td>{{$value->address}}</td>
                <td>{{$value->subject}}</td>
                <td>{{$value->qualification_level}}</td>
                <td>{{$value->experience}}</td>
                <td>{{$value->country_intrested}}</td>
                <td>{{$value->categories->name}}</td>
                <td>{{$value->enquiry_from}}</td>
                <td>{{$value->source}}</td>
                <td>{{$value->remarks}}</td>
                <td>{{$value->responded_through }}</td>
                <td>{{$value->eligibility}}</td>
                <td>
                  <a class="btn btn-sm btn-default px-1" href="{{ route('admin.edit_enquiries',$value->id)}}"><i class="fas fa-edit"></i></a>
                  <a class="btn btn-sm btn-danger px-1" href="{{ route('admin.delete-enquiries',$value->id)}}"><i class="fas fa-trash-alt"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection