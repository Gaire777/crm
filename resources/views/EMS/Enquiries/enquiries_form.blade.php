@extends('layouts.app')

@section('title')
Enquiries Add
@endsection

@section('page_heading')
<h3>Enquiries Add </h3>
@endsection

@section('content')

<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-xl-12 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-8">
							<h3 class="mb-0">Add Enquires</h3>
						</div>
						<div class="col-4 text-right">
							<a href="{{ route('admin.enquiries.list') }}" class="btn btn-sm btn-default">{{ __('Back to list') }}</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="post" action="{{ route('admin.enquiries.add') }}" autocomplete="on" enctype="multipart/form-data">
						@csrf

						<h6 class="heading-small text-muted mb-4">{{ __('Add New Enquires') }}</h6>
						<div class="pl-lg-4">
							<div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="first_name">First Name</label>
								<input type="text" name="first_name" id="first_name" class="form-control form-control-alternative{{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name" value="{{ old('first_name') }}">

								@if ($errors->has('first_name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('first_name') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="last_name">Last Name</label>
								<input type="text" name="last_name" id="last_name" class="form-control form-control-alternative{{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Last Name" value="{{ old('last_name') }}">

								@if ($errors->has('last_name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('last_name') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('middle_name') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="middle_name">Middle Name</label>
								<input type="text" name="middle_name" id="middle_name" class="form-control form-control-alternative{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" placeholder="Middle Name" value="{{ old('middle_name') }}">

								@if ($errors->has('middle_name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('middle_name') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="email">Email</label>
								<input type="text" name="email" id="email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{ old('email') }}">

								@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="phone">Phone</label>
								<input type="text" name="phone" id="phone" class="form-control form-control-alternative{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Phone" value="{{ old('phone') }}">

								@if ($errors->has('phone'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('phone') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="address">Address</label>
								<input type="text" name="address" id="address" class="form-control form-control-alternative{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Address" value="{{ old('address') }}">

								@if ($errors->has('address'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('address') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('subject') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="subject">Select Subject</label>
								<select type="text" name="subject" id="subject" class="select2-single form-control form-control-alternative{{ $errors->has('subject') ? ' is-invalid' : '' }}" placeholder="Subject" value="{{ old('subject') }}">
									<option value="Nursing">Nursing</option>
									<option value="Pharmacy">Pharmacy</option>
									<option value="Physiotherapy">Physiotherapy</option>
									<option value="Dentistry">Dentistry</option>
									<option value="Optometry">Optometry</option>
									<option value="MBBS(Doctor)">MBBS(Doctor)</option>
									<option value="MD/MS(Doctor)">MD/MS(Doctor)</option>
									<option value="Ocupational Therapy">Ocupational Therapy</option>
									<option value="Radiography">Radiography</option>
								</select>

								@if ($errors->has('subject'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('subject') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('qualification_level') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="qualification_level">Select Qualification Level</label>
								<select type="text" name="qualification_level" id="qualification_level" class="select2-single form-control form-control-alternative{{ $errors->has('qualification_level') ? ' is-invalid' : '' }}" placeholder="Qualifaction Level" value="{{ old('qualification_level') }}">
									<option value="Diploma">Diploma</option>
									<option value="Bachelor">Bachelor</option>
									<option value="Master">Master</option>
								</select>

								@if ($errors->has('qualification_level'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('qualification_level') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('experience') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="experience">Select Experience</label>
								<select type="text" name="experience" id="experience" class="select2-single form-control form-control-alternative{{ $errors->has('experience') ? ' is-invalid' : '' }}" placeholder="Experiences" value="{{ old('experience') }}">
									<option value="1">1</option>
									<option value="2" selected="selected">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>

								@if ($errors->has('experience'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('experience') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('country_intrested') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="country_intrested">Country Intrested</label>
								<select type="text" required name="country_intrested[]" multiple="true" id="country_intrested" class="select2-multi form-control-alternative{{ $errors->has('country_intrested') ? ' is-invalid' : '' }}" placeholder="Country Intrested" value="{{ old('country_intrested') }}">

									<option value="UAE-Dubai">UAE-Dubai</option>
									<option value="UAE-Abu Dhabi">UAE-Abu Dhabi</option>
									<option value="Qatar">Qatar</option>
									<option value="Oman">Oman</option>
									<option value="USA">USA</option>
									<option value="UK">UK</option>
									<option value="Canada">Canada</option>
									<option value="Australia">Australia</option>
									<option value="Ireland">Ireland</option>
									<option value="New Zealand">New Zealand</option>
									<option value="Europe">Europe</option>
								</select>

								@if ($errors->has('country_intrested'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('country_intrested') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('enquiry_from') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="enquiry_from">Enquiry_from</label>
								<select type="text" name="enquiry_from" id="enquiry_from" class="select2-single form-control form-control-alternative{{ $errors->has('enquiry_from') ? ' is-invalid' : '' }}" placeholder="Enquiry From" value="{{ old('enquiry_from') }}">
									<option value="Prometric Exam Nepal">Prometric Exam Nepal</option>
									<option value="OET preparation Nepal">OET preparation Nepal</option>
									<option value="DHA Exam Nepal">DHA Exam Nepal</option>
									<option value="Medical Exam Abroad">Medical Exam Abroad</option>
									<option value="Nursing Abroad Nepal">Nursing Abroad Nepal</option>
									<option value="Nursing in Dubai">Nursing in Dubai</option>
									<option value="Nursing in Australia">Nursing in Australia</option>
									<option value="Nursing in UK">Nursing in UK</option>
									<option value="Nursing in USA">Nursing in USA</option>
									<option value="Nursing in Canada">Nursing in Canada</option>
									<option value="Doctors Abroad Nepal">Doctors Abroad Nepal</option>
									<option value="Doctors in Dubai">Doctors in Dubai</option>
									<option value="Doctors in Australia">Doctors in Australia</option>
									<option value="Doctors in UK">Doctors in UK</option>
									<option value="Doctors in USA">Doctors in USA</option>
									<option value="Doctors in Canada">Doctors in Canada</option>
								</select>

								@if ($errors->has('enquiry_from'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('enquiry_from') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('source') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="source">Select Source</label>
								<select type="text" name="source" id="source" class="select2-single form-control form-control-alternative{{ $errors->has('source') ? ' is-invalid' : '' }}" placeholder="Source" value="{{ old('source') }}">
									<option value="Facebook" selected="selected">Facebook</option>
									<option value="Email">Email</option>
									<option value="Instagram">Instagram</option>
									<option value="Linkedin">Linkedin</option>
									<option value="Website">Website</option>
									<option value="Twak">Twak</option>
								</select>

								@if ($errors->has('source'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('source') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('responded_through') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="responded_through">Responded Through</label>
								<select type="text" required name="responded_through[]" multiple="true" id="responded_through" class="select2-multi select2-selection--multiple form-control form-control-alternative{{ $errors->has('responded_through') ? ' is-invalid' : '' }}" placeholder="Responded Through" value="{{ old('responded_through') }}">
									<option value="facebook">Facebook</option>
									<option value="Email">Email</option>
									<option value="Instagram">Instagram</option>
									<option value="Linkedin">Linkedin</option>
									<option value="Website">Website</option>
									<option value="Twak">Twak</option>
								</select>

								@if ($errors->has('responded_through'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('responded_through') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('eligibility') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="eligibility">Eligibility</label>
								<select type="text" name="eligibility" id="eligibility" class="select2-single form-control form-control-alternative{{ $errors->has('eligibility') ? ' is-invalid' : '' }}" placeholder="Eligibility" value="{{ old('eligibility') }}">
									<option value="Eligible">Eligible</option>
									<option value="Not Eligible">Not Eligible</option>
								</select>

								@if ($errors->has('eligibility'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('eligibility') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('category_id') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="category_id">Category</label>
								<select type="text" name="category_id" id="category_id" class="select2-single form-control form-control-alternative{{ $errors->has('category_id') ? ' is-invalid' : '' }}" placeholder="category_id" value="{{ old('category_id') }}">
									@foreach($category as $value)
									<option value="{{$value->id}}">{{$value->name}}</option>
									@endforeach
								</select>

								@if ($errors->has('category_id'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('category_id') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('remarks') ? ' has-danger' : '' }}">
								<label class="form-control-label" for="remarks">Remarks</label>
								<textarea rows="3" type="text" name="remarks" id="remarks" class="form-control form-control-alternative{{ $errors->has('remarks') ? ' is-invalid' : '' }}" placeholder="Service remarks" value="{{ old('remarks') }}"></textarea>

								@if ($errors->has('remarks'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('remarks') }}</strong>
								</span>
								@endif
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-success mt-4">Create</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')

<!-- Select2 Js -->
<script src="{{ asset('argon') }}/vendor/select2/js/select2.js"></script>

<script>
	$(document).ready(function() {
		$('.select2-single').select2();
		$('.select2-multi').select2();
	});
</script>
@endsection