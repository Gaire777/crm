@extends('layouts.app')
@section('content')
@section('title')
Add Applicant
@endsection
<div class="container">
	 @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
    
<div class="container" >
<form method="POST"  action="{{route('admin.applicant.add')}}" enctype="multipart/form-data" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
	<div >
		<div><label>Applicant Enquired ?</label>
			
			<select  class="checking"  id="enquirycheck" onchange="check()">
				<option>Select</option>

				<option value="yes">Yes</option>
				<option value="no">No</option>
			</select>






			<div id="ifYes" style="display: block;">

				<div class="form-group" id="enquired_applicant" style="display: none;"><label for="gender">Enquired Applicant</label>
					
					<select class="form-control" required="1"  id="enquiryid" data-placeholder="Enter Gender" rel="select2" name="enquiry_id" tabindex="-1" aria-hidden="true" aria-required="true">
						

						<option >Select Enquire Applicant</option>
						@foreach($enquiries as $value)
						
						<option value="{{$value->id}}">{{$value->first_name}} {{$value->last_name}}</option>
							
						@endforeach

					</select>
				</div>
				
				<div id="form" >

                    <div class="form-group"><label for="fname">First name * :</label><input class="form-control" id="fname" placeholder="Enter First name " required data-rule-maxlength="256" required="1" name="fname" type="text" value="{{ old('fname') }}" aria-required="true"></div>

					<div class="form-group"><label for="lname">Last Name :</label><input class="form-control" id="lname" placeholder="Enter Last Name" required data-rule-maxlength="256" name="lname" type="text" value="{{ old('lname') }}"></div>

					<div class="form-group"><label for="middle_name">Middle Name :</label><input class="form-control"  id="middle_name" placeholder="Enter Middle Name" data-rule-maxlength="256" name="middle_name" type="text" value="{{ old('middle_name') }}"></div>
										
					<div class="form-group"><label for="mobile_no">Mobile No :</label><input class="form-control" id="mobile_no" placeholder="Enter Mobile No" required data-rule-maxlength="20" data-rule-unique="true" field_id="66" adminroute="admin" row_id="0" name="mobile_no" type="number" value="{{ old('mobile_no') }}"></div>

					<div class="form-group"><label for="email">Email :</label><input class="form-control" id="email" placeholder="Enter Email" data-rule-maxlength="256" required data-rule-unique="true" field_id="68" adminroute="admin" row_id="0" data-rule-email="true" name="email" type="email" value="{{ old('email') }}"></div>
				</div>






					<div class="form-group"><label for="maiden_name">Maiden Name :</label><input class="form-control" placeholder="Enter Maiden Name" data-rule-maxlength="256" name="maiden_name" type="text" value="{{ old('maiden_name') }}"></div>
					
					<div class="form-group"><label for="gender">Gender* :</label>
						<select class="form-control select2" required="1" data-placeholder="Enter Gender" rel="select2" name="gender" tabindex="-1" aria-hidden="true" aria-required="true">

							<option value="Male">Male</option>
							<option value="Female">Female</option>
							<option value="Others">Others</option>
						</select>
					</div>

					<div class="form-group"><label for="dob">Date Of Birth* :</label><div class="input-group date"><input class="form-control" placeholder="Enter Date Of Birth" required="1" name="dob" type="date" value="{{ old('dob')}}" aria-required="true"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div>
					</div>

					<div class="form-group"><label for="identity_type">Identity Type :</label><select class="form-control " data-placeholder="Enter Identity Type" rel="select2" name="identity_type" tabindex="-1" aria-hidden="true">
						<option value="CitizenShip">CitizenShip</option>
						<option value="Passport">Passport</option>
					</select>

					</div>

					<div class="form-group"><label for="identity_card_name">Identity Card No* :</label><input class="form-control" placeholder="Enter Identity Card No" data-rule-maxlength="256" data-rule-unique="true" field_id="64" adminroute="admin" row_id="0" required="1" name="identity_card_name" type="text" value="{{ old('identity_card_name') }}" aria-required="true"></div>

					<div class="form-group"><label for="passport_no">Passport No :</label><input class="form-control" placeholder="Enter Passport No" data-rule-maxlength="256" data-rule-unique="true" field_id="65" adminroute="admin" row_id="0" name="passport_no" type="text" value="{{ old('passport_no ') }}"></div>

					

					<div class="form-group"><label for="nationality">Nationality* :</label><input class="form-control" placeholder="Enter Nationality" data-rule-maxlength="256" required="1" name="nationality" type="text" value="{{ old('nationality') }}" aria-required="true"></div>
					
					
					
					<div class="form-group">
						<label for="passport_docs" style="display:block;">Passport Docs* :</label>
						<span>
						<input class="form-control" id="up" placeholder="Enter Passport Docs" required="1" name="passport_docs" type="file"  aria-required="true">
						<a class="uploaded_file hide" onclick="document.getElementById('up').value = '' ">
						</span>
					</div>

					<div class="form-group"><label for="applicant_category">Applicant's Category :</label>
						<select class="form-control" data-placeholder="Enter Applicant's Category" rel="select2" required name="applicant_category" tabindex="-1" aria-hidden="true">
						    @foreach($categories as $value)
						    
						
							<option value="{{$value->id}}" class="form-control">{{$value->name}} 

						</option>


						@endforeach
							
						</select>
					</div>
					
			</div>

			</div>

			<div class="modal-footer">
				
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection
@section('js')


<script type="text/javascript">


	  	function check(){

  		var check=$('#enquirycheck').val();
  		// console.log(check);

  	 			if (check=='yes') {
  	 			 		// console.log('yes');


  	 			$('#enquired_applicant').css("display", "block");
				 }
  	 			
  	 			 else{
  	 			 	// console.log('no');
  	 			$('#enquired_applicant').css("display", "none");

  	 			}

  	 };

</script>




<script>
  $(document).ready(function() {

//============================Ajax for enquiry data in applicant form==================================

    $("#enquiryid").change(function(e) {
      if (e.target.value !== '') {
        var enquiry_ids = e.target.value;
         // alert(enquiry_ids);
        // ajax
        $('#fname').empty();
        $('#lname').empty();
        $('#middle_name').empty();
        $('#mobile_no').empty();
		$('#email').empty();
                

        $.get("/admin/search/fetch/"+enquiry_ids, function(data) {
         	// console.log('ongoing');

          $.each(data, function(index, sersearchObj){

          	console.log(sersearchObj);

          	 var foo = JSON.parse(sersearchObj);

  			// console.log(foo[0].first_name);

			$('#fname').val(foo[0].first_name);
			$('#lname').val(foo[0].last_name);
	        $('#middle_name').val(foo[0].middle_name);
	        $('#mobile_no').val(foo[0].phone);
			$('#email').val(foo[0].email);
	                
          });
        });
      }

    });
  });
</script>

@endsection
