@extends('layouts.app')
@section('title')
Profile
@endsection

@section('css')

<style type="text/css">
	
	.profile_table{

	
			overflow: hidden;
			overflow-x: scroll;

	}

</style>

@endsection


@section('content')
@section('button')
    <a href="{{route('admin.applicant.list')}}" class="btn btn-primary"><i class="fa fa-plus">Applicant List</i></a>

@endsection

<div id="about" class="container">
  <div class="cvitae-container"><div class="cvitae-section-content">
    <div class="content-left short-bio container box">

        <div class="row">

        <div class="col-md-6" style="position: relative;">
            
                  
        


      <p class="info"><span class="field-title">Category</span> <span class="field-separator">:</span> <span class="field-value"> @if($profile->applicant_category== '1') Doctor @elseif($profile->applicant_category!='1') Nurse @endif </span></p>






      <p class="info"><span class="field-title">Full Name</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->first_name}} {{$profile->surname}}</span></p>

      <p class="info"><span class="field-title">Gender</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->gender}}</span></p>

      

        <p class="info"><span class="field-title">Date Of birth</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->dob}}</span></p>

         <p class="info"><span class="field-title">Identity Type</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->identity_type}}</span></p>

          <p class="info"><span class="field-title">Identity No.</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->identity_card_no}}</span></p>

           <p class="info"><span class="field-title">Mobile No</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->mobile_no}}</span></p>

           <p class="info"><span class="field-title">Nationality</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->nationality}}</span></p>
            <p class="info"><span class="field-title">Email</span> <span class="field-separator">:</span> <span class="field-value">{{$profile->email}}</span></p>

             </div>
       </div>

  
     
              <div class="content-right about"><h3 class="cvitae-section-title">Check List</h3>

<div class="profile_table">
  <table class="table container align-items-center">
                              <thead>
                                  <tr>
                                    <th class="form-control">applicant</th>
                                    <th scope="col">password_citizenship_certificate </th>
                                    <th scope="col">slc_certificate</th>
                                    <th scope="col">plus_two_isc_pcl_certificate</th>
                                    <th scope="col">diploma_degree_certificate  </th>
                                    <th scope="col">mark_sheet_of_each_year_final_transcript  </th>
                                    <th scope="col">equivalent_certificate  </th>
                                    <th scope="col">council_registration_certificate </th>
                                    <th scope="col">council_registration_certificate_renew  </th>
                                    <th scope="col">good_standing_letter_from_council  </th>
                                    <th scope="col">work_experience_letter_till_date  </th>
                                    <th scope="col">basic_life_support_for_nurses </th>
                                    <th scope="col">mrp_size_photo_in_white_background  </th>
                                  </tr>
                               </thead>
                               </tr>

                              <tbody>
  @foreach($checklist as $value)
  
  <tr>
    <td>{{$value->applicant_id}}</td>
      <td @if($value->password_citizenship_certificate == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox" >
                  <input type="checkbox"  id="customCheck3" @if($value->password_citizenship_certificate == '1') checked @endif disabled> 
                   </div>
         </td>  

    <td @if($value->slc_certificate == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox" >
                  <input type="checkbox"  id="customCheck3" @if($value->slc_certificate == '1') checked @endif disabled> 
                   </div>
         </td>    
    <td @if($value->plus_two_isc_pcl_certificate == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox"   >
                  <input type="checkbox"  id="customCheck3" @if($value->plus_two_isc_pcl_certificate == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->diploma_degree_certificate == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->diploma_degree_certificate == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->mark_sheet_of_each_year_final_transcript == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->mark_sheet_of_each_year_final_transcript == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->equivalent_certificate == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->equivalent_certificate == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->council_registration_certificate == '1') style="background:green; " @endif >
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->council_registration_certificate == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->council_registration_certificate_renew == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->council_registration_certificate_renew == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->council_registration_certificate_renew == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->good_standing_letter_from_council == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->work_experience_letter_till_date == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->work_experience_letter_till_date == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->basic_life_support_for_nurses == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->basic_life_support_for_nurses == '1') checked @endif disabled>  </div>
            </td>   
    <td @if($value->mrp_size_photo_in_white_background == '1') style="background:green; " @endif>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  id="customCheck3" @if($value->mrp_size_photo_in_white_background == '1') checked @endif disabled>  </div>
            </td>   
      
    
  </tr>
  @endforeach
  </tbody>
                            </table>
               
              
              </div>
</div>
</div>
</div>
</div>






  @endsection
  
   