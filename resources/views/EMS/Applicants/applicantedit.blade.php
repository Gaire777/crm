@extends('layouts.app')
@section('content')
@section('title')
Edit
@endsection
<div class="container">
<form method="POST" action="{{route('admin.applicant.edited')}}" enctype="multipart/form-data" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="id" value="{{$applicant->id}}">

                    <!-- <div class="form-group"><label for="fname">Applicant </label><input class="form-control" placeholder="Enter First name " data-rule-maxlength="256" required="1" name="applicant_id" type="text" value="{{$applicant->id}}" aria-required="true"></div> -->

                    <div class="form-group"><label for="fname">First Name</label><input class="form-control" placeholder="Enter First name " data-rule-maxlength="256" required="1" name="fname" type="text" value="{{$applicant->first_name}}" aria-required="true"></div>

					<div class="form-group"><label for="lname">Last Name :</label><input class="form-control" required placeholder="Enter Last Name" data-rule-maxlength="256" name="lname" type="text" value="{{$applicant->surname}}"></div>

					<div class="form-group"><label for="middle_name">Middle Name :</label><input class="form-control" placeholder="Enter Middle Name" data-rule-maxlength="256" name="middle_name" type="text" value="{{$applicant->middel_name}}"></div>
					
					<div class="form-group"><label for="gender">Gender* :</label>
						<select class="form-control select2" required="1" data-placeholder="Enter Gender" rel="select2" name="gender" tabindex="-1" aria-hidden="true" aria-required="true">
							<option value="{{$applicant->gender}}" selected > {{$applicant->gender}}</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
							<option value="Others">Others</option>
						</select>
					</div>

					<div class="form-group"><label for="dob">Date Of Birth* :</label><div class="input-group date"><input class="form-control" placeholder="Enter Date Of Birth" required="1" name="dob" type="date" value="{{$applicant->dob}}" aria-required="true"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div>
					</div>

					<div class="form-group"><label for="identity_type">Identity Type :</label><select class="form-control " data-placeholder="Enter Identity Type" rel="select2" name="identity_type" tabindex="-1" aria-hidden="true">

						<option value="{{$applicant->identity_type}}" selected > {{$applicant->identity_type}}</option>
						<option value="CitizenShip">CitizenShip</option>
						<option value="Passport">Passport</option>
					</select>

					</div>

					<div class="form-group"><label for="identity_card_name">Identity Card No* :</label><input class="form-control" placeholder="Enter Identity Card No" data-rule-maxlength="256" data-rule-unique="true" field_id="64" adminroute="admin" row_id="0" required="1" name="identity_card_name" type="text" value="{{$applicant->identity_card_no}}" aria-required="true"></div>

					<div class="form-group"><label for="passport_no">Passport No :</label><input class="form-control" placeholder="Enter Passport No" data-rule-maxlength="256" data-rule-unique="true" field_id="65" adminroute="admin" row_id="0" name="passport_no" type="text" value="{{$applicant->passport_no}}"></div>

					<div class="form-group"><label for="mobile_no">Mobile No :</label><input class="form-control" placeholder="Enter Mobile No" data-rule-maxlength="20" data-rule-unique="true" field_id="66" adminroute="admin" row_id="0" name="mobile_no" type="text" value="{{$applicant->mobile_no}}"></div>

					<div class="form-group"><label for="nationality">Nationality* :</label><input class="form-control" placeholder="Enter Nationality" data-rule-maxlength="256" required="1" name="nationality" type="text" value="{{$applicant->nationality}}" aria-required="true"></div>
					
					<div class="form-group"><label for="email">Email :</label><input class="form-control" placeholder="Enter Email" data-rule-maxlength="256" data-rule-unique="true" field_id="68" adminroute="admin" row_id="0" data-rule-email="true" name="email" type="email" value="{{$applicant->email}}"></div>
					
					<div class="form-group">
						<label for="passport_docs" style="display:block;">Passport Docs* :</label>
						<input class="form-control" placeholder="Enter Passport Docs" required="1" name="passport_docs" type="file" aria-required="true">
						
						

				<div class="form-group"><label for="applicant_category">Applicant's Category :</label>
						<select class="form-control" data-placeholder="Enter Applicant's Category" rel="select2" name="applicant_category" tabindex="-1" aria-hidden="true">


							<option value="{{ $applicant->applicant_category}}">SELECT </option>
						    @foreach($categories as $value)
						    
						
							<option value="{{$value->id}}" class="form-control">{{$value->name}} 

						</option>


						@endforeach
							
						</select>
					</div>

					
			</div>

			</div>

			<div class="modal-footer">
				
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection