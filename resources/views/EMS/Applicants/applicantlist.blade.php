@extends('layouts.app')
@section('title')
Applicant List
@endsection

@section('css')

<style type="text/css">
	
	.applicant_table{

	
			overflow: hidden;
			overflow-x: scroll;
			/*overflow-y:scroll;*/

	}

</style>

@endsection


@section('page_heading')
	    	<h1>
	        Applicants  <small>Applicants listing</small>
	    	</h1>

@endsection

 @section('button')
	      
        	<a href="{{route('admin.applicant.add')}}" ><button class="btn btn-success btn-sm " >Add Applicant</button></a>
       
@endsection







@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="applicant_table">
	<table id="datatable-basic" class="table table-bordered dataTable no-footer " role="grid" aria-describedby="example1_info" style="width: 1365px;">
	
		<thead>

		<tr class="success" role="row">
			<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 14.0104px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">S.N</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 36.0104px;" aria-label="First name : activate to sort column ascending">First name </th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Last Name</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 46.0104px;" aria-label="Gender: activate to sort column ascending">Gender</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 32.0104px;" aria-label="Date Of Birth: activate to sort column ascending">Date Of Birth</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 50.0104px;" aria-label="Identity Type: activate to sort column ascending">Identity Type</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 50.0104px;" aria-label="Identity Card No: activate to sort column ascending">Identity Card No</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 56.0104px;" aria-label="Passport No: activate to sort column ascending">Passport No</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 42.0104px;" aria-label="Mobile No: activate to sort column ascending">Mobile No</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 70.0104px;" aria-label="Nationality: activate to sort column ascending">Nationality</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 105.01px;" aria-label="Email: activate to sort column ascending">Email</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 56.0104px;" aria-label="Passport Docs: activate to sort column ascending">Passport Docs</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 70.0104px;" aria-label="Applicant's Category: activate to sort column ascending">Applicant's Category</th>
			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;" aria-label="Actions">Actions</th>
		</tr>
		</thead>
		<tbody>
			@foreach($applicants as $value)

			
		<tr role="row" class="odd"><td class="sorting_1">{{$loop->iteration}}</td>
			<td><a href="{{route('admin.applicant.view',$value->id)}}">{{$value->first_name}}
			 @if( $value->progress_sts !='1') <span style="color:red;" >*</span> @endif 
			 
			</a></td>
			<td>{{$value->surname}}</td>
			<td>{{$value->gender}}</td>
			<td>{{$value->dob}}</td>
			<td>{{$value->identity_type}}</td>
			<td>{{$value->identity_card_no}}</td> 
			<td>{{$value->passport_no}}</td>
			<td>{{$value->mobile_no}}</td>
			<td>{{$value->nationality}}</td>
			<td>{{$value->email}}</td>
			<td>
			@if($value->passport_docs != null)
			<a href="{{url('/storage/app/public/'.$value->passport_docs)}}"target="_blank">Passport 
			@else
			No file found
			@endif
			</td>
			
			<td>{{$value->categories->name}}</td>
			<td><span>
			 	<a href="{{route('admin.applicant.edit',$value->id)}}">Edit</a></span> ||
                            <span>
                            <a  href="{{route('admin.applicant.delete',$value->id)}}">Delete</a>
                            </span>		
		</tr>
			@endforeach
			
	</table>
	</div>
	</div>
</div>


@endsection


@section('js')


@endsection
             		
	