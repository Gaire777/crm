@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-12 table-responsive">
  <table id="datatable-basic" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="example1_info" style="width: 1365px;">
	
		<thead>
			<tr><section class="content-header " >
	    	<h1>Service List</h1>
	        <span class=" float-right">

        	<a href="{{route('admin.service.form')}}" ><button class="btn btn-success btn-sm " >Add Service</button></a>
        </span>
    </section></tr>
		<tr class="success" role="row">
			<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 14.0104px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">Id</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 36.0104px;" aria-label="First name : activate to sort column ascending">Name</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Price</th>
			
			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;" aria-label="Actions">Actions</th>
			 
		</tr>
		</thead>
		<tbody>
			@foreach($service as $value)

			
		<tr role="row" class="odd">
			<td class="sorting_1">{{$loop->iteration}}</td>
			
			<td>{{$value->name}}</td>
			<td>{{$value->price}}</td>

			<td><span><a href="{{route('admin.serviceeditform',$value->id)}}">Edit</a></span> ||
                            <span>
                            <a  href="{{route('admin.servicedelete',$value->id)}}"  class=""  >Delete</a>
                            </span>
			
						
		</tr>
			@endforeach
	</table>
	</div>
</div>
@endsection
		
	