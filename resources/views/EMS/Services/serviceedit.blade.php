@extends('layouts.app')
@section('content')
<div class="container">
	 @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
<form method="POST" action="{{route('admin.serviceedit')}}" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
	
                    <div class="form-group"><label for="fname">Service :</label>
                    	<input class="form-control" placeholder="Enter Servicce name " data-rule-maxlength="256" required="1" name="servicename" type="text" value="{{$service->name}}" aria-required="true"></div>
                    	<input type="hidden" value="{{$service->id}}" name="id">

					<div class="form-group"><label for="lname">Price :</label>
						<input class="form-control" value="{{$service->price}}" placeholder="Enter Price" data-rule-maxlength="256" name="price" type="price" value=""></div>


					
					
			</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection