@extends('layouts.app')
@section('title')
Education List
@endsection

@section('css')

<style type="text/css">
	
	.education_table{

	
			overflow: hidden;
			overflow-x: scroll;

	}

</style>

@endsection


@section('page_heading')
	    	<h1>
	        Education       <small>Education listing</small>
	    	</h1>
@endsection

 @section('button')
	      
        	<a href="{{route('admin.education.add')}}" ><button class="btn btn-success btn-sm " >Add Education</button></a>
@endsection



@section('content')
<div class="row">
	<div class="col-sm-12">
	    <div class="education_table table-responsive">
	<table id="datatable-basic" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="example1_info" style="width: 1365px;">
	
		<thead>
			
	       
        
    
		<tr class="success" role="row">
<th>applicants_name </th>
<th>authority_name </th> 
<th>authority_address </th> 
<th>authority_city </th>
<th>authority_state </th> 
<th>authority_country </th> 
<th>authority_phone_type </th> 
<th>authority_country_code </th> 
<th>authority_phone </th>
<th>authority_email </th>
<th>authority_website </th>
<th>qualification </th>
<th>institution </th>
<th>type </th> 
<th>mode </th> 
<th>major_subject </th> 
<th>minor_subject </th> 
<th>roll </th>
<th>study_from </th> 
<th>study_to </th> 
<th>conferred_date </th> 
<th>degree_issue_date </th> 
<th>expected_degree_issue_date </th> 
<th>qualification_certificate </th>
<th>marksheet</th>
			
			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;" aria-label="Actions">Actions</th>
		</tr>
		</thead>
		<tbody>
			@foreach($applicant as $value)

			
		<tr role="row" class="odd">
			<td>{{$value->applicants->first_name}} {{$value->applicants->surname}} </td>
			<td>{{$value->authority_name}} </td> 
			<td>{{$value->authority_address}} </td> 
			<td>{{$value->authority_city}} </td>
			<td>{{$value->authority_state }}</td> 
			<td>{{$value->authority_country }}</td> 
			<td>{{$value->authority_phone_type}} </td> 
			<td>{{$value->authority_country_code}} </td> 
			<td>{{$value->authority_phone }}</td>
			<td>{{$value->authority_email}} </td> 
			<td>{{$value->authority_website}} </td> 
			<td>{{$value->qualification}} </td>
			<td>{{$value->institution }}</td>
			<td>{{$value->type}} </td> 
			<td>{{$value->mode}} </td> 
			<td>{{$value->major_subject}} </td> 
			<td>{{$value->minor_subject }}</td> 
			<td>{{$value->roll}} </td>
			<td>{{$value->study_from}} </td> 
			<td>{{$value->study_to}} </td> 
			<td>{{$value->conferred_date}} </td> 
			<td>{{$value->degree_issue_date}} </td> 
			<td>{{$value->expected_degree_issue_date}} </td> 
			<td>
			@if($value->qualification_certificate != null)
			<a href="{{url('/storage/app/public/'.$value->qualification_certificate)}}"target="_blank">Qualification Certificate
			@else
			No file found
			@endif
			</td>
			<td>	@if($value->marksheet != null)
			<a href="{{url('/storage/app/public/'.$value->marksheet)}}"target="_blank">MarkSheet
			@else
			No file found
			@endif</td>
			 <td><span>
			 	<a href="{{route('admin.education.edit',$value->id)}}">Edit</a></span> ||
                            <span>
                            <a  href="{{route('admin.education.delete',$value->id)}}"  >Delete</a>
                            </span>		
		</tr>
			@endforeach
	</table>
	</div>
</div>

@endsection


@section('js')



@endsection