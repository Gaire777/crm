@extends('layouts.app')
@section('title')
Education Edit
@endsection
@section('content')
<div class="container">
<form method="POST" action="{{route('admin.education.edited')}}" enctype="multipart/form-data"  accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
			<div class="modal-body">
				<div class="box-body">
					<input class="form-control" value="{{$education->id}}"type="hidden" name="id">

					<div class="form-group"><label for="applicant_category">applicants_name

					</label>
						<select class="form-control select2" data-placeholder="Enter Applicant's type" rel="select2" name="applicants_name" tabindex="-1" aria-hidden="true">
							<option value="{{$education->id}}" selected >Select</option>
					
						@foreach($applicant as $value)
						
							<option value="{{$value->id}}" class="form-control">{{$value->first_name}} {{$value->surname}}

						</option>{{$value->first_name}}


						@endforeach
						
						
						
						</select>
					</div>
					
					<div class="form-group"><label for="applicant_category">type   </label>
						<select class="form-control select2" data-placeholder="Enter Applicant's type" rel="select2" name="type" tabindex="-1" aria-hidden="true" >

							<option value="doctor" class="form-control" @if($education->type == 'doctor') selected @endif>Doctor</option>
												
							<option value="nurse" class="form-control"  @if($education->type == 'nurse') selected @endif>Nurse</option>
				
						</select>
					</div>

					<div class="form-group"><label for="qualification">qualification {{$education->qualification}}</label>
						<select class="form-control select2" data-placeholder="Enter Applicant's Category" rel="select2" name="qualification" tabindex="-1" aria-hidden="true">
							<option value="Diploma" class="form-control" @if($education->qualification == 'Diploma') selected @endif>Diploma</option>
							<option value="Bachelor" class="form-control" @if($education->qualification == 'Bachelor') selected @endif>Bachelor</option>
							<option value="Master" class="form-control" @if($education->qualification == 'Master') selected @endif>Master</option>
						<select>
					</div>

					<div class="form-group"><label for="authority_country">authority_country </label>
						<select class="form-control select2" data-placeholder="Enter authority_country" rel="select2" name="authority_country" tabindex="-1" aria-hidden="true">

							<option value="{{$education->authority_country}}" selected > {{$education->authority_country}}</option>
							<option value="Bangladesh" class="form-control">Bangladesh</option>
							<option value="Nepal" class="form-control">Nepal</option>
							<option value="India" class="form-control">India</option>
							<option value="China" class="form-control">China</option>
							<option value="Australia" class="form-control">Australia</option>
						</select>
					</div>

					authority_name<input class="form-control" value="{{$education->authority_name}}"type="text" name="authority_name">  
					authority_address <input class="form-control" value="{{$education->authority_address}}" type="text" name="authority_address">
					authority_city<input class="form-control" value="{{$education->authority_city}}" type="text" name="authority_city"> 
					authority_state  <input class="form-control" value="{{$education->authority_state }}" type="text" name="authority_state">
			
					authority_country_code  <input class="form-control"  value="{{$education->authority_country_code}} " type="text" name="authority_country_code">
					authority_phone <input class="form-control"  value="{{$education->authority_phone}}" type="text" name="authority_phone">
					authority_email<input class="form-control" value="{{$education->authority_email}}" type="email" name="authority_email"> 
					authority_website <input class="form-control" value="{{$education->authority_website}}" type="text" name="authority_website">
					
 
					institution <input class="form-control" value="{{$education->institution}}" type="text">
					

					
					<div class="form-group"><label for="applicant_category">mode </label>
						<select class="form-control select2-hidden-accessible" data-placeholder="Enter Applicant's Mode" rel="select2" name="mode" tabindex="-1" aria-hidden="true">

							<option value="{{$education->mode}}" selected > {{$education->mode}}</option>
							<option value="Active" class="form-control">Active Enrollment</option>
							<option value="Online" class="form-control">Online Enrollment</option>
						</select>
					</div>


					major_subject<input class="form-control" value="{{$education->major_subject}}" name="major_subject" type="text"> 
					minor_subject  <input class="form-control" value="{{$education->minor_subject}}" name="minor_subject"type="text">

					roll <input class="form-control" value="{{$education->roll}}" type="text" name="roll">

					study_from <input class="form-control" value="{{$education->study_from}}" name="study_from"type="date"> 
					study_to <input class="form-control" value="{{$education->study_to}}" name="study_to"type="date"> 
					conferred_date<input class="form-control" value="{{$education->conferred_date}}" name="conferred_date" type="date">  
					degree_issue_date<input class="form-control" value="{{$education->degree_issue_date}}"  name="degree_issue_date" type="date"> 
					expected_degree_issue_date <input class="form-control" value="{{$education->expected_degree_issue_date}}" name="expected_degree_issue_date" type="date">

					qualification_certificate <input class="form-control" id="up1" value="{{$education->qualification_certificate}}"type="file">
					<a class="uploaded_file hide" onclick="document.getElementById('up1').value = '' "><i title="Remove File" class="fa fa-times" class=""></i></a><br>
					marksheet<input class="form-control" value="{{$education->marksheet}}" id="up2" type="file">
					<a class="uploaded_file hide" onclick="document.getElementById('up2').value = '' "><i title="Remove File" class="fa fa-times" class=""></i></a>
			</div>

			</div>

			<div class="modal-footer">
				
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection