@extends('layouts.app')
@section('title')
Education Add
@endsection
@section('content')
<div class="container">
<form method="POST" action="{{route('admin.education.add')}}" enctype="multipart/form-data" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
			<div class="modal-body">
				<div class="box-body">

					<div class="form-group"><label for="applicant_category">applicants_name

					</label>
						<select class="form-control select2" data-placeholder="Enter Applicant's type" rel="select2" name="applicants_name" tabindex="-1" aria-hidden="true">
					
						@foreach($applicant as $value)
						
							<option value="{{$value->id}}" class="form-control">{{$value->first_name}} {{$value->surname}}

						</option>{{$value->first_name}}


						@endforeach
						
						
						
						</select>
					</div>
					
					<div class="form-group"><label for="applicant_category">type </label>
						<select class="form-control select2" data-placeholder="Enter Applicant's type" rel="select2" name="type" tabindex="-1" aria-hidden="true">
							<option value="doctor" class="form-control">Doctor</option>
							<option value="nurse" class="form-control">Nurse</option>
						</select>
					</div>

					<div class="form-group"><label for="qualification">qualification </label>
						<select class="form-control select2" data-placeholder="Enter Applicant's Category" rel="select2" name="qualification" tabindex="-1" aria-hidden="true">
							<option value="Diploma" class="form-control">Diploma</option>
							<option value="Bachelor" class="form-control">Bachelor</option>
							<option value="Master" class="form-control">Master</option>
						<select>
					</div>

					<div class="form-group"><label for="authority_country">authority_country </label>
						<select class="form-control select2" data-placeholder="Enter authority_country" rel="select2" name="authority_country" tabindex="-1" aria-hidden="true">
							<option value="Bangladesh" class="form-control">Bangladesh</option>
							<option value="Nepal" class="form-control">Nepal</option>
							<option value="India" class="form-control">India</option>
							<option value="China" class="form-control">China</option>
							<option value="Australia" class="form-control">Australia</option>
						</select>
					</div>

					authority_name<input class="form-control" value="authority_name"type="text" name="authority_name">  
					authority_address <input class="form-control" value="authority_address" type="text" name="authority_address">
					authority_city<input class="form-control" value="authority_city" type="text" name="authority_city"> 
					authority_state  <input class="form-control" value="authority_state " type="text" name="authority_state">
			
					authority_country_code  <input class="form-control"  value="authority_country_code " type="text" name="authority_country_code">
					authority_phone <input class="form-control"  value="authority_phone" type="text" name="authority_phone">
					authority_email<input class="form-control" value="authority_email" type="email" name="authority_email"> 
					authority_website <input class="form-control" value="authority_website" type="text" name="authority_website">
					
 
					institution <input class="form-control" value="institution" type="text">
					

					
					<div class="form-group"><label for="applicant_category">mode </label>
						<select class="form-control select2" data-placeholder="Enter Applicant's Mode" rel="select2" name="mode" tabindex="-1" aria-hidden="true">
							<option value="Active" class="form-control">Active Enrollment</option>
							<option value="Online" class="form-control">Online Enrollment</option>
						</select>
					</div>


					major_subject<input class="form-control" value="major_subject" name="major_subject" type="text"> 
					minor_subject  <input class="form-control" value="minor_subject" name="minor_subject"type="text">
					roll <input class="form-control" value="roll" type="text" name="roll">
					study_from <input class="form-control" value="study_from" name="study_from"type="date"> 
					study_to <input class="form-control" value="study_to" name="study_to"type="date"> 
					conferred_date<input class="form-control" value="conferred_date" name="conferred_date" type="date">  
					degree_issue_date<input class="form-control" value="degree_issue_date"  name="degree_issue_date" type="date"> 
					expected_degree_issue_date <input class="form-control" value="expected_degree_issue_date" name="expected_degree_issue_date" type="date">

					qualification_certificate <input class="form-control" id="up1" name="qualification_certificate" type="file">
					<a class="uploaded_file hide" onclick="document.getElementById('up1').value = '' "><i title="Remove File" class="fa fa-times" class=""></i></a><br>
					marksheet<input class="form-control" value="marksheet" id="up2" type="file" name="marksheet">
					<a class="uploaded_file hide" onclick="document.getElementById('up2').value = '' "><i title="Remove File" class="fa fa-times" class=""></i></a>
			</div>

			</div>

			<div class="modal-footer">
				
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection