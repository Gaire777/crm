@extends('layouts.app')
@section('title')
Health Edit
@endsection
@section('content')
<div class="container">
     @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
<form method="POST" action="{{route('admin.healthlicenseedit')}}" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate" enctype="multipart/form-data">
	@csrf



	
                     

                         <input type="hidden" class="form-control" name="id" placeholder="Applicant Name" value="{{$healthlicense->id}}">

                                    
                      <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Applicant</label>
                                    <select class="form-control select2-ajax select2" name="applicant_id"  tabindex="-1" aria-hidden="true">
                                         <option value="{{$healthlicense->applicant_id}}">Select</option>
                                        @foreach($applicant as $value)
                    
                                            <option value="{{$value->id}}">{{$value->first_name}} {{$value->surname}}</option>
                                            @endforeach
                    
                                    </select>
						<div class="form-group  col-md-12 ">
                                    
                      <label class="control-label" for="name">Professional Designation</label>
              		<input type="text" class="form-control" name="professional_designation" placeholder="Professional Designation" value="{{$healthlicense->professional_designation}}">

            		  </div>
					<div class="form-group  col-md-12 ">
                     <label class="control-label" for="name">Issuing Authority Name</label>                    
                     <input type="text" class="form-control" name="issuing_authority_name" placeholder="Issuing Authority Name" value="{{$healthlicense->issuing_authority_name}}">


                                    
                                                                                                        

 				<div class="form-group  col-md-12 ">
 					 <label class="control-label" for="name">Issuing Authority Country</label>
 					  <select class="form-control select2 select2" name="issuing_authority_country" data-select2-id="1" tabindex="-1"  aria-hidden= "true">



                            <option value="{{$healthlicense->issuing_authority_country}}">SELECT</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BE">Belgium</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BA">Bosnia and Herzegovina</option>
                            <option value="BB">Barbados</option>
                            <option value="WF">Wallis and Futuna</option>
                            <option value="BL">Saint Barthelemy</option>
                            <option value="BM">Bermuda</option>
                            <option value="BN">Brunei</option>
                            <option value="BO">Bolivia</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Nepal</option>


        </select>
         </div>
                                    
                                   
                                           

                                    
                                                                                                       
         <div class="form-group  col-md-12 ">
         	 <label class="control-label" for="name">Issuing Authority City</label>
 	  <input type="text" class="form-control" name="issuing_authority_city" placeholder="Issuing Authority City"   value="{{$healthlicense->issuing_authority_city}}">
         	   </div>
                                    
                                   
                                                                                                               

                                    
                                                                                                       
	<div class="form-group  col-md-12 ">
		<label class="control-label" for="name">License Conferred Date</label>
		 <input type="date" class="form-control" name="license_conferred_date" placeholder="License Conferred Date" value="{{$healthlicense->license_conferred_date}}">
		  </div>	
                                    
                                    
                                                                                                               

                                    
                                                                                                       			
	 <div class="form-group  col-md-12 ">
	 	 <label class="control-label" for="name">License Expiry Date</label>
	 	 <input type="date" class="form-control" name="license_expiry_date" placeholder="License Expiry Date" value="{{$healthlicense->license_expiry_date}}">
	 	 </div>	
                                    
                                   
                                                                                                                

                                    
                                                                                                        			
	
	<div class="form-group  col-md-12 ">
         <label class="control-label" for="name">License Type</label>
          <select class="form-control select2 select2" name="license_type" data-select2-id="4" tabindex="-1" aria-hidden="true">
          	<option value="Full Time" data-select2-id="6">Full Time</option>
                            <option value="Part Time">Part Time</option>
                        </select>
                        </div>
                                                                                                               
                                            

                                    
          

                                                                                                        
		
	<div class="form-group  col-md-12 ">
		<label class="control-label" for="name">License Number</label>
		 <input type="text" class="form-control" name="license_number" placeholder="License Number" value="{{$healthlicense->license_number}}">
		  </div>
                                    
                                    
                                                                                                               

                                    
                                                                                                       

	<div class="form-group  col-md-12 ">
  		<label class="control-label" for="name">License Status</label>
          	 <select class="form-control select2 select2" name="license_status" data-select2-id="7" tabindex="-1" aria-hidden="true">

                <!-- <option value="{{$healthlicense->licene_status}}">SELECT</option> -->

                
                <option value="{{$healthlicense->license_status}}">SELECT</option>
                <option value="Active" data-select2-id="9">Active</option>
                <option value="Inactive">Inactive</option>
               </select>

                                    
                                                                                                        </div>
    <div class="form-group  col-md-12 ">
                                    
                     <label class="control-label" for="name">License Attained</label>
         <input type="text" class="form-control" name="license_attained" placeholder="License Attained" value="{{$healthlicense->license_attained}}">

                                    
                                                                                                        </div>
                                                                                                        

<div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">License Copy</label>
                  <input type="file" name="license_copy" multiple="multiple" value="{{$healthlicense->license_copy}}">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                              

            
        
    
                                    
                                                                                                        </div>
			</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection		