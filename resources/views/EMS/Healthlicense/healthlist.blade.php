@extends('layouts.app')
@section('title')
Health List
@endsection

@section('css')

<style type="text/css">
	
	.health_table{

	
			overflow: hidden;
			overflow-x: scroll;

	}

</style>

@endsection


@section('page_heading')
	    	<h1>Health License List</h1>
@endsection

 @section('button')
	  <a href="{{route('admin.healthlicense.form')}}" ><button class="btn btn-success btn-sm " >Add Health License</button></a>
       
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
	    <div class="health_table table-responsive">
	<table id="datatable-basic" class="table table-bordered dataTable no-footer align-items-center" role="grid" aria-describedby="example1_info" style="width: 1365px;">
	
		<thead>

		<tr class="success" role="row">
			<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 14.0104px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">Id</th>
			<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 14.0104px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">Applicant Name</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 36.0104px;" aria-label="First name : activate to sort column ascending">Pro. Designation</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Issuing Authority</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Issuing Auth. Country</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Licence Conferred</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Licence Expiry date</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">License Number</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">License Status</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">License Type</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Licence Attained</th>
			<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 37.0104px;" aria-label="Last Name: activate to sort column ascending">Licence Copy</th>
			
			<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;" aria-label="Actions">Actions</th>
			 
		</tr>
		</thead>
		<tbody>
			@foreach($healthlicense as $value)

			
		<tr role="row" class="odd">
			<td class="sorting_1">{{$loop->iteration}}</td>
			
			<td>{{$value->applicant->first_name}} {{$value->applicant->surname}}</td>
			<td>{{$value->professional_designation}}</td>
			<td>{{$value->issuing_authority_name}}</td>
			<td>{{$value->issuing_authority_country}}</td>
			<td>{{$value->license_conferred_date}}</td>
			<td>{{$value->license_expiry_date}}</td>
			<td>{{$value->license_number}}</td>
			<td>{{$value->license_status}}</td>
			<td>{{$value->license_type}}</td>
			<td>{{$value->license_attained}}</td>
			
			<td>	@if($value->marksheet != null)
			<a href="{{ url('/storage/app/public/'.$value->license_copy) }}"target="_blank">MarkSheet
			@else
			No file found
			@endif</td>

		

			<td><span><a href="{{route('admin.healthlicenseeditform',$value->id)}}">Edit</a></span> ||
                            <span>
                            <a  href="{{route('admin.healthlicensedelete',$value->id)}}"  class=""  >Delete</a>
                            </span>
			
						
		</tr>
			@endforeach
	</table>
	{{$healthlicense->links()}}
	</div>
	</div>
</div>
@endsection
		
		