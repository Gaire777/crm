@extends('layouts.app')
@section('title')
Check List
@endsection

@section('css')

<style type="text/css">
  
  .checklist_table{

  
      overflow: hidden;
      overflow-x: scroll;

  }

</style>

@endsection


@section('page_heading')
  <h1>
              Checklist<small>Checklist listing</small>
  </h1>

@endsection

 @section('button')
           <span class="headerElems float-right">
              <a href="{{route('admin.checklist.add')}}" ><button class="btn btn-success btn-sm " >Add Checklist</button></a>
              </span>
       
@endsection
@section('content')

<div class="row">
  <div class="white-box container">
    <div class="checklist_table table-responsive">
  <table id="datatable-basic" class="table table-bordered">
        <thead>
     

        <tr>
        		<th class="form-control">applicant</th>
        		<th scope="col">password citizenship certificate</th>
        		<th scope="col">slc_certificate</th>
        		<th scope="col">+2/isc/pcl certificate</th>
        		<th scope="col">diploma degreecertificate  </th>
        		<th scope="col">mark sheet of each year final transcript  </th>
        		<th scope="col">equivalent certificate  </th>
        		<th scope="col">council registration certificate </th>
        		<th scope="col">council registration certificate_renew  </th>
        		<th scope="col">good standing_letter from_council  </th>
        		<th scope="col">work experience letter till_date  </th>
        		<th scope="col">basic life support for nurses </th>
        		<th scope="col">mrp size photo in white background  </th>
            <th scope="col">Action  </th>
        </tr>
        </thead>
        <tbody>
	@foreach($checklist as $value)
        <tr>
          <td> {{$value->applicants->first_name}}</td>
          <td>
            <div class="custom-control custom-checkbox">
              <input type="checkbox"  id="customCheck3" @if($value->password_citizenship_certificate == '1') checked @endif disabled> 
            </div>
          </td>	
          <td>
            <div class="custom-control custom-checkbox">
            <input type="checkbox"  id="customCheck3" @if($value->slc_certificate == '1') checked @endif disabled> 
            </div>
          </td>		
          <td>
            <div class="custom-control custom-checkbox">
            <input type="checkbox"  id="customCheck3" @if($value->plus_two_isc_pcl_certificate == '1') checked @endif disabled>  </div>
          </td>		
          <td>
            <div class="custom-control custom-checkbox">
            <input type="checkbox"  id="customCheck3" @if($value->diploma_degree_certificate == '1') checked @endif disabled>  </div>
          </td>		
          <td>
          <div class="custom-control custom-checkbox">
            <input type="checkbox"  id="customCheck3" @if($value->mark_sheet_of_each_year_final_transcript == '1') checked @endif disabled>  </div>
          </td>		
          <td>
            <div class="custom-control custom-checkbox">
            <input type="checkbox"  id="customCheck3" @if($value->equivalent_certificate == '1') checked @endif disabled>  </div>
          </td>		
          <td>
            <div class="custom-control custom-checkbox">
            <input type="checkbox"  id="customCheck3" @if($value->council_registration_certificate == '1') checked @endif disabled>  </div>
            </td>		
            <td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"  id="customCheck3" @if($value->council_registration_certificate_renew == '1') checked @endif disabled>  </div>
                          </td>		
              		<td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"  id="customCheck3" @if($value->good_standing_letter_from_council == '1') checked @endif disabled>  </div>
                          </td>		
              		<td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"  id="customCheck3" @if($value->work_experience_letter_till_date == '1') checked @endif disabled>  </div>
                          </td>		
              		<td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"  id="customCheck3" @if($value->basic_life_support_for_nurses == '1') checked @endif disabled>  </div>
                          </td>		
              		<td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"  id="customCheck3" @if($value->mrp_size_photo_in_white_background == '1') checked @endif disabled>  </div>
                          </td>	

                        <td>
                            <span>
                            <a  href="{{route('admin.checklist.edit',$value->id)}}"  >Edit </a>||
                            </span>
                             <span>
                            <a  href="{{route('admin.checklist.delete',$value->id)}}" >Delete</a>
                            </span>
            </td>	
              			
              		
              	</tr>
	@endforeach
	      </tbody>
	
      </table>
      {{$checklist->links()}}

    </div>
  </div>
</div>



@endsection


@section('css')
<style type="text/css">
.container {
  padding: 2rem 0rem;
}

h4 {
  margin: 2rem 0rem 1rem;
}

.table-image {
  td, th {
    vertical-align: middle;
  }
}
</style>

@endsection


             		
	