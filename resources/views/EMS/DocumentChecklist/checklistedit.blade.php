@extends('layouts.app')
@section('title')
Edit Checklist
@endsection
@section('content')


<form action="{{route('admin.checklist.edited')}}" method="post">
	@csrf




	<input type="hidden" name="id" value="{{$app_checklist->id}}">


<div class="form-group  col-md-12 container">
	<div class="row"><div class="col-12">

		<div class="form-group"><label for="applicant_category">applicants_name

					</label>
		<select class="form-control select2-hidden-accessible" data-placeholder="Enter Applicant's type" rel="select2" name="applicants_name" tabindex="-1" aria-hidden="true">

			<option value="{{$app_checklist->applicant_id}}" class="form-control">{{$app_checklist->applicants->first_name}} {{$app_checklist->applicants->surname}}</option>
					
			@foreach($checklist as $value)
						
				<option value="{{$value->id}}" class="form-control">{{$value->first_name}} {{$value->surname}}</option>


			@endforeach
						
		</select>
		</div>
    <br>
   
 <!--    <td>{{$value->applicant_id}}</td> -->
      <label>password_citizenship_certificate</label>
              <div class="custom-control custom-checkbox" >
                  <input type="checkbox" name="password_citizenship_certificate"  id="e1" value="1" @if($app_checklist->password_citizenship_certificate == '1') checked @endif > 
                   </div>
           

    <label>slc_certificate</label>
              <div class="custom-control custom-checkbox" >
                  <input type="checkbox"  name="slc_certificate"  id="e2" value="1" @if($app_checklist->slc_certificate == '1') checked @endif > 
                   </div>
             
    <label>plus_two_isc_pcl_certificate</label>
              <div class="custom-control custom-checkbox"   >
                  <input type="checkbox"  name="plus_two_isc_pcl_certificate"  id="e3" value="1" @if($app_checklist->plus_two_isc_pcl_certificate == '1') checked @endif >  </div>
               
    <label>diploma_degree_certificate</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="diploma_degree_certificate"  id="e4" value="1" @if($app_checklist->diploma_degree_certificate == '1') checked @endif >  </div>
               
    <label>mark_sheet_of_each_year_final_transcript</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="mark_sheet_of_each_year_final_transcript"  id="e5" value="1" @if($app_checklist->mark_sheet_of_each_year_final_transcript == '1') checked @endif >  </div>
               
    <label>equivalent_certificate</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="equivalent_certificate"  id="e6" value="1" @if($app_checklist->equivalent_certificate == '1') checked @endif >  </div>
               
    <label>council_registration_certificate</label> >
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="council_registration_certificate"  id="e7" value="1" @if($app_checklist->council_registration_certificate == '1') checked @endif >  </div>
               
    <label>council_registration_certificate_renew</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="council_registration_certificate_renew"  id="e8" value="1" @if($app_checklist->council_registration_certificate_renew == '1') checked @endif >  </div>
               
    <label>good_standing_letter_from_council</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="good_standing_letter_from_council"  id="e9" value="1" @if($app_checklist->good_standing_letter_from_council == '1') checked @endif >  </div>
               
    <label>work_experience_letter_till_date</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="work_experience_letter_till_date"  id="e10" value="1" @if($app_checklist->work_experience_letter_till_date == '1') checked @endif >  </div>
               
    <label>basic_life_support_for_nurses</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="basic_life_support_for_nurses"  id="e11" value="1" @if($app_checklist->basic_life_support_for_nurses == '1') checked @endif >  </div>
               
    <label>mrp_size_photo_in_white_background</label>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  name="mrp_size_photo_in_white_background"  id="e12" value="1" @if($app_checklist->mrp_size_photo_in_white_background == '1') checked @endif >  </div>
               
      	<input type="hidden" name="progress_sts" id="progress_sts">   
    
 
   	<button type="submit" class="form-control btn-primary" onclick="progressedit()" >Save</button>
</div>
</div></div>
</form>


 @endsection



 @section('js')
	<script type="text/javascript">

		 <!-- For Status bar jquery -->

		function progressedit(){
			// alert('hello');
			if ($('#e1').is(":checked"))
			{
			  var e1=parseInt($('#e1').val());
			  // console.log(a1);
			}

			if ($('#e2').is(":checked"))
			{
			  var e2=parseInt($('#e2').val());
			}
						if ($('#e3').is(":checked"))
			{
			  var e3=parseInt($('#e3').val());
			}
						if ($('#e4').is(":checked"))
			{
			  var e4=parseInt($('#e4').val());
			}
						if ($('#e5').is(":checked"))
			{
			  var e5=parseInt($('#e5').val());
			}
						if ($('#e6').is(":checked"))
			{
			  var e6=parseInt($('#e6').val());
			}
						if ($('#e7').is(":checked"))
			{
			  var e7=parseInt($('#e7').val());
			}
						if ($('#e8').is(":checked"))
			{
			  var e8=parseInt($('#e8').val());
			}
						if ($('#e9').is(":checked"))
			{
			  var e9=parseInt($('#e9').val());
			}
						if ($('#e10').is(":checked"))
			{
			  var e10=parseInt($('#e10').val());
			}
						if ($('#e11').is(":checked"))
			{
			  var e11=parseInt($('#e11').val());
			}
						if ($('#e12').is(":checked"))
			{
			  var e12=parseInt($('#e12').val());
			}


		  var sum=e1+e2+e3+e4+e5+e6+e7+e8+e9+e10+e11+e12;
		 //  // console.log(sum);
		  if (isNaN(sum)) {

		  		$("#progress_sts").val("0");
		  //		 console.log('valuefase');
		  }
		  else{
			$("#progress_sts").val("1");
		  //	console.log('valuetrue');
		  }




		
		 }	





	</script>
 @endsection
<!-- 
 		  // if (isNaN($("input[id^='e']"))) {

		  // 	console.log('hello');
		  // }
		  // else
		  // {
		  // 	console.log('sexy');
		  // } -->