@extends('layouts.app')
@section('title')
Add Checklist
@endsection

@section('content')


<form action="{{route('admin.checklist.added')}}" method="post">
	@csrf
<div class="form-group  col-md-12 container">
	<div class="row"><div class="col-12">

		<div class="form-group"><label for="applicant_category">applicants_name

					</label>
		<select class="form-control select2-hidden-accessible" data-placeholder="Enter Applicant's type" rel="select2" name="applicants_name" tabindex="-1" aria-hidden="true">
					
			@foreach($checklist as $value)
						
				<option value="{{$value->id}}" class="form-control">{{$value->first_name}} {{$value->surname}}

				</option>


			@endforeach
						
		</select>
		</div>
    <br>
    <label class="control-label" for="name">password_citizenship_certificate</label>
   	<input type="checkbox" name="password_citizenship_certificate" id="a1" class="toggleswitch" value="1">
   		
  	<br>
 	<label class="control-label" for="name">slc_certificate</label>
   	<input type="checkbox" name="slc_certificate" class="toggleswitch" id="a2" value="1"   >
    
 	<br>
 	<label class="control-label" for="name">plus_two_isc_pcl_certificate</label>
   	<input type="checkbox" name="plus_two_isc_pcl_certificate" id="a3" value="1">
 	<br>
 	
 	<label class="control-label" for="name">diploma_degree_certificate</label>
   	<input type="checkbox" name="diploma_degree_certificate" id="a4" value="1"  >
 	<br>
 	<label class="control-label" for="name">mark_sheet_of_each_year_final_transcript</label>
   	<input type="checkbox" name="mark_sheet_of_each_year_final_transcript" id="a5" value="1"  >
 	<br>
 	<label class="control-label" for="name">equivalent_certificate</label>
   	<input type="checkbox" name="equivalent_certificate" id="a6"  value="1" >
 	<br>
 	<label class="control-label" for="name">council_registration_certificate</label>
   	<input type="checkbox" name="council_registration_certificate" id="a7" value="1"  >
 	<br>
 	<label class="control-label" for="name">council_registration_certificate_renew</label>
   	<input type="checkbox" name="council_registration_certificate_renew" id="a8" value="1"  >
	<br>
	<label class="control-label" for="name">good_standing_letter_from_council</label>
   	<input type="checkbox" name="good_standing_letter_from_council" id="a9" value="1" >
	<br>
	<label class="control-label" for="name">work_experience_letter_till_date</label>
   	<input type="checkbox" name="work_experience_letter_till_date" id="a10" value="1">
	<br>
	<label class="control-label" for="name">basic_life_support_for_nurses</label>
   	<input type="checkbox" name="basic_life_support_for_nurses" id="a11"  value="1">
   	<br>
   	<label class="control-label" for="name">mrp_size_photo_in_white_background</label>
   	<input type="checkbox" name="mrp_size_photo_in_white_background" id="a12" value="1"  >

   	<input type="hidden" name="progress_sts" id="progress_sts">
<br><br><br>

   	<button type="submit" class="form-control btn-primary" onclick="progress()">Save</button>
</div>
</div></div>
</form>


 @endsection

 @section('js')
	<script type="text/javascript">

		function progress(){
			// alert('hello');
			if ($('#a1').is(":checked"))
			{
			  var a1=parseInt($('#a1').val());
			}


						if ($('#a2').is(":checked"))
			{
			  var a2=parseInt($('#a2').val());
			}
						if ($('#a3').is(":checked"))
			{
			  var a3=parseInt($('#a3').val());
			}
						if ($('#a4').is(":checked"))
			{
			  var a4=parseInt($('#a4').val());
			}
						if ($('#a5').is(":checked"))
			{
			  var a5=parseInt($('#a5').val());
			}
						if ($('#a6').is(":checked"))
			{
			  var a6=parseInt($('#a6').val());
			}
						if ($('#a7').is(":checked"))
			{
			  var a7=parseInt($('#a7').val());
			}
						if ($('#a8').is(":checked"))
			{
			  var a8=parseInt($('#a8').val());
			}
						if ($('#a9').is(":checked"))
			{
			  var a9=parseInt($('#a9').val());
			}
						if ($('#a10').is(":checked"))
			{
			  var a10=parseInt($('#a10').val());
			}
						if ($('#a11').is(":checked"))
			{
			  var a11=parseInt($('#a11').val());
			}
						if ($('#a12').is(":checked"))
			{
			  var a12=parseInt($('#a12').val());
			}
			// // if ($('#a13').is(":checked"))
			// // {
			// //   var a13=parseInt(('#a13').val());
			// // }

		  var sum=a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12;
		  // console.log(sum);
		  if (isNaN(sum)) {

		  		$("#progress_sts").val("0");
		  		 // console.log('hello');
		  }
		  else{
			$("#progress_sts").val("1");
		  	// console.log('sexy');
		  }





		 }	





	</script>
 @endsection