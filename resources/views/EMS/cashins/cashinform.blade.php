@extends('layouts.app')
@section('title')
Add Cashin
@endsection
@section('content')
<div class="container">
	 @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
<form method="POST" action="{{route('admin.cashin.add')}}" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf
	
                    <div class="form-group"><label for="applicant_category">Paid By:

					</label>
						<select class="form-control " data-placeholder="Enter Applicant's type" rel="select2" name="paidby" tabindex="-1" aria-hidden="true">
					
						@foreach($data1 as $value1)
						
							<option value="{{$value1->id}}" class="form-control">{{$value1->first_name}} {{$value1->surname}}

						</option>


						@endforeach
						
						
						
						</select>
					</div>
					 <div class="form-group"><label for="applicant_category">Service:

					</label>
						<select class="form-control " data-placeholder="Enter Applicant's type" rel="select2" name="service" tabindex="-1" aria-hidden="true">
					
						@foreach($data as $value)
						
							<option value="{{$value->id}}" class="form-control">{{$value->name}}

						</option>


						@endforeach
						
						
						
						</select>
					</div>

					<div class="form-group"><label for="applicant_category">Recieved By:

					</label>
						<select class="form-control " data-placeholder="Enter Applicant's type" rel="select2" name="receivedby" tabindex="-1" aria-hidden="true">
					
						@foreach($data2 as $value2)
						
							<option value="{{$value2->id}}" class="form-control">{{$value2->name}}

						</option>


						@endforeach
						
						
						
						</select>
					</div>

					<div class="form-group"><label for="lname">Amount Paid :</label>
						<input class="form-control" placeholder="Enter Price" data-rule-maxlength="256" name="amount_paid" type="price" value=""></div>

						<div class="form-group"><label for="lname">Date:</label>
						<input class="form-control" placeholder="Enter Price" data-rule-maxlength="256" name="date_paid" type="date" value=""></div>

						<div class="form-group"><label for="lname">Remarks:</label>
						 <textarea class="form-control" name="remarks" rows="5"></textarea>


					
					
			</div>
			
					
					
			</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection