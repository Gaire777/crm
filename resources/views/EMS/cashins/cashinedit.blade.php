@extends('layouts.app')
@section('content')
@section('title')
Cashin Edit
@endsection
<div class="container">
	 @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
<form method="POST" action="{{route('admin.cashin.edit')}}" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	@csrf			

				<input type="hidden" name="id" value="{{$cashin->id}}">
	
                    <div class="form-group"><label for="applicant_category">Paid By:

					</label>

						<select class="form-control " data-placeholder="Enter Applicant's type" rel="select2" name="paidby" tabindex="-1" aria-hidden="true" disabled="">

							
					
						@foreach($data1 as $value1)
						
							<option value="{{$cashin->paidby}}" class="form-control">{{$cashin->applicants->first_name}} {{$cashin->applicants->surname}}

						</option>


						@endforeach
						
						
						
						</select>
					</div>
					 <div class="form-group"><label for="applicant_category">Service:

					</label>
						<select class="form-control " data-placeholder="Enter Applicant's type" rel="select2" name="service" tabindex="-1" aria-hidden="true">

							<option value="{{$cashin->service}}" class="form-control">select

						</option>
					
						@foreach($data as $value)
						
							<option value="{{$value->id}}" class="form-control">{{$value->name}}

						</option>


						@endforeach
						
						
						
						</select>
					</div>

					<div class="form-group"><label for="applicant_category">Recieved By:

					</label>
						<select class="form-control" data-placeholder="Enter Applicant's type" rel="select2" name="receivedby" tabindex="-1" aria-hidden="true">
					
						<option value="{{$cashin->receivedby}}" class="form-control">select

						</option>
						@foreach($data2 as $value2)
						
							<option value="{{$value2->id}}" class="form-control">{{$value2->name}}

						</option>


						@endforeach
						
						
						
						</select>
					</div>

					<div class="form-group"><label for="lname">Amount Paid :</label>
						<input class="form-control" placeholder="Enter Price" data-rule-maxlength="256" name="amount_paid" type="price" value="{{$cashin->amount_paid}}"></div>

						<div class="form-group"><label for="lname">Date:</label>
						<input class="form-control" placeholder="Enter Price" data-rule-maxlength="256" name="date_paid" type="date" value="{{$cashin->date_paid}}"></div>

						<div class="form-group"><label for="lname">Remarks:</label>
						 <textarea class="form-control" name="remarks" rows="5" value="{{$cashin->remarks}}"></textarea>


					
					
			</div>
			
					
					
			</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" value="Submit">
			</div>

			</form>
			</div>
@endsection