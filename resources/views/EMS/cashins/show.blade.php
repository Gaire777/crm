@extends('layouts.app')
@section('title')
Cashin List
@endsection

@section('page_heading')
            <h1>
            Cashins Listing
            </h1>

@endsection

 @section('button')
          
            <a href="{{route('admin.cashin.add')}}" ><button class="btn btn-success btn-sm " >Add Cashins</button></a>
       
@endsection







@section('content')


    <div class="row helo">
        <div class="white-box">
            <table class="table table-bordered  " id="user-datatable">
                <thead>
                    {{--<tr>--}}
                        <th>S.N.</th>
                        <th>Paid By</th>
                        <th>Service</th>
                        <th>Service Charge</th>
                        <th>Recieved By</th>
                        <th>Date</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    {{--</tr>--}}
                </thead>

                <tbody>

                @foreach($cashin as $value)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$value->applicants->first_name}} {{$value->applicants->surname}}</td>
                        <td>{{$value->Service->name}}</td>
                        <td>{{$value->amount_paid}}</td>
                        <td>{{$value->users->name}}</td>
                        <td>{{$value->date_paid}}</td>
                        <td>{{$value->remarks}}</td>
                        <td><span><a href="{{route('admin.cashineditform',$value->id)}}">Edit</a></span> ||
                            <span>
                            <a  href="{{route('admin.cashin.delete',$value->id)}}">Delete</a>
                            </span>
                            <!-- Modal -->
                        </td>


                    </tr>
                  
                @endforeach
                </tbody>
            </table>
        </div>
        @endsection
       
        
