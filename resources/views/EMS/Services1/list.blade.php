@extends('layouts1.app')


@section('title','List Service')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="header-icon">
                <i class="fa fa-users"></i>
            </div>
            <div class="header-title">
                <h1>Service </h1>
                <small>Service List</small>
            </div>
        </section>
    <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                                 <h4>Service List</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="btn-group">
                              <div class="buttonexport" id="buttonlist">
                                 <a class="btn btn-add" href="{{route('admin.service.form')}}"> <i class="fa fa-plus"></i> New Services
                                 </a>

                              </div>


                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Id</th>
                                       <th>Name</th>
                                       <th>Price</th>
                                       <th>Action</th>

                                    </tr>
                                 </thead>
                                 <tbody>
                                 @foreach($service as $value)
                                    <tr>
                                       <td>{{$loop->iteration}}</td>

                                       <td>{{$value->name}}</td>
                                       <td>{{$value->price}}</td>
                                       <td>
                                          <a href="{{route('admin.serviceeditform',$value->id)}}"><i class="fa fa-pencil"></i></a>
                                          <button href="javascript:" rel="{{ $value->id }}" rel1="delete-service" class="btn btn-danger btn-xs deleteRecord" >
                                          <i class="fa fa-trash-o "></i>
                                          </button>
                                       </td>
                                    </tr>
                                @endforeach

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </section>
    </div>

@endsection

@section('scripts1')

<script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif

        @if(session('flash_error'))
        swal("Error", "{!! session('flash_error') !!}")
        @endif
    </script>

@endsection
