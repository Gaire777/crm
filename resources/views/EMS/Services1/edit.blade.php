
@extends('layouts1.app')

@section('content')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="header-icon">
   <i class="fa fa-id-card-o" aria-hidden="true" style="color: #027a32;"></i>
   </div>
   <div class="header-title">
      <h1>Edit Services</h1>
      <small>Service list</small>
   </div>
      <section class="content">
         <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
               <div class="panel panel-bd lobidrag">
                  <div class="panel-heading">
                     <div class="btn-group" id="buttonlist"> 
                        
                        <i class="fa fa-list"></i>Edit Service 
                     </div>
                  </div>
                  <div class="panel-body">
                     <form class="col-sm-6" method="post" action="{{route('admin.serviceedit')}}">
                     @csrf
                     <input type="hidden" name="id" value="{{$service->id}}">
                        <div class="form-group">
                           <label>Name</label>
                           <input type="text" name="servicename" class="form-control"  placeholder="Enter Name" value="{{$service->name}}" required >
                        </div>
                        <div class="form-group">
                           <label>Price</label>
                           <input type="price" name="price" class="form-control"  placeholder="Enter Price" value="{{$service->price}}" required>
                        </div>
                        


                        <div class="reset-button">
                           <button class="btn btn-success" type="submit" value="Submit">Submit</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
                  </section>
               </div>
            @endsection