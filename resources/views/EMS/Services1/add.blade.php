
@extends('layouts1.app')

@section('content')
<div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
               <i class="fa fa-id-card-o" aria-hidden="true" style="color: #027a32;"></i>
               </div>
               <div class="header-title">
                  <h1>Add Services</h1>
                  <small>Service list</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <!-- <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="#"> 
                              <i class="fa fa-list"></i>  Service Details </a>  
                           </div> -->
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="POST" action="{{route('admin.service.add')}}" accept-charset="UTF-8" id="applicant-add-form" novalidate="novalidate">
	                        @csrf
                              <div class="form-group">
                                 <label>Service Name</label>
                                 <input type="text" class="form-control"  placeholder="Enter Service name " data-rule-maxlength="256" required="1" name="servicename" type="text" value="{{ old('servicename') }}" aria-required="true">
                              </div>
                              <div class="form-group">
                                 <label>Price</label>
                                 <input type="text" class="form-control"placeholder="Enter Price" data-rule-maxlength="256" name="price" type="price" value="{{ old('price') }}">
                              </div>
                              
                              <div class="reset-button">
                                 <button type="submit" class="btn btn-success">Submit</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
@endsection