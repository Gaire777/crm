@extends('layouts1.app')
@section('title')
Category List
@endsection
@section('content')


<div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
               <i class="fa fa-list" aria-hidden="true" style="color: red;"></i>
               </div>
               <div class="header-title">
                  <h1>Categories</h1>
                  <small>Category List</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="{{route('admin.category.form')}}">
                                 <h4>Add Category</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr class="info">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 14.0104px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">Id</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 36.0104px;" aria-label="First name : activate to sort column ascending">Category Name</th>
                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;" aria-label="Actions">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($category as $value)
                                    <tr>
                                       <td>{{$loop->iteration}}</td>
                                       <td>{{$value->name}}</td>
                                       <td>
                                       <span><a href="{{route('admin.categoryeditform',$value->id)}}"><button class="btn-sm btn-danger">Edit</button></a></span>
                                        <span>
                                        <a href="{{route('admin.categorydelete',$value->id)}}" class=""><button class="btn-sm btn-danger"> Delete </button></a>
                                        <a class="btn btn-sm btn-primary" data-toggle="collapse" href="#collapsem{{$value['id']}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                           Send Mail to Applicants
                                        </a>


                                        <a class="btn btn-sm btn-primary" data-toggle="collapse" href="#collapse{{$value['id']}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Send SMS to Applicant
                                        </a>

                                        <a class="btn btn-sm btn-primary" data-toggle="collapse" href="#collapse-ene{{$value['id']}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Send Mail to Enquiry
                                        </a>


                                        <a class="btn btn-sm btn-primary" data-toggle="collapse" href="#collapse-enm{{$value['id']}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Send SMS to Enquiry
                                        </a>

                <div class="collapse mt-3" id="collapse{{$value['id']}}">
                  <div class="card card-body bg-light">
                    <form method="post" action="{{ route('admin.sendgroupsms')}}" autocomplete="on">
                      @csrf
                      <h6 class="heading-small mb-4">Send SMS</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col">
                            <input type="hidden" name="id" value="{{$value->id}}">
                            <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                              <label class="form-control-label" for="message">Type your Message</label>
                              <textarea rows="5" name="message" id="message" class="form-control form-control-alternative{{ $errors->has('message') ? ' is-invalid' : '' }}" placeholder="{{ __('Write Your Message here') }}" value=""> </textarea>

                              @if ($errors->has('message'))
                              <span class="invalid-sms" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                              </span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="text-right">
                          <button type="submit" class="btn btn-success mt-4"><i class="fa fa-paper-plane"></i> Send</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div class="collapse mt-3" id="collapsem{{$value['id']}}">
                  <div class="card card-body bg-light">
                    <form method="post" action="{{ route('admin.sendgroupemail')}}" autocomplete="on">
                      @csrf
                      <h6 class="heading-small mb-4">Send email</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col">
                            <input type="hidden" name="id" value="{{$value->id}}">
                            <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                              <label class="form-control-label" for="message">Type your Email</label>
                              <textarea rows="5" name="message" id="message" class="form-control form-control-alternative{{ $errors->has('message') ? ' is-invalid' : '' }}" placeholder="{{ __('Write Your Message here') }}" value=""> </textarea>

                              @if ($errors->has('message'))
                              <span class="invalid-sms" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                              </span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="text-right">
                          <button type="submit" class="btn btn-success mt-4"><i class="fa fa-paper-plane"></i> Send</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div class="collapse mt-3" id="collapse-enm{{$value['id']}}">
                  <div class="card card-body bg-light">
                    <form method="post" action="{{ route('admin.sendgroupsms-en')}}" autocomplete="on">
                      @csrf
                      <h6 class="heading-small mb-4">Send SMS</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col">
                            <input type="hidden" name="id" value="{{$value->id}}">
                            <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                              <label class="form-control-label" for="message">Type your Message</label>
                              <textarea rows="5" name="message" id="message" class="form-control form-control-alternative{{ $errors->has('message') ? ' is-invalid' : '' }}" placeholder="{{ __('Write Your Message here') }}" value=""> </textarea>

                              @if ($errors->has('message'))
                              <span class="invalid-sms" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                              </span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="text-right">
                          <button type="submit" class="btn btn-success mt-4"><i class="fa fa-paper-plane"></i> Send</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div class="collapse mt-3" id="collapse-ene{{$value['id']}}">
                  <div class="card card-body bg-light">
                    <form method="post" action="{{ route('admin.sendgroupemail-en')}}" autocomplete="on">
                      @csrf
                      <h6 class="heading-small mb-4">Send email</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col">
                            <input type="hidden" name="id" value="{{$value->id}}">
                            <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                              <label class="form-control-label" for="message">Type your Email</label>
                              <textarea rows="5" name="message" id="message" class="form-control form-control-alternative{{ $errors->has('message') ? ' is-invalid' : '' }}" placeholder="{{ __('Write Your Message here') }}" value=""> </textarea>

                              @if ($errors->has('message'))
                              <span class="invalid-sms" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                              </span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="text-right">
                          <button type="submit" class="btn btn-success mt-4"><i class="fa fa-paper-plane"></i> Send</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </span>                                       
            
            </td>
        </tr>
    </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /.content -->
</div>
@endsection