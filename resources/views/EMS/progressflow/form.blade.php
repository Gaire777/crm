

@extends('layouts.app')
@section('title')
 Add New 
@endsection
@section('page_heading')
    <h3> Add New </h3>
@endsection

<div class="container">
     @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif
@section('content')

<form role="form" class="form-edit-add" action="{{route('admin.progressflow.add')}}" method="POST" enctype="multipart/form-data">
     @csrf
                          <!-- PUT Method if we are editing -->
                        
                        <!-- CSRF TOKEN -->
                      

                       
                        <!-- PUT Method if we are editing -->
                        
                        <!-- CSRF TOKEN -->
                      

                        <div class="panel-body">

                            
                            <!-- Adding / Editing -->
                            
                                                            <!-- GET THE DISPLAY OPTIONS -->
                             <div class="form-group  col-md-12 ">
                                    
                                 <label class="control-label" for="name">Applicant Name</label>
                         <select class="form-control " name="applicant_id"   aria-hidden="true">

                                                @foreach($applicant as $value)
                             
                                            <option value="{{$value->id}}"> {{$value->first_name}} {{$value->surname}}</option>
                                            @endforeach
                                        </select>

            
        
    
                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Profession</label>
                                <select class="form-control select2 " name="profession" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                            <option value="Registered Nurse" data-select2-id="3">Registered Nurse</option>
                            <option value="Assistant Nurse">Assistant Nurse</option>
                            <option value="MBBS Doctor">MBBS Doctor</option>
                            <option value="BBS Doctor">BBS Doctor</option>
                            <option value="Specialist Doctor">Specialist Doctor</option>
                            <option value="Allied Health">Allied Health</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                

                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Contact Number</label>
                                 <input type="text" class="form-control" name="contact_number" placeholder="Contact Number" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Date Of Birth</label>
                                    <input type="date" class="form-control" name="date_of_birth" placeholder="Date Of Birth" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Passport Number</label>
                                    <input type="text" class="form-control" name="passport_number" placeholder="Passport Number" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Signed By Applicant</label>
                                 <select class="form-control select2 " name="signed_by_applicant" data-select2-id="4" tabindex="-1" aria-hidden="true">
                                            <option value="Yes" data-select2-id="6">Yes</option>
                            <option value="No">No</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="5" style="width: 100%;"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Signed Document</label>
                                     <input type="file" name="signed_docs" multiple="multiple">

                                    
                                <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="If Applicant has signed the service aggrement &amp; has accepted service charge"></span>
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Service Charge</label>
                                          <input type="text" class="form-control" name="service_charge" placeholder="Service Charge" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Paid Date</label>
                                                     <input type="date" class="form-control" name="service_paid_date" placeholder="Paid Date" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Mode Of Payment</label>
                                             <select class="form-control select2 select2-hidden-accessible" name="service_mode_of_payment" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                            <option value="Cash" data-select2-id="9">Cash</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Bank Deposite">Bank Deposite</option>
                            <option value="E-Sewa">E-Sewa</option>
                            <option value="IME">IME</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="8" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Received By</label>
                                             <input type="text" class="form-control" name="service_charge_received_by" placeholder="Received By" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHAMCQ Fee</label>
                                                                                                                <input type="text" class="form-control" name="dhamcq_fee" placeholder="DHAMCQ Fee" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Mode Of Payment</label>
                                         <select class="form-control e" name="dhamcq_mode_of_payment" data-select2-id="10" tabindex="-1" aria-hidden="true">
                                            <option value="Cash" data-select2-id="12">Cash</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Bank Deposite">Bank Deposite</option>
                            <option value="E-Sewa">E-Sewa</option>
                            <option value="IME">IME</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="11" style="width: 100%;"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHAMCQ Subject</label>
                                                <input type="text" class="form-control" name="dhamcq_subject" placeholder="DHAMCQ Subject" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHAMCQ Username</label>
                                     <input type="text" class="form-control" name="dhamcq_username" placeholder="DHAMCQ Username" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHAMCQ Password</label>
                                                                                                                <input type="text" class="form-control" name="dhamcq_password" placeholder="DHAMCQ Password" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHAMCQ Email Sent</label>
                                                                                                                <select class="form-control select2" name="dhamcq_email_sent" data-select2-id="13" tabindex="-1" aria-hidden="true">
                                            <option value="Yes" data-select2-id="15">Yes</option>
                            <option value="No">No</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="14" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Bls Training Completed Date</label>
                                                                                                                <input type="date" class="form-control" name="bls_training_completed_date" placeholder="Bls Training Completed Date" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Good Standing Certificate Issue Date</label>
                                                                                                                <input type="date" class="form-control" name="good_standing_certificate_issue_date" placeholder="Good Standing Certificate Issue Date" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Equivalent Certificate (PCL Only)</label>
                                                                                                                <select class="form-control select2 select2-hidden-accessible" name="equivalent_certificate" data-select2-id="16" tabindex="-1" aria-hidden="true">
                                            <option value="Yes" data-select2-id="18">Yes</option>
                            <option value="No">No</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="17" style="width: 100%;"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Books Provided</label>
                                                                                                                <select class="form-control select2" name="books_provided" data-select2-id="19" tabindex="-1" aria-hidden="true">
                                            <option value="Yes" data-select2-id="21">Yes</option>
                            <option value="No">No</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="20" style="width: 100%;"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Email Account</label>
                                                                                                                <input type="text" class="form-control" name="dha_email_account" placeholder="DHA Email Account" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Unique Id</label>
                                                                                                                <input type="text" class="form-control" name="dha_unique_id" placeholder="DHA Unique Id" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Username</label>
                                                                                                                <input type="text" class="form-control" name="dha_username" placeholder="DHA Username" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Password</label>
                                                                                                                <input type="text" class="form-control" name="dha_password" placeholder="DHA Password" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Application Ref Number</label>
                                                                                                                <input type="text" class="form-control" name="dha_application_ref_number" placeholder="DHA Application Ref Number" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Fees First Installment</label>
                                                                                                                <input type="text" class="form-control" name="dha_fees_first_installment" placeholder="DHA Fees First Installment" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Paid Date</label>
                                                                                                                <input type="date" class="form-control" name="first_installment_paid_date" placeholder="Paid Date" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Mode Of Payment</label>
                                                                                                                <select class="form-control select2 " name="first_installment_mode_of_payment" data-select2-id="22" tabindex="-1" aria-hidden="true">
                                            <option value="Cash" data-select2-id="24">Cash</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Bank Deposite">Bank Deposite</option>
                            <option value="E-Sewa">E-Sewa</option>
                            <option value="IME">IME</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="23" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Received By</label>
                                                                                                                <input type="text" class="form-control" name="first_installment_received_by" placeholder="Received By" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Fees Second Installment</label>
                                                                                                                <input type="text" class="form-control" name="dha_fees_second_installment" placeholder="DHA Fees Second Installment" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Paid Date</label>
                                                                                                                <input type="date" class="form-control" name="second_installment_paid_date" placeholder="Paid Date" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Mode Of Payment</label>
                                                                                                                <select class="form-control select2 select2-hidden-accessible" name="second_installment_mode_of_payment" data-select2-id="25" tabindex="-1" aria-hidden="true">
                                            <option value="Cash" data-select2-id="27">Cash</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Bank Deposite">Bank Deposite</option>
                            <option value="E-Sewa">E-Sewa</option>
                            <option value="IME">IME</option>
                        </select><span class="select2 select2-container " dir="ltr" data-select2-id="26" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Second Installment Received By</label>
                                                                                                                <input type="text" class="form-control" name="second_installment_received_by" placeholder="Second Installment Received By" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Dataflow Email</label>
                                                                                                                <input type="text" class="form-control" name="dataflow_email" placeholder="Dataflow Email" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Dataflow Username</label>
                                                                                                                <input type="text" class="form-control" name="dataflow_username" placeholder="Dataflow Username" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Dataflow Password</label>
                                                                                                                <input type="text" class="form-control" name="dataflow_password" placeholder="Dataflow Password" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Dataflow Ref No</label>
                                                                                                                <input type="text" class="form-control" name="dataflow_ref_no" placeholder="Dataflow Ref No" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">DHA Exam Eligibility Id</label>
                                                                                                                <input type="text" class="form-control" name="dha_exam_eligibility_id" placeholder="DHA Exam Eligibility Id" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Eligibility Date</label>
                                                                                                                <input type="date" class="form-control" name="eligibility_date" placeholder="Eligibility Date" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Exam Date Confirmed</label>
                                                                                                                <input type="date" class="form-control" name="exam_date_confirmed" placeholder="Exam Date Confirmed" value="">

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Send Confirmation To Candidate</label>
                                                                                                                <select class="form-control select2 " name="send_confirmation_to_candidate" data-select2-id="28" tabindex="-1" aria-hidden="true">
                                            <option value="Email" data-select2-id="30">Email</option>
                            <option value="SMS">SMS</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="29" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Exam Result</label>
                                                                                                                <select class="form-control select2 " name="exam_result" data-select2-id="31" tabindex="-1" aria-hidden="true">
                                            <option value="Not Given" data-select2-id="33">Not Given</option>
                            <option value="pass">Pass</option>
                            <option value="failed">Failed</option>
                        </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="32" style="width: 100%;"><span class="selection"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Data Flow Report</label>
                                                                                                                <select class="form-control select2 " name="data_flow_report" data-select2-id="34" tabindex="-1" aria-hidden="true">
                                            <option value="completed" data-select2-id="36">Completed</option>
                            <option value="Pending">Pending</option>
                            <option value="More Docs Required">More Docs Required</option>
                        </select>

                                    
                                                                                                        </div>
                                                            <!-- GET THE DISPLAY OPTIONS -->
                                                                
                                <div class="form-group  col-md-12 ">
                                    
                                    <label class="control-label" for="name">Remarks</label>
                                                                                                                <textarea class="form-control" name="remarks" rows="5"></textarea>

                                    
                                                                                                        </div>
                            
                        </div><!-- panel-body -->

             
                        <div class="panel-footer">
               <input class="btn btn-success" type="submit" value="Submit">
                    </form>
                    </div>

              
                    
@endsection