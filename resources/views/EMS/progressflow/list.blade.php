@extends('layouts.app')
@section('title')
List
@endsection

@section('css')

<style type="text/css">
	
	.progress_table{

	
			overflow: hidden;
			overflow-x: scroll;

	}

</style>

@endsection


@section('page_heading')
	    	<h1>
	        Progress Flow Report </h1>

@endsection

 @section('button')
	      
        <a href="{{route('admin.progressflow.add')}}" class="btn btn-primary"><i class="fa fa-plus">  Add New</i></a>
       
@endsection

@section('content')
<section class="content ">



	<div class="row">
	    <div class="col-sm-12">
	        <div class="progress_table table-responsive">
  <table id="datatable-basic" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="example1_info" style="width: 1546px;">

		<thead>

		      <th>S.N.</th>
            <th>Applicant Name</th>
            <th>profession</th>
            <th>Email</th>          
            <th>Contact_number</th>
            <th>Date_of_birth</th>
            <th>Passport_number</th>
            <th>Signed_by_applicant</th>
            <th>Signed_docs</th>
            <th>Service_charge</th>
            <th>Service_paid_date</th>
            <th>Service_mode_of_payment</th>
            <th>Service_charge_received_by</th>
            <th>Good_standing_certificate_issue_date</th>
            <th>Dhamcq_fee</th>
            <th>Dhamcq_mode_of_payment</th>
            <th>Dhamcq_subject</th>
            <th>Dhamcq_username</th>
            <th>Dhamcq_password</th>
            <th>Dhamcq_email_sent</th>
            <th>Books_provided</th>
            <th>Bls_training_completed_date</th>
            <th>Good_standing_certificate_issue_date</th>
            <th>Equivalent_certificate</th>
            <th>Dha_email_account</th>
            <th>Dha_unique_id</th>
            <th>Dha_username</th>
            <th>Dha_password</th>
            <th>Dha_application_ref_number</th>
            <th>Dha_fees_first_installment</th>
            <th>First_installment_paid_date</th>
            <th>First_installment_mode_of_payment</th>
            <th>First_installment_received_by</th>
            <th>Dha_fees_second_installment</th>
            <th>Second_installment_paid_date</th>
            <th>Second_installment_mode_of_payment</th>
            <th>Second_installment_received_by</th>
           <th>Dataflow_email</th>
            <th>Dataflow_username</th>
            <th>Dataflow_password</th>
            <th>Dataflow_ref_no</th>
            <th>Dha_exam_eligibility_id</th>
            <th>Eligibility_date</th>
            <th>Exam_date_confirmed</th>
            <th>Send_confirmation_to_candidate</th>
            <th>Exam_result</th>
            <th>Data_flow_report</th>
             <th>Remarks</th>
            <th>Action</th>
                
                </thead>
                <tbody>
                	
 @foreach($progressflow as $value)
           <tr>
                         
           <td>{{$loop->iteration}}</td>
            <td>{{$value->applicant->first_name}} {{$value->applicant->surname}}</td>
            <td>{{$value->profession}}</td>
            <td>{{$value->email}}</td>
            <td>{{$value->contact_number}}</td>
            <td>{{$value->date_of_birth}}</td>
            <td>{{$value->passport_number}}</td>
            <td>{{$value->signed_by_applicant}}</td>
            
            <td> @if($value->signed_docs != null)
			<a href="{{url('/storage/app/public/'.$value->signed_docs)}}"target="_blank">Signed Docs
			@else
			No file found
			@endif
            </td>
            
            <td>{{$value->service_charge}}</td>

            <td>{{$value->service_paid_date}}</td>
            <td>{{$value->service_mode_of_payment}}</td>
            <td>{{$value->service_charge_received_by}}</td>
            <td>{{$value->good_standing_certificate_issue_date}}</td>
            <td>{{$value->dhamcq_fee}}</td>
            <td>{{$value->dhamcq_mode_of_payment}}</td>
            <td>{{$value->dhamcq_subject}}</td>
            <td>{{$value->dhamcq_username}}</td>
            <td>{{$value->dhamcq_password}}</td>
            <td>{{$value->dhamcq_email_sent}}</td>
            <td>{{$value->books_provided}}</td>
            <td>{{$value->bls_training_completed_date}}</td>
            <td>{{$value->good_standing_certificate_issue_date}}</td>
            <td>{{$value->equivalent_certificate}}</td>
            <td>{{$value->dha_email_account}}</td>
            <td>{{$value->dha_unique_id}}</td>
            <td>{{$value->dha_username}}</td>
            <td>{{$value->dha_password}}</td>
            <td>{{$value->dha_application_ref_number}}</td>
            <td>{{$value->dha_fees_first_installment}}</td>
            <td>{{$value->first_installment_paid_date}}</td>
            <td>{{$value->first_installment_mode_of_payment}}</td>
            <td>{{$value->first_installment_received_by}}</td>
            <td>{{$value->dha_fees_second_installment}}</td>
            <td>{{$value->second_installment_paid_date}}</td>
            <td>{{$value->second_installment_mode_of_payment}}</td>
            <td>{{$value->second_installment_received_by}}</td>
           <td>{{$value->dataflow_email}}</td>
            <td>{{$value->dataflow_username}}</td>
            <td>{{$value->dataflow_password}}</td>
            <td>{{$value->dataflow_ref_no}}</td>
            <td>{{$value->dha_exam_eligibility_id}}</td>
            <td>{{$value->eligibility_date}}</td>
            <td>{{$value->exam_date_confirmed}}</td>
            <td>{{$value->send_confirmation_to_candidate}}</td>
            <td>{{$value->exam_result}}</td>
            <td>{{$value->data_flow_report}}</td>
            <td>{{$value->remarks}}</td>
            <td>
                  <a href="{{route('admin.edit',$value->id)}}">Edit</a>||
                  <a  href="{{route('admin.delete-progressflow',$value->id)}}">Delete</a></td>

@endforeach

                </tbody>
            </section>
            </table>
            {{$progressflow->links()}}
            </div>
            </div>
            </div>



@endsection
@section('scripts')
<script>
        $('#deleteUser').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id');
            var name = button.data('name');
            // var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.id').val(id);
            modal.find('.id').text(name);
        })
    </script>

@endsection