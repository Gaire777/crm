@extends('layouts.app')

@section('title')
Dashboard
@endsection

@section('content')

@include('layouts1.headers1.top', ['title' => ('Dashboard')])

<div class="container-fluid mt--7">
  <div class="row mb-4">
    <div class="col-lg-6">
      <a href="#">
        <div class="card py-3 bg-gradient-green">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-white"><a href="{{route('admin.applicant.list')}}">Total Applicant</a></h5>
                <span class="h2 font-weight-bold mb-0 text-white">{{$app_count}}</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                  <i class="fa fa-users" style="color: #412ab7;"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <div class="col-lg-6">
      <a href="#">
        <div class="card py-3 bg-gradient-indigo">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-white"><a href="{{route('admin.enquiries.list')}}">Total Enquiries</a></h5>
                <span class="h2 font-weight-bold mb-0 text-white">{{$enq_count}}</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                  <i class="fas fa-id-card-alt" style="color: #027a32;"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

   <div class="row mb-4">
    <div class="col-lg-6">
      <a href="#">
        <div class="card py-3 bg-gradient-primary">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-white"><a href="{{route('admin.cashin.list')}}">Total Cashins</a></h5>
                <span class="h2 font-weight-bold mb-0 text-white">{{$cashins}}</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                  <i class="fas fa-funnel-dollar" style="color: red"></i></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>   
    <div class="col-lg-6">
      <a href="#">
        <div class="card py-3 bg-gradient-default">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-white"><a href="{{route('admin.cashout.list')}}">Total Cashouts</a></h5>
                <span class="h2 font-weight-bold mb-0 text-white">{{$cashouts}}</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                 <i style="color: blue" class="fas fa-hand-holding-usd">    </i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
</div>
    <!-- <div class="col-lg-4">
      <a href="#">
        <div class="card py-3 bg-gradient-green">
         
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Booking</h5>
                <span class="h2 font-weight-bold mb-0 text-white">applicantss</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                  <i class="fas fa-user-tie" style="color: indigo;"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div> -->
    <!-- <div class="col-lg-4">
      <a href="#">
        <div class="card py-3 bg-gradient-primary">
          
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Bookings</h5>
                <span class="h2 font-weight-bold mb-0 text-white">booking</span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                  <i class="fas fa-calendar-check" style="color: darkgreen;"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div> -->
    
 

  

  @include('layouts.footers.auth')
</div>
@endsection

@push('js')
<script src="{{ asset('argon/vendor/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{ asset('argon/vendor/chart.js/dist/Chart.extension.js')}}"></script>
@endpush