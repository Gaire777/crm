<!-- jQuery, Bootstrap JS, Cookie JS -->
<script src="{{ asset('argon/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('argon/vendor/js-cookie/js.cookie.js')}}"></script>
<script src="{{ asset('argon/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
<script src="{{ asset('argon/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>



<!-- Chart JS -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src="{{ asset('argon/vendor/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{ asset('argon/vendor/chart.js/dist/Chart.extension.js')}}"></script>

<!-- Custom JS -->
@yield('js')


<!-- Argon JS -->
<script src="{{ asset('argon/js/argon.min.js')}}"></script>

<!-- datatable -->
<script src="{{asset('argon/vendor/datatable/js/jquery.dataTable.min.js')}}"></script>
<script src="{{asset('argon/vendor/datatable/js/dataTable.bootstrap.min.js')}}"></script>

<script>
 $(document).ready(function() {
  //  datatable
   $('#datatable-basic').DataTable();
});
 </script>

