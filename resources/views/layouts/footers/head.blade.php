<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>EMS | @yield('title')</title>

  <!-- Favicon -->
  <link href="{{ asset('argon/img/brand/favicon.ico')}}" rel="icon" type="image/png">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

  <!-- Icons -->
  <link href="{{ asset('argon/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('argon/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('argon/vendor/datatable/css/dataTable.bootstrap4.min.css')}}">

  <!-- Additional CSS -->
  @yield('css')

  <!-- Bootstrap CSS -->
  <link type="text/css" href="{{ asset('argon/vendor/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('argon/css/argon.css')}}" rel="stylesheet">
</head>