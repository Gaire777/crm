<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
  <div class="scrollbar-inner">
    <div class="sidenav-header d-flex align-items-center">
      <!-- Brand -->
      <a class="navbar-brand" href="#">
        <img src="{{ asset('argon/img/brand/logo.png')}}" class="navbar-brand-img" alt="logo">
      </a>
      <div class="ml-auto">
        <!-- Sidenav toggler -->
        <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
          <div class="sidenav-toggler-inner">
            <i class="sidenav-toggler-line"></i>
            <i class="sidenav-toggler-line"></i>
            <i class="sidenav-toggler-line"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="navbar-inner">
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
          <li class="nav-item border-bottom">
            <a class="nav-link" href="{{route('home')}}">
              <i class="fa fa-desktop" style="color: #f4645f;"></i>
              <span class="nav-link-text" style="color: #f4645f;">{{ __('Dashboard') }}</span>
            </a>
          </li>
          

          <li class="nav-item border-bottom">
            <a class="nav-link" href="#navbar-services" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-services">
              <i class="fas fa-id-card-alt" style="color: #027a32;"></i>
              <span class="nav-link-text">{{ __('Services') }}</span>
            </a>

            <div class="collapse" id="navbar-services">
              <ul class="nav nav-sm flex-column bg-lighter">
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.service.list')}}">
                    {{ __('Services List') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.service.form')}}">
                    {{ __('New Service') }}
                  </a>
                </li> 
              </ul>
            </div>
          </li>
          <li class="nav-item border-bottom">
            <a class="nav-link" href="#navbar-category" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-category">
              <i class="fas fa-list" style="color: red;"></i>
              <span class="nav-link-text">{{ __('Category') }}</span>
            </a>

            <div class="collapse" id="navbar-category">
              <ul class="nav nav-sm flex-column bg-lighter">
                <li class="nav-item">
                  <a class="nav-link"href="{{route('admin.categorylist')}}">
                    {{ __('Category List') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.category.form')}}">
                    {{ __('New Category') }}
                  </a>
                </li>
              </ul>
            </div>
          </li>
        
         <li class="nav-item border-bottom">
       <a class="nav-link" href="#navbar-accounts" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-accounts">
             <i class="fas fa-calendar-check" style="color: darkgreen;"></i>
             <span class="nav-link-text">{{ __('Accounts') }}</span>
           </a>
            <div class="collapse" id="navbar-accounts">
             <ul class="nav nav-sm flex-column bg-lighter">
               <li class="nav-item">
                 <a class="nav-link" href="{{route('admin.cashin.list')}}">
                   {{ __('Cashins') }}
                 </a>
                    </li>
                   <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.cashout.list')}}">
                       {{ __('Cashouts') }}
                  </a>
                        </li>

                         <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.balances.browse')}}">
                       {{ __('Balances') }}
                  </a>
                        </li>
             </ul>
            </div>
             </li>
       <li class="nav-item border-bottom">
                <a class="nav-link" href="{{route('admin.invoice.list')}}">
              <i class="fas fa-file-invoice-dollar" style="color: #000;"></i>
              <span class="nav-link-text">{{ __('Invoices') }}</span>
            </a>
          </li>
          <!--<li class="nav-item border-bottom">-->
          <!--  <a class="nav-link" href="{{route('admin.enquiries.list')}}">-->
          <!--    <i class="far fa-comment-dots" style="color: #000;"></i>-->
          <!--    <span class="nav-link-text">{{ __('Enquiry') }}</span>-->
          <!--  </a>-->
          <!--</li>-->
          
        <li class="nav-item border-bottom">
            <a class="nav-link" href="#navbar-enquiries" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-enquiries">
              <i class="far fa-comment-dots"  style="color: #027a32;"></i>
              <span class="nav-link-text">{{ __('Enquiries') }}</span>
            </a>

            <div class="collapse" id="navbar-enquiries">
              <ul class="nav nav-sm flex-column bg-lighter">
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.enquiries.list')}}">
                    {{ __('Enquiries List') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.enquiries.form')}}">
                    {{ __('Add Enquiry') }}
                  </a>
                </li>
              </ul>
            </div>
          </li>


          <li class="nav-item border-bottom">
            <a class="nav-link" href="#navbar-applicants" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-applicants">
              <i class="fas fa-calendar-check" style="color: darkgreen;"></i>
              <span class="nav-link-text">{{ __('Applicants') }}</span>
            </a>

            <div class="collapse" id="navbar-applicants">
              <ul class="nav nav-sm flex-column bg-lighter">
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.applicant.list')}}">
                    {{ __('Applicants') }}
                  </a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.checklist.list')}}">
                    {{ __('Document Checklist') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.education.list')}}">
                    {{ __('Education') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.healthlicense.list')}}">
                    {{ __('Health License') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.employment.list')}}">
                    {{ __('Employment') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.progressflow.list')}}">
                    {{ __('Progress Flow Report') }}
                  </a>
                </li>

              </ul>
            </div>
          </li>

          <li class="nav-item border-bottom">
            <a class="nav-link" href="#navbar-booking" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-booking">
              <i class="far fa-calendar-check"  style="color: #b54232;"></i>
              <span class="nav-link-text">{{ __('Booking') }}</span>
            </a>

            <div class="collapse" id="navbar-booking">
              <ul class="nav nav-sm flex-column bg-lighter">
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.booking.index')}}">
                    {{ __('Booking List') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.booking.create')}}">
                    {{ __('Add Booking') }}
                  </a>
                </li>
              </ul>
            </div>
          </li>


         <!--  <li class="nav-item border-bottom">--}}
{{--            <a class="nav-link" href="#navbar-user" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-user">--}}
{{--              <i class="fa fa-users" style="color: #412ab7;"></i>--}}
{{--              <span class="nav-link-text">{{ __('User') }}</span>--}}
{{--            </a>--}}

{{--            <div class="collapse" id="navbar-user">--}}
{{--              <ul class="nav nav-sm flex-column bg-lighter">--}}
{{--                <li class="nav-item">--}}
{{--                  <a class="nav-link" href="#">--}}
{{--                    {{ __('User List') }}--}}
{{--                  </a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                  <a class="nav-link" href="#">--}}
{{--                    {{ __('New User') }}--}}
{{--                  </a>--}}
{{--                </li>--}}
{{--              </ul>--}}
{{--            </div>--}}
{{--          </li>--} -->


          

        </ul>
      </div>
    </div>
  </div>
</nav>