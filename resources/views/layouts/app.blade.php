<!DOCTYPE html>
<html lang="en">

@include('layouts.headers.head')

<body class="{{ $class ?? '' }}">
  @auth()
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
  @include('layouts.navs.sidebar')
  @endauth

  <div class="main-content">
    @auth
    @include('layouts.navs.navbar')
    @endauth
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">@yield('page_heading')</h4> </div>

                     <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="text-align: right;">
                    @yield('button')
                </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">@yield('content')</h3> </div>
                    </div>
                </div>
                <!-- ============================================================== -->
               
                
               
            </div>
  </div>

  @guest()
  @include('layouts.footers.guest')
  @endguest

  @include('layouts.footers.scripts')
</body>

</html>