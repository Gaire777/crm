<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>CRM | @yield('title')</title>

  <!-- Favicon -->
  <link href="{{ asset('argon/img/brand/favicon.ico')}}" rel="icon" type="image/png">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

  <!-- Icons -->
  <link href="{{ asset('argon/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
  <link href="{{ asset('argon/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">

  <!-- Additional CSS -->
  @yield('css')

  <!-- select-2 -->
  <link href="{{ asset('argon') }}/vendor/select2/css/select2.css" rel="stylesheet">



  <!-- Bootstrap CSS -->
  <link type="text/css" href="{{ asset('argon/vendor/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">


  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('argon/css/argon.css')}}" rel="stylesheet">

  <!-- datatable css -->
  <link type="text/css" href="{{ asset('argon/vendor/dataTable/css/dataTable.bootstrap.css')}}" rel="stylesheet">

</head>