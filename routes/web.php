<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){

    /*=======================Booking Routes===================================================================*/
    Route::group(['prefix' => 'booking'], function () {
        Route::get('/', ['as' => 'admin.booking.index', 'uses' => 'BookingController@index']);
        Route::get('create', ['as' => 'admin.booking.create', 'uses' => 'BookingController@create']);
        Route::post('store', ['as' => 'admin.booking.store', 'uses' => 'BookingController@store']);
        Route::get('edit/{id}', ['as' => 'admin.booking.edit', 'uses' => 'BookingController@edit']);
        Route::post('update', ['as' => 'admin.booking.update', 'uses' => 'BookingController@update']);
        Route::get('destroy/{id}', ['as' => 'admin.booking.destroy', 'uses' => 'BookingController@destroy']);
    });

	/*===================================================Applicants========================================================================*/
	Route::get('/applicant/user/list', 'ERP\ApplicantController@applicantlist')->name('admin.applicant.list');
	Route::get('/applicant/add', 'ERP\ApplicantController@applicantform')->name('admin.applicant.add');
	Route::post('/applicant/add', 'ERP\ApplicantController@applicantadd')->name('admin.applicant.add');
	Route::get('/applicant/profile/{id}', 'ERP\ApplicantController@visitprofile')->name('admin.applicant.view');
	Route::get('/applicant/edit/{id}', 'ERP\ApplicantController@applicantedit')->name('admin.applicant.edit');
	Route::post('/applicant/edit', 'ERP\ApplicantController@postapplicantedit')->name('admin.applicant.edited');
	Route::get('/applicant/delete/{id}', 'ERP\ApplicantController@applicantdelete')->name('admin.applicant.delete');


	/*===================================================Services========================================================================*/
	Route::get('/service/list', 'ERP\ServiceController@servicelist')->name('admin.service.list');
	Route::post('/service/add', 'ERP\ServiceController@serviceadd')->name('admin.service.add');
	Route::get('/service/form', 'ERP\ServiceController@serviceform')->name('admin.service.form');
	Route::get('/service/edit/{id}', 'ERP\ServiceController@editservice')->name('admin.serviceeditform');
	Route::post('/service/edit', 'ERP\ServiceController@postedit')->name('admin.serviceedit');
	Route::get('/delete-service/{id}','ERP\ServiceController@deleteservice')->name('admin.servicedelete');

	/*===================================================Category========================================================================*/
	Route::get('/category/list', 'ERP\CategoryController@categorylist')->name('admin.categorylist');
	Route::post('/category/add', 'ERP\CategoryController@categoryadd')->name('admin.category.add');
	Route::get('/category/form', 'ERP\CategoryController@categoryform')->name('admin.category.form');
	Route::get('/category/edit/{id}', 'ERP\CategoryController@editcategory')->name('admin.categoryeditform');
	Route::post('/category/edit', 'ERP\CategoryController@postedit')->name('admin.categoryedit');
	Route::get('/category/delete/{id}','ERP\CategoryController@deletecategory')->name('admin.categorydelete');

    /*======================================================sms and email==================================================================*/
	Route::post('/categories/sendsms', 'ERP\CategoryController@sendsms')->name('admin.sendgroupsms');
	Route::post('/categories/sendemail', 'ERP\CategoryController@sendemail')->name('admin.sendgroupemail');
	Route::post('/categories/sendsms-en', 'ERP\CategoryController@sendsms_en')->name('admin.sendgroupsms-en');
	Route::post('/categories/sendemail-en', 'ERP\CategoryController@sendemail_en')->name('admin.sendgroupemail-en');

	/*===================================================Health License========================================================================*/
	Route::get('/healthlicense/list', 'ERP\HealthLicenseController@healthlicenselist')->name('admin.healthlicense.list');
	Route::post('/healthlicense/add', 'ERP\HealthLicenseController@healthlicenseadd')->name('admin.healthlicense.add');
	Route::get('/healthlicense/form', 'ERP\HealthLicenseController@healthlicenseform')->name('admin.healthlicense.form');
	Route::get('/healthlicense/edit/{id}', 'ERP\HealthLicenseController@edithealthlicense')->name('admin.healthlicenseeditform');
	Route::post('/healthlicense/edit', 'ERP\HealthLicenseController@postedit')->name('admin.healthlicenseedit');
	Route::get('/healthlicense/delete/{id}','ERP\HealthLicenseController@deletehealthlicense')->name('admin.healthlicensedelete');


    /*================================================================Education===================================================*/
	Route::get('/education/list', 'ERP\EducationController@educationlist')->name('admin.education.list');
	Route::get('/education/add', 'ERP\EducationController@educationform')->name('admin.education.add');
	Route::post('/education/add', 'ERP\EducationController@educationadd')->name('admin.education.added');
	Route::get('/education/edit/{id}', 'ERP\EducationController@educationedit')->name('admin.education.edit');
	Route::post('/education/edit', 'ERP\EducationController@posteducationedit')->name('admin.education.edited');
	Route::get('/education/delete/{id}', 'ERP\EducationController@educationdelete')->name('admin.education.delete');

    /*=====================================Document Checklist========================*/
	Route::get('/checklist/list', 'ERP\ChecklistController@checklistlist')->name('admin.checklist.list');
	Route::get('/checklist/add', 'ERP\ChecklistController@checklistform')->name('admin.checklist.add');
	Route::post('/checklist/add', 'ERP\ChecklistController@checklistadd')->name('admin.checklist.added');
	Route::get('/checklist/edit/{id}', 'ERP\ChecklistController@checklistedit')->name('admin.checklist.edit');
	Route::post('/checklist/edit', 'ERP\ChecklistController@postchecklistedit')->name('admin.checklist.edited');
	Route::get('/checklist/delete/{id}', 'ERP\ChecklistController@checklistdelete')->name('admin.checklist.delete');

    /*=====================================Employment========================*/
	Route::get('/employment/list', 'ERP\EmploymentController@employmentlist')->name('admin.employment.list');
	Route::get('/employment/add', 'ERP\EmploymentController@employmentform')->name('admin.employment.add');
	Route::post('/employment/add', 'ERP\EmploymentController@employmentadd')->name('admin.employment.added');
	Route::get('/employment/edit/{id}', 'ERP\EmploymentController@employmentedit')->name('admin.employment.edit');
	Route::post('/employment/edit', 'ERP\EmploymentController@postemploymentedit')->name('admin.employment.edited');
	Route::get('/employment/delete/{id}', 'ERP\EmploymentController@employmentdelete')->name('admin.employment.delete');

	/*=========================================== Enquiries ======================================================*/
    Route::get('/enquiries/list', 'ERP\EnquiryController@enquirieslist')->name('admin.enquiries.list');
    Route::get('/enquiries/add', 'ERP\EnquiryController@enquiriesform')->name('admin.enquiries.form');
    Route::post('/enquiries/add','ERP\EnquiryController@addenquiries')->name('admin.enquiries.add');
    Route::get('/enquiries/edit/{id}',  'ERP\EnquiryController@getEditenquiries')->name('admin.edit_enquiries');
    Route::post('/enquiries/edit',  'ERP\EnquiryController@postEditEnquiries')->name('admin.store-enquiries');
    Route::get('/enquiries/delete/{id}','ERP\EnquiryController@deleteEnquiries')->name('admin.delete-enquiries');

    /*==========================================Progressflow===================================================================*/
    Route::get('/progressflow/list', 'ERP\ProgressFlowController@showlist')->name('admin.progressflow.list');
    Route::get('/progressflow/add', 'ERP\ProgressFlowController@showform')->name('admin.progressflow.add');
    Route::post('/progressflow/add', 'ERP\ProgressFlowController@addprogressFlow')->name('admin.progressflow.add');
    Route::get('/progressflow/edit/{id}',  'ERP\ProgressFlowController@getEditprogressFlow')->name('admin.edit');
    Route::post('/progressflow/edit',  'ERP\ProgressFlowController@postEditprogressFlow')->name('admin.store-progressflow');
    Route::get('/progressflow/delete/{id}','ERP\ProgressFlowController@deleteprogressFlow')->name('admin.delete-progressflow');

    /*===================================================Cashins========================================================================*/
    Route::get('/cashin/list','ERP\CashinsController@listcashin')->name('admin.cashin.list');
    Route::get('/cashin/add','ERP\CashinsController@cashinform')->name('admin.cashin.form');
    Route::post('/cashin/add','ERP\CashinsController@cashinadd')->name('admin.cashin.add');
    Route::get('/cashin/edit/{id}', 'ERP\CashinsController@editcashin')->name('admin.cashineditform');
    Route::post('/cashin/edit', 'ERP\CashinsController@postedit')->name('admin.cashin.edit');
    Route::get('/cashin/delete/{id}','ERP\CashinsController@deletecashin')->name('admin.cashin.delete');


    /*===================================================Manpower========================================================================*/
    Route::match(['get','post'],'/personnel/add/','ERP\ManpowerController@manpowerform')->name('manpowerform');
    Route::get('/personnel/list','ERP\ManpowerController@viewmanpowerlist')->name('viewmanpowerlist');
    Route::match(['get','post'],'/personnel/edit/{id}','ERP\ManpowerController@manpoweredit')->name('manpoweredit');
    Route::get('/personnel/list/{id}','ERP\ManpowerController@deletemanpower')->name('deletemanpower');
    Route::any('/personnel/search','ERP\ManpowerController@ajaxsearch')->name('ajaxsearch');



    /*===================================================Invoice========================================================================*/
    Route::get('/invoice/list','ERP\InvoiceController@listinvoice')->name('admin.invoice.list');
    Route::get('/invoice/add','ERP\InvoiceController@forminvoice')->name('admin.invoice.form');
    Route::post('/invoice/add','ERP\InvoiceController@addinvoice')->name('admin.invoice.add');
    Route::get('/invoice/view/{id}', 'ERP\InvoiceController@view')->name('admin.invoice.view');
    Route::get('/invoice/delete/{id}','ERP\InvoiceController@deleteinvoice')->name('admin.invoice.delete');

    /*===================================================Cashout========================================================================*/
    Route::get('/cashout/list','ERP\CashoutController@listcashout')->name('admin.cashout.list');
    Route::get('/cashout/add','ERP\CashoutController@formcashout')->name('admin.cashout.form');
    Route::post('/cashout/add','ERP\CashoutController@addcashout')->name('admin.cashout.add');
    Route::get('/cashout/edit/{id}', 'ERP\CashoutController@getedit')->name('admin.cashout_edit');
    Route::post('/cashout/edit', 'ERP\CashoutController@postedit')->name('admin.cashout.edit');
    Route::get('/cashout/delete/{id}','ERP\CashoutController@deletecashout')->name('admin.cashout.delete');

			/*===================================================Balance========================================================================*/
    Route::get('/balance/browse','ERP\BalanceController@browse')->name('admin.balances.browse');
    Route::post('/balance/report','ERP\BalanceController@report')->name('admin.balances.report');

			/*===================================================PDF========================================================================*/
    Route::get('/customers/pdf/{id}','ERP\InvoiceController@export_pdf')->name('admin.pdf.view');

		/*=================================Ajax to fill applicant form================================================*/
    Route::get('/search/fetch/{id}', 'ERP\ApplicantController@ajaxRequest')->name('applicant.fetch');
    Route::get('/sample','sampleController@viewsample')->name('version2');

 });
