<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document_check_list extends Model
{
		protected $table='document_check_lists';

	    public function applicants(){

    	return $this->hasOne(Applicant::class,'id','applicant_id');
    }
}
