<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function cash_in()
{
    return $this->hasMany(Cash_in::class,'paidby','name');
}

  public function invoice()
{
    return $this->hasMany(Invoice::class,'ClientName','name');
}
}
