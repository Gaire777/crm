<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function applicants()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function enquiry()
    {
        return $this->hasMany(Enquiry::class);
    }
}
