<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Applicant;
use App\Enquiry;
use DB;
use Mail;
use GuzzleHttp\Client;
use Session;
use URL;
use Auth;
use App\Notifications\EmailNotification;

class CategoryController extends Controller
{
    public function categorylist()
    {
        $data['category'] = Category::all();
        // dd($data);
        return view('EMS.Categories1.catlist', $data);
    }




    public function categoryadd(Request $request)
    {
        // dd($request->all());

        $request->validate([

            'name' => 'required|max:255',


        ]);
        // dd($request);


        $category = new Category();

        $category->name = $request->name;

        $category->save();


        \Session::flash('success', 'Product Added Successfully');
        return redirect()->route('admin.categorylist');
    }

    public function categoryform()
    {
        // dd($request->all());


        return view('EMS.Categories.categoryadd');
    }

    public function editcategory($id)
    {

        $data['category'] = Category::find($id);

        return view('EMS.Categories.categoryedit', $data);
    }

    public function postedit(Request $request)
    {

        $request->validate([

            'name' => 'required|max:255',




        ]);

        // dd($request);
        $category = Category::find($request->id);
        $category->name = $request->name;

        $category->update();

        return redirect()->route('admin.categorylist');
    }

    public function deletecategory($id)
    {

        // dd($request);
        $category = Category::find($id);

        $category->delete();

        return redirect()->route('admin.categorylist');
    }

    public function sendsms(Request $request)

    {


        // dd($request);

        $results = DB::select('select mobile_no from applicants where applicant_category = ?', [$request->id]);


        $numbers = array();
        //      dd($numbers);


        foreach ($results as $result) {
            // dd($result);
            $value = (int) $result->mobile_no + 0;

            array_push($numbers, $value);
        }


        $phone = implode(',', $numbers);
        // dd($phone);

        // return($phone);

        //      API integration

        $token = env('TOKEN_API');
        $token = 'Hkebx0n6COOv8cP5Tm5D';
        // dd($token);
        $from  = 'InfoSMS';
        $to    = $phone;
        $text  = $request->message;
        $api_url = "http://api.sparrowsms.com/v2/sms/?" . http_build_query(array(
            'token' => $token,
            'from'  => $from,
            'to'    => $to,
            'text'  => $request->message
        ));

        // $response = file_get_contents($api_url);

        $client = new \GuzzleHttp\Client();
        $request = $client->get($api_url);
        $response = $request->getBody();


        Session::flash('message', 'SMS has been Sent');


        return redirect()->route('admin.categorylist');
    }

    //    public function sendemail(){
    //      $to_name = 'sarojbhusal';
    //      $to_email = 'chakratimilsina77@gmail.com';
    //      $data = array('name'=>'Saroj', 'emailbody' => 'hello');
    //      Mail::send('EMS.Email.email', $data, function($message) use ($to_name, $to_email) {
    //              $message->to($to_email, $to_name)
    //              ->subject('Laravel Test Mail');
    //              $message->from('sarojbhusaltrail@gmail.com','Saroj Bhusal');
    //      });
    // }

    public function sendemail(Request $request, $data = '')
    {

        $value = array();
        $value['message'] = $request->message;

        $value['url'] = URL::current();
        $applicant = Applicant::where('applicant_category', $request->id)->get();
        // dd($applicant);
        foreach ($applicant as $item) {


            $chk = $item->notify(new EmailNotification($value));
        }
        // Session::flash('info','sending email sucess');
        // return redirect()->back()->with('sucess', 'Email Was Sent.');
        Session::flash('message', 'Email has been Sent Successfully');

        return redirect()->back();
    }

    public function sendsms_en(Request $request)
    {

        $results = DB::select('select phone from enquiries where category_id = ?', [$request->id]);


        $numbers = array();

        foreach ($results as $result) {

            $value = (int) $result->phone + 0;

            array_push($numbers, $value);
        }


        $phone = implode(',', $numbers);

        // dd($phone);
        //      API integration

        $token = env('TOKEN_API');
        $token = 'Hkebx0n6COOv8cP5Tm5D';
        // dd($token);
        $from  = 'InfoSMS';
        $to    = $phone;
        $text  = $request->message;
        $api_url = "http://api.sparrowsms.com/v2/sms/?" . http_build_query(array(
            'token' => $token,
            'from'  => $from,
            'to'    => $to,
            'text'  => $request->message
        ));

        // $response = file_get_contents($api_url);

        $client = new \GuzzleHttp\Client();
        $request = $client->get($api_url);
        $response = $request->getBody();


        Session::flash('message', 'SMS has been Sent');


        return redirect()->route('admin.categorylist');
    }

    public function sendemail_en(Request $request, $data = '')
    {
        $value = array();
        $value['message'] = $request->message;

        $value['url'] = URL::current();
        $enquires = Enquiry::where('category_id', $request->id)->get();
        
        foreach ($enquires as $item) {

            $chk = $item->notify(new EmailNotification($value));
        }
        // Session::flash('info','sending email sucess');
        // return redirect()->back()->with('sucess', 'Email Was Sent.');
        Session::flash('message', 'Email has been Sent Successfully');

        return redirect()->back();
    }
}
