<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Health_license;
use App\Applicant;
use Illuminate\Support\Facades\Storage;

class HealthLicenseController extends Controller
{
    public function healthlicenselist(){

    	// $data['healthlicense']=Health_license::all();
      $data['healthlicense']=Health_license::with('applicant')->paginate(10);

    	// dd($data);


    	return view('EMS.Healthlicense.healthlist',$data);

    	
    }

    public function healthlicenseadd(Request $request){

    	// dd($request);

       $request->validate([
         
            
            
            'license_type' => 'required',
            'license_number' => 'required|integer',
            'license_status' => 'required|max:255',

            

          
      ]);

    	$healthlicense = new Health_license();

		$healthlicense->applicant_id=$request->applicant_id;
		$healthlicense->professional_designation=$request->professional_designation;
		$healthlicense->issuing_authority_name=$request->issuing_authority_name;
		$healthlicense->issuing_authority_country=$request->issuing_authority_country;
		$healthlicense->issuing_authority_city=$request->issuing_authority_city;
		$healthlicense->license_conferred_date=$request->license_conferred_date;
		$healthlicense->license_expiry_date=$request->license_expiry_date;
		$healthlicense->license_type=$request->license_type;
		$healthlicense->license_number=$request->license_number;
		$healthlicense->license_status=$request->license_status;
		$healthlicense->license_attained=$request->license_attained;
		// $healthlicense->license_copy=$request->license_copy;
		// $healthlicense->applicant_id=$request->applicant_id;

    if(($request->license_copy != null)){
            $healthlicense->license_copy = $request->file('license_copy')->store('healthlicense','public');
        }

		$healthlicense->save();


   


    	return redirect()->route('admin.healthlicense.list');
    }

     public function healthlicenseform(){
      $data['applicant'] = Applicant::all();
     	// dd($data);

    	return view('EMS.Healthlicense.healthadd',$data);
    }


      public function edithealthlicense($id){
         $data['applicant'] = Applicant::all();
         $data1['healthlicense']=Health_license::find($id);

      	return view('EMS.Healthlicense.healthedit',$data,$data1);
      }

      public function postedit(Request $request){

        $request->validate([
         
            
            
            'license_type' => 'required',
            'license_number' => 'required|integer',
            'license_status' => 'required|max:255',

            

          
      ]);

        // dd($request);

        $healthlicense=Health_license::find($request->id);
         // dd($healthlicense);

    $healthlicense->applicant_id=$request->applicant_id;
    // dd($healthlicense->applicant_id);
    $healthlicense->professional_designation=$request->professional_designation;
    $healthlicense->issuing_authority_name=$request->issuing_authority_name;
    $healthlicense->issuing_authority_country=$request->issuing_authority_country;
    $healthlicense->issuing_authority_city=$request->issuing_authority_city;
    $healthlicense->license_conferred_date=$request->license_conferred_date;
    $healthlicense->license_expiry_date=$request->license_expiry_date;
    $healthlicense->license_type=$request->license_type;
    $healthlicense->license_number=$request->license_number;
    $healthlicense->license_status=$request->license_status;
    $healthlicense->license_attained=$request->license_attained;
    // $healthlicense->license_copy=$request->license_copy;
    

    if(($request->license_copy != null)){
             Storage::delete('public/'.$healthlicense->license_copy);
             $path = $request->file('license_copy')->store('healthlicense', 'public');
            $healthlicense->license_copy = $path;

             $healthlicense->updated_at = date('Y-m-d H:i:s');
              $healthlicense->update();
            $healthlicense->license_copy = $request->file('license_copy')->store('healthlicense','public');

      } 
        $healthlicense->update();

      	return redirect()->route('admin.healthlicense.list');
      }

        public function deletehealthlicense($id){

          $healthlicense=Health_license::find($id);
          if(($healthlicense->license_copy != null)){
             Storage::delete('public/'.$healthlicense->license_copy);}
          
          $healthlicense->delete();

      	return redirect()->route('admin.healthlicense.list');
      }








}
