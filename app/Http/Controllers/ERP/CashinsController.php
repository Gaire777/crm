<?php

namespace App\Http\Controllers\ERP;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Cashin;
use App\Applicant;
use App\Service;
use APp\User;

class CashinsController extends Controller
{
    public function listcashin(){

    	$data['cashin'] =Cashin::with('service','applicants','users')->get();
    	// dd($data);
    	
    	
    	return view('EMS.cashins.show',$data);
    }

    public function cashinform(){

    	$data1=Applicant::all();
    	$data=Service::all();
    	$data2=User::all();

    	// dd($data2);
    	
    	
    	return view('EMS.cashins.cashinform',compact('data','data1','data2'));
    }

     public function cashinadd(Request $request){
     		// dd($request);
        $request->validate([
         
            'amount_paid' => 'required|integer',
           
          
      ]);
     	$cashin=new Cashin();
     	$cashin->paidby=$request->paidby;
     	$cashin->receivedby=$request->receivedby;
     	$cashin->service=$request->service;
     	$cashin->amount_paid=$request->amount_paid;
     	$cashin->remarks=$request->remarks;
     	$cashin->date_paid=$request->date_paid;

     	$cashin->Save();
        app('App\Http\Controllers\ERP\InvoiceController')->storefromcashins($request);

    	
    	
    	
    	return redirect()->route('admin.cashin.list');
    }

    public function editcashin($id){
     		// dd($request);
     	$data1=Applicant::all();
    	$data=Service::all();
    	$data2=User::all();
    	$cashin=Cashin::with('applicants')->find($id);
        $olddata = Cashin::find($id);


    	// dd($cashin);


    	
    	
    	return view('EMS.cashins.cashinedit',compact('data','data1','data2','cashin'));

    	
    	}

    	 public function postedit(Request $request){
     		// dd($request);
        $olddata = Cashin::find($request->id);
        // dd($olddata);
 	    $cashin=Cashin::find($request->id);
     	
     	$cashin->receivedby=$request->receivedby;
     	$cashin->service=$request->service;
     	$cashin->amount_paid=$request->amount_paid;
     	$cashin->remarks=$request->remarks;
     	$cashin->date_paid=$request->date_paid;

     	$cashin->update();
        app('App\Http\Controllers\ERP\InvoiceController')->updatefromcashins($request,$olddata);
    	
    	
    	return redirect()->route('admin.cashin.list');

    	
    	}

    	 public function deletecashin($id){

          $cashin=Cashin::find($id);
          
          
          $cashin->delete();

      	return redirect()->route('admin.cashin.list');
      }
    	
    	
}
