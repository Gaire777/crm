<?php

namespace App\Http\Controllers\ERP;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Client;
use DB;
use PDF;
use App\Service;
use App\Cashin;
use App\ Applicant;
use Auth;




class InvoiceController extends Controller
{
    public function listinvoice(){

    	$data['invoices'] = Invoice::with('applicants','service','users')->get();
    	// $data['invoice'] = Invoice::with('Applicant','Service')->get();
    //   dd($data);
    	
    	
    	return view('EMS.invoice.list',$data);
 

    }
    public function forminvoice(){

    	$data['applicants']= Applicant::all();
    	
    	
    	return view('EMS.invoice.form',$data);
    }
    public function addinvoice(Request $request){
      // dd($request);

         // $request->validate([
   //          'clientName' => 'required|max:255',
   //          'date' => 'required|max:255',
   //          'paid' => 'max:255',
   //          'service' => 'required|max:255',
   //          'price' => 'required|max:10',
   //          'receivedby' => 'required|max:255',
   //          
            
   //      ]);

         $invoices = new Invoice();
        $invoices->applicant = $request->applicant;
        $invoices->date = $request->date;
        
        $invoices->service = $request->service;
        $invoices->price= $request->amount_paid;
        $invoices->receivedby = $request->receivedby;
     
        
          // $invoices->added_by = Auth::user()->name;
      // dd(Auth::user()->id);
       $invoices->created_at = date('Y-m-d H:i:s');
        // dd($invoices);
        $invoices->save();
        return redirect()->route('admin.invoice.list');
    }

    public function storefromcashins(Request $request)

  {

    // dd ($request);
    $invoicedata = Invoice::where([['applicant', '=', $request->paidby],['service', '=', $request->service]])->first();
;

    if($invoicedata == null){
    $invoices = new Invoice();
        $invoices->applicant = $request->paidby;
        $invoices->date = $request->date_paid;
        
        $invoices->service = $request->service;
        $serviceprice = Service::where('id' , $request->service)->pluck('price');
        $invoices->price= $serviceprice[0];
        $invoices->receivedby = $request->receivedby;
     
        
          // $invoices->added_by = Auth::user()->name;
      // dd(Auth::user()->id);
       $invoices->created_at = date('Y-m-d H:i:s');
        // dd($invoices);
        $invoices->save();
  

    } else {

    $totalpaid = Cashin::where([['paidby', '=', $request->paidby],['service', '=', $request->service]])->sum('amount_paid');

    $due = $invoicedata->price-$totalpaid;

    if($due <= 0){
        Invoice::find($invoicedata->id)->update(['marked_as'=> 'paid']);

    }
    

    }



  }


  public function updatefromcashins(Request $request,$olddata)

  {
    // dd($olddata);

    // dd($olddata->count());
    $invoicedata = Invoice::where([['applicant', '=', $olddata->paidby],['service', '=', $olddata->service]])->get();
    $invoicedatas = Invoice::where([['applicant', '=', $olddata->paidby],['service', '=', $olddata->service]])->first();

     
    
      

      if ($invoicedata->count()==1) {

          
      
      $invoices=Invoice::find($invoicedatas->id);


        
        $invoices->date = $request->date_paid;
        
        $invoices->service = $request->service;
        $serviceprice = Service::where('id' , $request->service)->pluck('price');
        // dd($serviceprice);
        $invoices->price= $serviceprice[0];
        $invoices->receivedby = $request->receivedby;
     
        
          // $invoices->added_by = Auth::user()->name;
      // dd(Auth::user()->id);
       $invoices->updated_at = date('Y-m-d H:i:s');
        // dd($invoices);
        $invoices->save();

    
      return ;
      }

      else{
        // dd($request);
          $totalpaid = Cashin::where([['paidby', '=', $request->paidby],['service', '=', $request->service]])->sum('amount_paid');

          $due = $invoicedata->price-$totalpaid;

          if($due <= 0){
          Invoice::find($invoicedata->id)->update(['marked_as'=> 'paid']);

    }

      }

      
      
    }


    
  

     public function view($id){
        
         $invoicedata = Invoice::find($id);
         // dd($invoicedata);
        
         $invoice=Invoice::find($id);
                          
          // dd($invoice);

          $applicant=Applicant::where('id',$invoice->applicant)->get();
                          // dd($applicant[0]->first_name);
        
         

         $cashindata = Cashin::where([['paidby', '=', $invoicedata->applicant],['service', '=', $invoicedata->service]])->with('service','applicants')->get();
         // dd($cashindata);
         
            $totalpaid = Cashin::where([['paidby', '=', $invoicedata->applicant],['service', '=', $invoicedata->service]])->sum('amount_paid');
        // dd($totalpaid);

            $due=$invoicedata->price-$totalpaid;

         // dd($due);


        
        
        return view('EMS.invoice.view',compact('invoice','invoicedata', 'cashindata','totalpaid','due','applicant'));

}

 public function deleteinvoice($id){

          $invoice=Invoice::find($id);
          
          
          $invoice->delete();

        return redirect()->route('admin.invoice.list');
      }

      public function export_pdf($id)
  {
    // Fetch all customers from database
    $invoicedata = Invoice::find($id);
         // dd($invoicedata);
        
         $invoice=Invoice::find($id);
                          
          // dd($invoice);

          $applicant=Applicant::where('id',$invoice->applicant)->get();
                          // dd($applicant[0]->first_name);
        
         

         $cashindata = Cashin::where([['paidby', '=', $invoicedata->applicant],['service', '=', $invoicedata->service]])->with('service','applicants')->get();
         // dd($cashindata);
         
            $totalpaid = Cashin::where([['paidby', '=', $invoicedata->applicant],['service', '=', $invoicedata->service]])->sum('amount_paid');
        // dd($totalpaid);

            $due=$invoicedata->price-$totalpaid;

    // Send data to the view using loadView function of PDF facade
    $pdf = PDF::loadView('EMS.invoice.pdf', compact('invoice','invoicedata', 'cashindata','totalpaid','due','applicant'));
    // If you want to store the generated pdf to the server then you can use the store function
    $pdf->save(storage_path().'_filename.pdf');

    // Finally, you can download the file using download function
    return $pdf->download('Invoice.pdf');
  }

}
