<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Notifications\EmailNotification;
use App\Applicant;
use App\Category;
use App\Enquiry;
use App\Document_check_list;

class ApplicantController extends Controller
{
     /* Applicant*/
	public function applicantlist(){

        
        $data['applicants']=Applicant::with('categories')->get();

        //paginate replace get with paginate (10)
        
         // dd($data);
        
      return view('EMS.Applicants.applicantlist',$data);
	}

	public function applicantform(){
	    
	   // $data['applicant']= Applicant::all();
		$data2['categories'] = Category::all();
		$data1['enquiries']=Enquiry::where('eligibility','Eligible')->get();
// 		dd($data2);

		// dd($data);
		return view('EMS.Applicants.applicantadd',$data1,$data2);
	}

	 public function applicantadd(Request $request){
        // dd($request->all());

    	$request->validate([
         
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'gender' => 'required|max:255',
            'dob' => 'required|max:255',
            'identity_type' => 'required|max:255',
            'mobile_no' => 'required',
            'nationality' => 'required|max:255',
            'email' => 'required|email',

          
    	]);

    	$applicant = new Applicant();
		    	
   
		$applicant->first_name = $request->fname;
		$applicant->middel_name = $request->middle_name;
		$applicant->surname = $request->lname; 
		$applicant->maiden_name = $request->maiden_name; 
		$applicant->gender = $request->gender; 
		$applicant->dob =$request->dob;
		$applicant->identity_type = $request->identity_type; 
		$applicant->identity_card_no = $request->identity_card_name;
		$applicant->passport_no = $request->passport_no;
		$applicant->mobile_no = $request->mobile_no;
		$applicant->nationality = $request->nationality;
		$applicant->email = $request->email;


        if(($request->passport_docs != null)){
            $applicant->passport_docs = $request->file('passport_docs')->store('passport','public');
        }
		
		$applicant->applicant_category= $request->applicant_category;

		$applicant->save();

    	\Session::flash('success','Applicant Added Successfully');
    	return redirect()->route('admin.applicant.list');
    }

     public function applicantedit($id){

     	
        $data['applicant'] = Applicant::find($id);
        $data2['categories'] = Category::all();
        // dd($data);
        return view('EMS.Applicants.applicantedit',$data,$data2);
    }

    public function postapplicantedit(Request $request){
    	 // dd($request);
      $request->validate([
         
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'gender' => 'required|max:255',
            'dob' => 'required|max:255',
            'identity_type' => 'required|max:255',
            'mobile_no' => 'required|integer',
            'nationality' => 'required|max:255',
            'email' => 'required|email',

          
      ]);
        $applicant = Applicant::find($request->id);
        // dd($applicant);
    $applicant->first_name = $request->fname;
    $applicant->middel_name = $request->middle_name;
    $applicant->surname = $request->lname; 
    $applicant->maiden_name = $request->maiden_name; 
    $applicant->gender = $request->gender; 
    $applicant->dob =$request->dob;
    $applicant->identity_type = $request->identity_type; 
    $applicant->identity_card_no = $request->identity_card_name;
    $applicant->passport_no = $request->passport_no;
    $applicant->mobile_no = $request->mobile_no;
    $applicant->nationality = $request->nationality;
    $applicant->email = $request->email;


        if(($request->passport_docs != null)){
            $applicant->passport_docs = $request->file('passport_docs')->store('passport','public');
        }
    
    $applicant->applicant_category= $request->applicant_category;


    $applicant->update();

        return redirect()->route('admin.applicant.list');

        
    }

    public function applicantdelete($id){
    
        $applicant = Applicant::find($id);
        $applicant->delete();
         
    
            return redirect()->route('admin.applicant.list');
      }
  
    public function visitprofile($id){
        //  dd($id);
        $data['profile']=Applicant::find($id);
        $data1['checklist']=Document_check_list::where('applicant_id','=',$id)->get();
        // $columns['column'] = Schema::getColumnListing('document_check_lists');

       // dd($columns);
         
      return view('EMS.Applicants.profile',$data,$data1);
    }
    
    
    

    public function ajaxRequest($id){
    

    		 // dd($id);
           $sersearch['checklist']=Enquiry::where('id','=',$id)->get()->toJson();
        
       return response()->json($sersearch);

    }




  
}
