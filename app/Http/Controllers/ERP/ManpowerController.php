<?php
namespace App\Http\Controllers\ERP;



use App\FinalDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EducationQualification;
use App\WorkExperience;
use App\PersonalDetail;
use App\LanguageKnown;
use App\Passport;
use App\TrainingTechKnowledge;
use App\InterviewComment;
use Illuminate\Support\Str;
use Svg\Tag\Image;
use Illuminate\Support\Facades\Input;

class ManpowerController extends Controller
{
    public function manpowerform(Request $request) {

        if($request->isMethod('post'))
        {

            $data = $request->all();
            $personal_detail=new PersonalDetail();
            $personal_detail->name = $data['name'];
            $personal_detail->father_name = $data['father_name'];
            $personal_detail->mother_name = $data['mother_name'];
            $personal_detail->permanent_address = $data['permanent_address'];
            $personal_detail->temporary_address = $data['temporary_address'];
            $personal_detail->phone = $data['phone'];
            $personal_detail->height = $data['height'];
            $personal_detail->weight = $data['weight'];
            $personal_detail->maritial_status = $data['maritial_status'];
            $personal_detail->contact_no = $data['contact_number'];
            $personal_detail->nationality = $data['nationality'];
            $personal_detail->next_of_kin = $data['next_of_kin'];


            if(($request->prof_img != null)){
                $personal_detail->prof_img = $request->file('prof_img')->store('Profile','public');
            }

            $personal_detail->save();

            $personsid= DB::getPdo()->lastInsertId();


            $final_detail = new FinalDetail;
            $final_detail->person_id=$personsid;
            $final_detail->position_applied= $data["position_applied"];
            $final_detail->reference_person=$data["reference_person"];
            $final_detail->ref_contact=$data["ref_contact"];
            $final_detail->country=$data["country"];
            $final_detail->save();



            $count=0;
            foreach ($data["qualification"] as $value){
                $count=$count+1;

            };


            for($i=0;$i<$count;++$i){
                $education_qual = new EducationQualification() ;
                $education_qual->person_id=$personsid;
                $education_qual->qualification_id=$data['qualification'][$i];
                $education_qual->name_of_course=$data['courses'][$i];
                $education_qual->name_of_institute=$data['institute'][$i];
                $education_qual->year=$data['qualification_year'][$i];
                $education_qual->save();
            };


            $count1=0;
            foreach ($data["name_company"] as $value){
                $count1=$count1+1;
            };

            for($i=0;$i<$count1;++$i){
                $workexperience = new WorkExperience() ;
                $workexperience->person_id=$personsid;
                $workexperience->name_of_company=$data['name_company'][$i];
                $workexperience->position_held=$data['position_held'][$i];
                $workexperience->from=$data['date_from'][$i];
                $workexperience->to=$data['date_to'][$i];
                $workexperience->reason_for_leaving=$data['reason_for_leave'][$i];
                $workexperience->save();
            };



            $count2=0;
            foreach ($data["language"] as $value){
                $count2=$count2+1;
            };

            for($i=0;$i<$count2;++$i){
                $languageknown = new LanguageKnown ;
                $languageknown->person_id=$personsid;
                $languageknown->language=$data['language'][$i];
                $languageknown->speaking=$data['speak'][$i];
                $languageknown->reading=$data['read'][$i];
                $languageknown->writing=$data['write'][$i];
                $languageknown->save();
            };

            $count3=0;
            foreach ($data["knowledge_description"] as $value){
                $count3=$count3+1;
            };

            for($i=0;$i<$count3;++$i){
                $knowledge_description = new TrainingTechKnowledge ;
                $knowledge_description->person_id=$personsid;
                $knowledge_description->knowledge=$data['knowledge_description'][$i];
                $knowledge_description->save();
            };


            $passport_detail= new Passport;
            $passport_detail->person_id=$personsid;
            $passport_detail->passport_no=$data['passport-no'];
            $passport_detail->issue_date=$data['issue_date'];
            $passport_detail->expiry_date=$data['expiry_date'];
            $passport_detail->issue_place=$data['issue_place'];
            $passport_detail->birth_place=$data['birth_place'];
            $passport_detail->birth_date=$data['birth_date'];
            $passport_detail->age=$data['age'];

            if(($request->doc_img != null)){
                $passport_detail->image = $request->file('doc_img')->store('Documents','public');
            }
            $passport_detail->save();

            return redirect()->route('viewmanpowerlist');
        }
        return view('EMS.Manpower.add');
    }

    public function viewmanpowerlist(){
        $personnel=PersonalDetail::with('finaldetails')->paginate(10)->all();
//        dd($personnel);
        return view('EMS.Manpower.list',compact('personnel'));

    }

    public function manpoweredit(Request $request,$id){

        $personnel=PersonalDetail::where('id',$id)->with('finaldetails','training_tech_knowledges','education_qualifications','work_experiences','language_known','passport_details','interview_comments')->first();

        if($request->isMethod('post'))
        {




            $data = $request->all();
            $personal_detail=PersonalDetail::findOrfail($id);
            $personal_detail->name = $data['name'];
            $personal_detail->father_name = $data['father_name'];
            $personal_detail->mother_name = $data['mother_name'];
            $personal_detail->permanent_address = $data['permanent_address'];
            $personal_detail->temporary_address = $data['temporary_address'];
            $personal_detail->phone = $data['phone'];
            $personal_detail->height = $data['height'];
            $personal_detail->weight = $data['weight'];
            $personal_detail->maritial_status = $data['maritial_status'];
            $personal_detail->contact_no = $data['contact_number'];
            $personal_detail->nationality = $data['nationality'];
            $personal_detail->next_of_kin = $data['next_of_kin'];


            if(($request->prof_img != null)){
                $personal_detail->prof_img = $request->file('prof_img')->store('Profile','public');
            }

            $personal_detail->save();




            $final_detail =FinalDetail::where('person_id',$id)->first();
            $final_detail->position_applied= $data["position_applied"];
            $final_detail->reference_person=$data["reference_person"];
            $final_detail->ref_contact=$data["ref_contact"];
            $final_detail->country=$data["country"];
            $final_detail->save();



            $count=0;
            foreach ($data["qualification"] as $value){
                $count=$count+1;

            };


            for($i=0;$i<$count;++$i){
                $education_qual =EducationQualification::where('person_id',$id)->first();

                $education_qual->qualification_id=$data['qualification'][$i];
                $education_qual->name_of_course=$data['courses'][$i];
                $education_qual->name_of_institute=$data['institute'][$i];
                $education_qual->year=$data['qualification_year'][$i];
                $education_qual->save();
            };


            $count1=0;
            foreach ($data["name_company"] as $value){
                $count1=$count1+1;
            };

            for($i=0;$i<$count1;++$i){
                $workexperience =WorkExperience::where('person_id',$id)->first();

                $workexperience->name_of_company=$data['name_company'][$i];
                $workexperience->position_held=$data['position_held'][$i];
                $workexperience->from=$data['date_from'][$i];
                $workexperience->to=$data['date_to'][$i];
                $workexperience->reason_for_leaving=$data['reason_for_leave'][$i];
                $workexperience->save();
            };



            $count2=0;
            foreach ($data["language"] as $value){
                $count2=$count2+1;
            };

            for($i=0;$i<$count2;++$i){
                $languageknown = LanguageKnown::where('person_id',$id)->first();

                $languageknown->language=$data['language'][$i];
                $languageknown->speaking=$data['speak'][$i];
                $languageknown->reading=$data['read'][$i];
                $languageknown->writing=$data['write'][$i];
                $languageknown->save();
            };

            $count3=0;
            foreach ($data["knowledge_description"] as $value){
                $count3=$count3+1;
            };

            for($i=0;$i<$count3;++$i){
                $knowledge_description =TrainingTechKnowledge::where('person_id',$id)->first();

                $knowledge_description->knowledge=$data['knowledge_description'][$i];
                $knowledge_description->save();
            };


            $passport_detail= Passport::where('person_id',$id)->first();

            $passport_detail->passport_no=$data['passport-no'];
            $passport_detail->issue_date=$data['issue_date'];
            $passport_detail->expiry_date=$data['expiry_date'];
            $passport_detail->issue_place=$data['issue_place'];
            $passport_detail->birth_place=$data['birth_place'];
            $passport_detail->birth_date=$data['birth_date'];
            $passport_detail->age=$data['age'];

            if(($request->doc_img != null)){
                $passport_detail->image = $request->file('doc_img')->store('Documents','public');
            }
            $passport_detail->save();

            return redirect()->route('viewmanpowerlist');
        }
        return view('EMS.Manpower.edit',compact('personnel'));

    }

        public function deletemanpower($id){

        $personnel=PersonalDetail::findOrfail($id);
        $personnel->delete();
        return redirect()->route('viewmanpowerlist');

        }


    public function ajaxsearch(Request $request){

            if ($request->ajax()){
                $data = $request->all();
                $personnel = User::where('email', $data['email'])->get();
                if(($personnel->count()) > 0){

                    return view('EMS.Manpower.list',compact('personnel'));
                }
                else{
                    return view('EMS.Manpower.list')->with('msg','No Data for Query Found');

                }
            }
        return view('EMS.Manpower.list')->with('msg','No Data for Query Found');


    }



}
