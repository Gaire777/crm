<?php

namespace App\Http\Controllers\ERP;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use App\Cashin;
use App\Cashout;

class BalanceController extends Controller
{
    public function browse(){
        $cashins = Cashin::sum('amount_paid');
        $cashouts = Cashout::sum('amount');
        $net_sum = $cashins - $cashouts;


        /*
			For Each services cashin record
		*/
		$services = Service::all('id' ,'name');
        foreach($services as $service)
		{

			if (Cashin::where('service', '=', $service->id)->exists()) {
   			$service->service_sum_cashin = Cashin::where('service', '=', $service->id)->sum('amount_paid');
					}
					else{
						$service->service_sum_cashin = 0;
					}
        }
        

        /*
			For Each Expenditure cashout record
		*/
		
        $expenditures = DB::table('cashouts')
        ->select('expendituretype', DB::raw('SUM(amount) as expendituretype_sum_chashout'))
        ->groupBy('expendituretype')
        ->get();


        return view('EMS.balances.browse', compact('cashins', 'cashouts' , 'net_sum' , 'services' , 'expenditures'));
    }


    public function report(Request $request){

        $date1 = $request['date1'];
        $date2 = $request['date2'];


        $cashins = Cashin::whereBetween('date_paid', array($date1, $date2))->sum('amount_paid');
        $cashouts = Cashout::whereBetween('date', array($date1, $date2))->sum('amount');
        $net_sum = $cashins - $cashouts;


        /*
			For Each services cashin record
		*/
		$services = Service::all('id' ,'name');
        foreach($services as $service)
		{

			if (Cashin::where('service', '=', $service->id)->exists()) {


   			$service->service_sum_cashin = Cashin::where('service', '=', $service->id)->whereBetween('date_paid', array($date1, $date2))->sum('amount_paid');
					}
					else{
						$service->service_sum_cashin = 0;
					}
        }
        

        /*
			For Each Expenditure cashout record
		*/
		
        $expenditures = DB::table('cashouts')
        ->select('expendituretype', DB::raw('SUM(amount) as expendituretype_sum_chashout'))
        ->groupBy('expendituretype')->whereBetween('date', array($date1, $date2))
        ->get();


        return view('EMS.balances.browse', compact('cashins', 'cashouts' , 'net_sum' , 'services' , 'expenditures'));
    }




}
