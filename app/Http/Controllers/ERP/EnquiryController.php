<?php

namespace App\Http\Controllers\ERP;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enquiry;


class EnquiryController extends Controller
{
    public function enquirieslist()
    {

        $data['enquiries'] = Enquiry::with('categories')->get();

        return view('EMS.Enquiries.enquiries_list', $data);
    }
    public function enquiriesform()
    {

        $data['category'] = Category::all();
        return view('EMS.Enquiries.enquiries_form', $data);
    }
    public function addenquiries(Request $request)
    {

        // dd($request->all());

        // dd($request);

       

        $enquiries = new Enquiry();
        $enquiries->first_name = $request->first_name;
        $enquiries->last_name = $request->last_name;
        $enquiries->middle_name = $request->middle_name;
        $enquiries->email = $request->email;
        $enquiries->phone = $request->phone;
        $enquiries->address = $request->address;
        $enquiries->subject = $request->subject;
        $enquiries->qualification_level = $request->qualification_level;
        $enquiries->experience = $request->experience;
        $enquiries->country_intrested = json_encode($request->country_intrested);
        $enquiries->enquiry_from = $request->enquiry_from;
        $enquiries->source = $request->source;
        $enquiries->remarks = $request->remarks;
        $enquiries->responded_through = json_encode($request->responded_through);
        $enquiries->eligibility = $request->eligibility;
        $enquiries->category_id = $request->category_id;
        // $enquiry->added_by = Auth::user()->name;
        // dd(Auth::user()->id);

        // dd($enquiries);
        $enquiries->save();
        return redirect()->route('admin.enquiries.list');
    }




    public function getEditenquiries($id)
    {


        $data['enquiries'] = Enquiry::find($id);

        $data1['category'] = Category::all();
        // dd($data);

        $cis = json_decode($data['enquiries']->country_intrested);
        $rts = json_decode($data['enquiries']->responded_through);

        return view('EMS.Enquiries.enquiries_edit', $data1, $data)->withCis($cis)->withRts($rts);
    }

    public function postEditEnquiries(Request $request)
    {

        //        dd($request->all());

        // $request->validate([

        //     'first_name' => 'required|max:255',
        //     'last_name' => 'required|max:255',
        //     'middle_name' => 'max:255',
        //     'email' => 'required|max:255',
        //     'phone' => 'required|max:10',
        //     'address' => 'required|max:255',
        //     'subject' => 'required|max:255',
        //     'qualification_level' => 'required|max:255',
        //     'experience' => 'required|max:10',
        //     'country_intrested' => 'required',
        //     'service_intrested' => 'required',
        //     'enquiry_form' => 'required',
        //     'source' => 'required',
        //     'eligibility' => 'required',

        // ]);

    

        $enquiries = Enquiry::find($request->id);
        // dd($enquiries);
        $enquiries->first_name = $request->first_name;
        $enquiries->last_name = $request->last_name;
        $enquiries->middle_name = $request->middle_name;
        $enquiries->email = $request->email;
        $enquiries->phone = $request->phone;
        $enquiries->address = $request->address;
        $enquiries->subject = $request->subject;
        $enquiries->qualification_level = $request->qualification_level;
        $enquiries->experience = $request->experience;
        $enquiries->country_intrested = json_encode($request->country_intrested);
        $enquiries->enquiry_from = $request->enquiry_from;
        $enquiries->source = $request->source;
        $enquiries->remarks = $request->remarks;
        $enquiries->responded_through = json_encode($request->responded_through);
        $enquiries->eligibility = $request->eligibility;
        $enquiries->category_id = $request->category_id;
        // $enquiry->added_by = Auth::user()->name;
        // dd(Auth::user()->id);

        // dd($enquiries);
        $enquiries->update();
        return redirect()->route('admin.enquiries.list');
    }

    public function deleteEnquiries($id)
    {
        // dd($id);
        $enquiries = Enquiry::find($id);
        // dd($enquiries);

        $enquiries->delete();

        return redirect()->route('admin.enquiries.list');
    }
}
