<?php

namespace App\Http\Controllers\ERP;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employment;
use App\Applicant;
use App\Education;

class EmploymentController extends Controller
{
     /* Employment*/
    public function employmentlist(){
        $data['employment']=Employment::with('applicants')->paginate(10);
        // dd($data);
         
      return view('EMS.Employment.employmentlist',$data);
	}

	public function employmentform(){
	
		$data['applicant']=Applicant::all();
		// dd($data);
		return view('EMS.Employment.employmentadd',$data);
	}


	 public function employmentadd(Request $request){


        $request-> validate([
        'issuing_authority_name'=>'required',
        'issuing_authority_address'=>'required',
        'issuing_authority_country'=>'required',
        'issuing_authority_state'=>'required',
        'issuing_authority_city'=>'required', 
        'issuing_authority_country_code'=>'required',
        'issuing_authority_phone'=>'required || min:10',
        'reason_for_leaving'=>'required',
        'issuing_authority_email'=>'required',
        'issuing_authority_website'=>'required',
        'nature_of_employment'=>'required',
        'employment_from'=>'required',
        'employment_to'=>'required', 
        'designation'=>'required',
        'employee_code'=>'required || integer', 
        'department'=>'required',
        ]);

        $employment = new Employment();

            $employment->issuing_authority_name = $request->issuing_authority_name; 
            $employment->issuing_authority_address = $request->issuing_authority_address; 
            $employment->issuing_authority_country = $request->issuing_authority_country; 
            $employment->issuing_authority_state = $request->issuing_authority_state; 
            $employment->issuing_authority_city = $request->issuing_authority_city; 
            $employment->issuing_authority_country_code = $request->issuing_authority_country_code; 
            $employment->issuing_authority_phone = $request->issuing_authority_phone; 
            $employment->reason_for_leaving =$request->reason_for_leaving; 
            $employment->issuing_authority_email = $request->issuing_authority_email; 
            $employment->issuing_authority_website = $request->issuing_authority_website; 
            $employment->nature_of_employment = $request->nature_of_employment; 
            $employment->employment_from =$request->employment_from; 
            $employment->employment_to =$request->employment_to; 
            $employment->designation = $request->designation; 
            $employment->employee_code = $request->employee_code; 
            $employment->department = $request->department; 

     //    $request-> validate([
    	// 'issuing_authority_name'=>'required',
    	// 'issuing_authority_address'=>'required',
    	// 'issuing_authority_country'=>'required',
    	// 'issuing_authority_state'=>'required',
    	// 'issuing_authority_city'=>'required', 
    	// 'issuing_authority_country_code'=>'required',
    	// 'issuing_authority_phone'=>'required || integer || min:10',
    	// 'reason_for_leaving'=>'required',
    	// 'issuing_authority_email'=>'required',
    	// 'issuing_authority_website'=>'required',
    	// 'nature_of_employment'=>'required',
    	// 'employment_from'=>'required',
    	// 'employment_to'=>'required', 
    	// 'designation'=>'required',
    	// 'employee_code'=>'required', 
    	// 'department'=>'required',
    	// ]);

    	$employment = new Employment();

			$employment->issuing_authority_name = $request->issuing_authority_name; 
			$employment->issuing_authority_address = $request->issuing_authority_address; 
			$employment->issuing_authority_country = $request->issuing_authority_country; 
			$employment->issuing_authority_state = $request->issuing_authority_state; 
			$employment->issuing_authority_city = $request->issuing_authority_city; 
			$employment->issuing_authority_country_code = $request->issuing_authority_country_code; 
			$employment->issuing_authority_phone = $request->issuing_authority_phone; 
			$employment->reason_for_leaving =$request->reason_for_leaving; 
			$employment->issuing_authority_email = $request->issuing_authority_email; 
			$employment->issuing_authority_website = $request->issuing_authority_website; 
			$employment->nature_of_employment = $request->nature_of_employment; 
			$employment->employment_from =$request->employment_from; 
 			$employment->employment_to =$request->employment_to; 
 			$employment->designation = $request->designation; 
			$employment->employee_code = $request->employee_code; 
			$employment->department = $request->department; 



        if(($request->experience_letter != null)){
            $employment->experience_letter = $request->file('experience_letter')->store('experience_letter','public');
        }


            $employment->applicant_id= $request->applicant_id;

            $employment->save();

        \Session::flash('success','Employment Added Successfully');
        return redirect()->route('admin.employment.list');
    }

     public function employmentedit($id){
            // dd($id);
        $data['employment'] = Employment::find($id);
     	$data1['applicant']=Applicant::all();
		// dd($data);
		
     	
        
        // dd($data);
        return view('EMS.Employment.employmentedit',$data,$data1);
    }

    public function postemploymentedit(Request $request){
        // dd($request);
        $employment = Employment::find($request->id);
        
            $employment->issuing_authority_name = $request->issuing_authority_name; 
            $employment->issuing_authority_address = $request->issuing_authority_address; 
            $employment->issuing_authority_country = $request->issuing_authority_country; 
            $employment->issuing_authority_state = $request->issuing_authority_state; 
            $employment->issuing_authority_city = $request->issuing_authority_city; 
            $employment->issuing_authority_country_code = $request->issuing_authority_country_code; 
            $employment->issuing_authority_phone = $request->issuing_authority_phone; 
            $employment->reason_for_leaving =$request->reason_for_leaving; 
            $employment->issuing_authority_email = $request->issuing_authority_email; 
            $employment->issuing_authority_website = $request->issuing_authority_website; 
            $employment->nature_of_employment = $request->nature_of_employment; 
            $employment->employment_from =$request->employment_from; 
            $employment->employment_to =$request->employment_to; 
            $employment->designation = $request->designation; 
            $employment->employee_code = $request->employee_code; 
            $employment->department = $request->department; 
            $employment->applicant_id= $request->applicant_id;
            
            $employment->update();
        // dd($education);

         return redirect()->route('admin.employment.list');
    }

public function employmentdelete($id){

    $employment = Employment::find($id);
    $employment->delete();
       
        return redirect()->route('admin.employment.list');
  }
}
