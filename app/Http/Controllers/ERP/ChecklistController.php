<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document_check_list;
use App\Applicant;

class ChecklistController extends Controller
{
    
	public function checklistlist(){
        // $datas['checklist']=Applicant::all();
        $data['checklist']=Document_check_list::with('applicants')->paginate(10);
         // dd($data);
         
      return view('EMS.DocumentChecklist.checklistlist',$data);
	}

	public function checklistform(){
		$data['checklist'] = Applicant::all();
		// dd($data);
		return view('EMS.DocumentChecklist.checklistadd',$data);
	}


	 public function checklistadd(Request $request){
        // dd($request->all());

    	$request->validate([
    		
    	]);
    	
             $status= Applicant::find($request->applicants_name);
               // dd($status);

               $status->progress_sts=$request->progress_sts;
                $status->save();

    	$checklist = new Document_check_list();

    	$checklist->applicant_id = $request->applicants_name;


      

		$checklist->password_citizenship_certificate = $request->password_citizenship_certificate;
		$checklist->slc_certificate = $request->slc_certificate;
		$checklist->plus_two_isc_pcl_certificate = $request->plus_two_isc_pcl_certificate; 

		$checklist->diploma_degree_certificate = $request->diploma_degree_certificate; 
		$checklist->mark_sheet_of_each_year_final_transcript = $request->mark_sheet_of_each_year_final_transcript; 
		$checklist->equivalent_certificate =$request->equivalent_certificate;
		$checklist->council_registration_certificate = $request->council_registration_certificate; 
		$checklist->council_registration_certificate_renew = $request->council_registration_certificate_renew;
		$checklist->good_standing_letter_from_council = $request->good_standing_letter_from_council;
		$checklist->work_experience_letter_till_date = $request->work_experience_letter_till_date;
		$checklist->basic_life_support_for_nurses = $request->basic_life_support_for_nurses;
		$checklist->mrp_size_photo_in_white_background = $request->mrp_size_photo_in_white_background;



        $checklist->save();

    	\Session::flash('success','Product Added Successfully');
    	return redirect()->route('admin.checklist.list');
    }

     public function checklistedit($id){

        $data1['checklist']=Applicant::all();
        // dd($data);
        
        $data['app_checklist']= Document_check_list::find($id);
             // dd($data);



        return view('EMS.DocumentChecklist.checklistedit',$data,$data1);

    }

    public function postchecklistedit(Request $request){
        
        
        
              $status= Applicant::find($request->applicants_name);
               // dd($status);

               $status->progress_sts=$request->progress_sts;
                $status->update();

    	// dd($request->id);
       $checklist= Document_check_list::find($request->id);
       // dd($request);

        $checklist->applicant_id = $request->applicants_name;

        $checklist->password_citizenship_certificate = $request->password_citizenship_certificate;
        $checklist->slc_certificate = $request->slc_certificate;
        $checklist->plus_two_isc_pcl_certificate = $request->plus_two_isc_pcl_certificate; 

        $checklist->diploma_degree_certificate = $request->diploma_degree_certificate; 
        $checklist->mark_sheet_of_each_year_final_transcript = $request->mark_sheet_of_each_year_final_transcript; 
        $checklist->equivalent_certificate =$request->equivalent_certificate;
        $checklist->council_registration_certificate = $request->council_registration_certificate; 
        $checklist->council_registration_certificate_renew = $request->council_registration_certificate_renew;
        $checklist->good_standing_letter_from_council = $request->good_standing_letter_from_council;
        $checklist->work_experience_letter_till_date = $request->work_experience_letter_till_date;
        $checklist->basic_life_support_for_nurses = $request->basic_life_support_for_nurses;
        $checklist->mrp_size_photo_in_white_background = $request->mrp_size_photo_in_white_background;




        $checklist->update();

         // dd($data);
         
         return redirect()->route('admin.checklist.list');
    }


public function checklistdelete($id){

    $checklist = Document_check_list::find($id);
    // dd($checklist);
    $checklist->delete();
        \Session::flash('success', 'Checklist Deleted Successfully');

        return redirect()->route('admin.checklist.list');
  }
}