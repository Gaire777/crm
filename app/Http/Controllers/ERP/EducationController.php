<?php

namespace App\Http\Controllers\ERP;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Education;
use App\Applicant;


class EducationController extends Controller
{
     /* Product*/
	public function educationlist(){
		$data1['applicant']=Education::with('applicants')->paginate(10);
		// dd($data1);
        // $data['education']=Education::all();
         
      return view('EMS.Educations.educationlist',$data1);
	}

	public function educationform(){
	
		$data['applicant']=Applicant::all();
		// dd($data);
		return view('EMS.Educations.educationadd',$data);
	}


	 public function educationadd(Request $request){
       // dd($request->all());

    	$request->validate([
    		
    	]);


    	$education = new Education();
		    	
		$education->applicant_id  = $request->applicants_name;
		$education->authority_name  = $request->authority_name;
		$education->authority_address  = $request->authority_address; 
		$education->authority_city  = $request->authority_city;
		$education->authority_state  = $request->authority_state;
		$education->authority_country  = $request->authority_country;
		$education->authority_phone_type  = $request->authority_phone_type;
		$education->authority_country_code  = $request->authority_country_code;
		$education->authority_phone  = $request->authority_phone;
		$education->authority_email  = $request->authority_email;
		$education->authority_website  = $request->authority_website;
		$education->qualification  = $request->qualification;
		$education->institution  = $request->institution;
		$education->type  = $request->type;
		$education->mode  = $request->mode;
		$education->major_subject  = $request->major_subject; 
		$education->minor_subject  = $request->minor_subject;
		$education->roll  = $request->roll;
		$education->study_from  = $request->study_from; 
		$education->study_to  = $request->study_to;
		$education->conferred_date  = $request->conferred_date;
		$education->degree_issue_date  = $request->degree_issue_date;
		$education->expected_degree_issue_date  = $request->expected_degree_issue_date;

  		 if(($request->qualification_certificate != null)){
            $education->qualification_certificate = $request->file('qualification_certificate')->store('qualification_certificate','public');
        }

        if(($request->marksheet != null)){
           $education->marksheet = $request->file('marksheet')->store('marksheet','public');
        }
		
		$education->save();

    	\Session::flash('success','Product Added Successfully');
    	return redirect()->route('admin.education.list');
    }

     public function educationedit($id){

     	$data1['applicant']=Applicant::all();
		// dd($data);
		
     	$data['education'] = Education::find($id);
        // dd($data);



        return view('EMS.Educations.educationedit',$data,$data1);
    }

    public function posteducationedit(Request $request){
    	// dd($request->id);
        $education = Education::find($request->id);

        $education->applicant_id  = $request->applicants_name;
		$education->authority_name  = $request->authority_name;
		$education->authority_address  = $request->authority_address; 
		$education->authority_city  = $request->authority_city;
		$education->authority_state  = $request->authority_state;
		$education->authority_country  = $request->authority_country;
		$education->authority_phone_type  = $request->authority_phone_type;
		$education->authority_country_code  = $request->authority_country_code;
		$education->authority_phone  = $request->authority_phone;
		$education->authority_email  = $request->authority_email;
		$education->authority_website  = $request->authority_website;
		$education->qualification  = $request->qualification;
		$education->institution  = $request->institution;
		$education->type  = $request->type;
		$education->mode  = $request->mode;
		$education->major_subject  = $request->major_subject; 
		$education->minor_subject  = $request->minor_subject;
		$education->roll  = $request->roll;
		$education->study_from  = $request->study_from; 
		$education->study_to  = $request->study_to;
		$education->conferred_date  = $request->conferred_date;
		$education->degree_issue_date  = $request->degree_issue_date;
		$education->expected_degree_issue_date  = $request->expected_degree_issue_date;

  		 if(($request->qualification_certificate != null)){
            $education->qualification_certificate = $request->file('qualification_certificate')->store('qualification_certificate','public');
        }

        if(($request->marksheet != null)){
           $education->marksheet = $request->file('marksheet')->store('marksheet','public');
        }
		
		$education->save();

    	\Session::flash('success','Product Added Successfully');
    	return redirect()->route('admin.education.list');
    }

    public function educationdelete($id){
    
        $education = Education::find($id);
    	
    
    
        $education->delete();
           
    
            return redirect()->route('admin.education.list');
      }
}
