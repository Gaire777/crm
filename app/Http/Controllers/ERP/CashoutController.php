<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cashout;
use Auth;
use App\Applicant;


class CashoutController extends Controller
{
    //
    public function listcashout(){
    	$data['cashouts']=Cashout::with('applicants')->get();
        // dd($data);
    	return view('EMS.cashout.list',$data);
    }
    public function formcashout(){
   
     
     	$data['applicants']=Applicant::all();
        
        
        return view('EMS.cashout.form',$data);
}
public function addcashout(Request $request){
	 // dd($request);

  $request->validate([
         
            'amount' => 'required|integer',
           
          
      ]);

	
		 $cashouts = new Cashout();
        $cashouts->expendituretype = $request->expendituretype;
        $cashouts->amount = $request->amount;
        $cashouts->withdrawnby = $request->applicant_id;
        $cashouts->date = $request->date;
        $cashouts->remarks = $request->remarks;
       
         // $enquiry->added_by = Auth::user()->name;
        // dd(Auth::user()->id);
        $cashouts->created_at = date('Y-m-d H:i:s');
        // dd($enquiries);
        $cashouts->save();
        return redirect()->route('admin.cashout.list');
	}
   public function getedit($id){
        $data['applicants']= Applicant::all();
        $data1['cashouts']=Cashout::find($id);

        // dd($data1);

      
      
      return view('EMS.cashout.edit',$data,$data1);

      
      }

       public function postedit(Request $request){

        $request->validate([
         
            'price' => 'required|integer',
           
          
      ]);
        
      // dd($request);
     $cashouts = Cashout::find($request->id);
        $cashouts->expendituretype = $request->expendituretype;
        $cashouts->amount = $request->amount;
        $cashouts->withdrawnby = $request->applicant_id;
        $cashouts->date = $request->date;
        $cashouts->remarks = $request->remarks;
       
         // $enquiry->added_by = Auth::user()->name;
        // dd(Auth::user()->id);
        $cashouts->created_at = date('Y-m-d H:i:s');
        // dd($enquiries);
        $cashouts->save();
        return redirect()->route('admin.cashout.list');
      }

     public function deletecashout($id){

          $cashout=Cashout::find($id);
          
          
          $cashout->delete();

        return redirect()->route('admin.cashout.list');
      }

}
