<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Progressflow;
use Auth;
use App\Applicant;

class ProgressFlowController extends Controller
{
 
    
    public function showlist(){

    	// $data['progressflow']=ProgressFlow::all();
      $data['progressflow']=Progressflow::with('applicant')->paginate(10);
      // dd($data);
    	return view('EMS.progressflow.list',$data);
    }
     public function showform(){
      $data['applicant'] = Applicant::all();
      // dd($data);
     	
        
        
        return view('EMS.progressflow.form',$data);
}
public function addprogressflow(Request $request){
	// dd($request);
    $request->validate([

'profession'=>'required',
'email'=>'required||email',
'contact_number'=>'required||integer||min:10',
'date_of_birth'=>'required',

'passport_number'=> 'required',
'signed_by_applicant'=> 'required',
'service_charge'=>'required||integer',
'service_paid_date'=>'required',
'service_mode_of_payment'=> 'required',
'service_charge_received_by'=>'required||integer',
'dhamcq_fee'=>'required||integer',

'dhamcq_mode_of_payment'=>'required',
'dhamcq_subject'=> 'required',
'dhamcq_username'=>'required',
'dhamcq_password'=>'required',
'dhamcq_email_sent'=> 'required',
'books_provided'=>'required',
'bls_training_completed_date'=>'required',
'good_standing_certificate_issue_date'=> 'required',
'equivalent_certificate'=>'required',

'dha_email_account'=>'required',
'dha_unique_id'=>'required||integer',
'dha_username'=>'required',
'dha_password'=>'required',
'dha_application_ref_number'=>'required',
'dha_fees_first_installment'=>'required||integer',
'first_installment_paid_date'=>'required',
'first_installment_mode_of_payment'=>'required',
'first_installment_received_by'=>'required',
'dha_fees_second_installment'=>'required||integer',
'second_installment_paid_date'=>'required',
'second_installment_mode_of_payment'=> 'required',
'second_installment_received_by'=>'required',

'dataflow_email'=>'required',
'dataflow_username'=> 'required',
'dataflow_password'=>'required',
'dataflow_ref_no'=>'required||integer',
'dha_exam_eligibility_id'=> 'required',
'eligibility_date'=>'required',
'exam_date_confirmed'=> 'required',
'send_confirmation_to_candidate'=>'required', 
'exam_result'=>'required',
'data_flow_report'=> 'required',


]);


$progressflow = new ProgressFlow();


$progressflow->applicant_id = $request->applicant_id;
$progressflow->profession = $request->profession;
$progressflow->email = $request->email;
$progressflow->contact_number=$request->contact_number; 
$progressflow->date_of_birth = $request->date_of_birth;

$progressflow->passport_number = $request->passport_number;
$progressflow->signed_by_applicant = $request->signed_by_applicant;
$progressflow->service_charge = $request->service_charge;
$progressflow->service_paid_date = $request->service_paid_date;
$progressflow->service_mode_of_payment = $request->service_mode_of_payment;
$progressflow->service_charge_received_by = $request->service_charge_received_by;
$progressflow->dhamcq_fee = $request->dhamcq_fee;

$progressflow->dhamcq_mode_of_payment = $request->dhamcq_mode_of_payment;
$progressflow->dhamcq_subject = $request->dhamcq_subject;
$progressflow->dhamcq_username = $request->dhamcq_username;
$progressflow->dhamcq_password = $request->dhamcq_password;
$progressflow->dhamcq_email_sent = $request->dhamcq_email_sent;
$progressflow->books_provided = $request->books_provided;
$progressflow->bls_training_completed_date = $request->bls_training_completed_date;
$progressflow->good_standing_certificate_issue_date = $request->good_standing_certificate_issue_date;
$progressflow->equivalent_certificate = $request->equivalent_certificate;

$progressflow->dha_email_account = $request->dha_email_account;
$progressflow->dha_unique_id = $request->dha_unique_id;
$progressflow->dha_username = $request->dha_username;
$progressflow->dha_password = $request->dha_password;
$progressflow->dha_application_ref_number = $request->dha_application_ref_number;
$progressflow->dha_fees_first_installment = $request->dha_fees_first_installment;
$progressflow->first_installment_paid_date = $request->first_installment_paid_date;
$progressflow->first_installment_mode_of_payment = $request->first_installment_mode_of_payment;
$progressflow->first_installment_received_by = $request->first_installment_received_by;
$progressflow->dha_fees_second_installment = $request->dha_fees_second_installment;
$progressflow->second_installment_paid_date = $request->second_installment_paid_date;
$progressflow->second_installment_mode_of_payment = $request->second_installment_mode_of_payment;
$progressflow->second_installment_received_by = $request->second_installment_received_by;

$progressflow->dataflow_email = $request->dataflow_email;
$progressflow->dataflow_username = $request->dataflow_username;
$progressflow->dataflow_password = $request->dataflow_password;
$progressflow->dataflow_ref_no = $request->dataflow_ref_no;
$progressflow->dha_exam_eligibility_id = $request->dha_exam_eligibility_id;
$progressflow->eligibility_date = $request->eligibility_date;
$progressflow->exam_date_confirmed = $request->exam_date_confirmed;
$progressflow->send_confirmation_to_candidate = $request->send_confirmation_to_candidate;
$progressflow->exam_result = $request->exam_result;
$progressflow->data_flow_report = $request->data_flow_report;
$progressflow->remarks = $request->remarks;        

if(($request->signed_docs != null)){
            $progressflow->signed_docs = $request->file('signed_docs')->store('signeddocs','public');
        }

    


// $progressflow->added_by = Auth::user()->name;
//         // dd(Auth::user()->id);
        $progressflow->created_at = date('Y-m-d H:i:s');
//         // dd($progressflow);
        $progressflow->save();

return redirect()->route('admin.progressflow.list');


} 


    public function getEditprogressflow($id){
        $data['progressflow'] = ProgressFlow::find($id);
        $data1['applicant'] = Applicant::all();
        // dd($data);
        
        return view('EMS.progressflow.edit',$data,$data1);
    }

    public function postEditprogressflow(Request $request){

       // dd($request);
$progressflow = ProgressFlow::find($request->id);


$progressflow->applicant_id = $request->applicant_id;
$progressflow->profession = $request->profession;
$progressflow->email = $request->email;
$progressflow->contact_number=$request->contact_number; 
$progressflow->date_of_birth = $request->date_of_birth;

$progressflow->passport_number = $request->passport_number;
$progressflow->signed_by_applicant = $request->signed_by_applicant;
$progressflow->signed_docs = $request->signed_docs;
$progressflow->service_charge = $request->service_charge;
$progressflow->service_paid_date = $request->service_paid_date;
$progressflow->service_mode_of_payment = $request->service_mode_of_payment;
$progressflow->service_charge_received_by = $request->service_charge_received_by;
$progressflow->dhamcq_fee = $request->dhamcq_fee;

$progressflow->dhamcq_mode_of_payment = $request->dhamcq_mode_of_payment;
$progressflow->dhamcq_subject = $request->dhamcq_subject;
$progressflow->dhamcq_username = $request->dhamcq_username;
$progressflow->dhamcq_password = $request->dhamcq_password;
$progressflow->dhamcq_email_sent = $request->dhamcq_email_sent;
$progressflow->books_provided = $request->books_provided;
$progressflow->bls_training_completed_date = $request->bls_training_completed_date;
$progressflow->good_standing_certificate_issue_date = $request->good_standing_certificate_issue_date;
$progressflow->equivalent_certificate = $request->equivalent_certificate;

$progressflow->dha_email_account = $request->dha_email_account;
$progressflow->dha_unique_id = $request->dha_unique_id;
$progressflow->dha_username = $request->dha_username;
$progressflow->dha_password = $request->dha_password;
$progressflow->dha_application_ref_number = $request->dha_application_ref_number;
$progressflow->dha_fees_first_installment = $request->dha_fees_first_installment;
$progressflow->first_installment_paid_date = $request->first_installment_paid_date;
$progressflow->first_installment_mode_of_payment = $request->first_installment_mode_of_payment;
$progressflow->first_installment_received_by = $request->first_installment_received_by;
$progressflow->dha_fees_second_installment = $request->dha_fees_second_installment;
$progressflow->second_installment_paid_date = $request->second_installment_paid_date;
$progressflow->second_installment_mode_of_payment = $request->second_installment_mode_of_payment;
$progressflow->second_installment_received_by = $request->second_installment_received_by;

$progressflow->dataflow_email = $request->dataflow_email;
$progressflow->dataflow_username = $request->dataflow_username;
$progressflow->dataflow_password = $request->dataflow_password;
$progressflow->dataflow_ref_no = $request->dataflow_ref_no;
$progressflow->dha_exam_eligibility_id = $request->dha_exam_eligibility_id;
$progressflow->eligibility_date = $request->eligibility_date;
$progressflow->exam_date_confirmed = $request->exam_date_confirmed;
$progressflow->send_confirmation_to_candidate = $request->send_confirmation_to_candidate;
$progressflow->exam_result = $request->exam_result;
$progressflow->data_flow_report = $request->data_flow_report;
$progressflow->remarks = $request->remarks;        


// $progressflow->added_by = Auth::user()->name;
//         // dd(Auth::user()->id);
        $progressflow->created_at = date('Y-m-d H:i:s');

         if(($request->signed_docs != null)){
             Storage::delete('public/'.$progressflow->signed_docs);
             $path = $request->file('signed_docs')->store('signeddocs', 'public');
            $progressflow->signed_docs = $path;

             $progressflow->updated_at = date('Y-m-d H:i:s');
              $progressflow->update();
            $progressflow->signed_docs = $request->file('signed_docs')->store('signeddocs','public');

      } 

        $progressflow->update();
        return redirect()->route('admin.progressflow.list');


    
  }
    
  public function deleteprogressflow($id){
// dd($id);
    $progressflow = ProgressFlow::find($id);
        // dd($enquiries);
      
          $progressflow->delete();

        return redirect()->route('admin.progressflow.list');
  }
    

}   
