<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;

class ServiceController extends Controller
{
    public function servicelist(){
        $data['service']=Service::all();
        // dd($data);
      return view('EMS.Services1.list',$data);
	}

	


	 public function serviceadd(Request $request){
       // dd($request->all());

    	 $request->validate([
         
            'servicename' => 'required|max:255',
            'price' => 'required|integer',
           
      ]);
    	// dd($request);


    	$service = new Service();
		$service->name = $request->servicename;
		$service->price= $request->price;
		$service->save();


    	\Session::flash('success','Product Added Successfully');
    	return redirect()->route('admin.service.list');
    }

     public function serviceform(){
       // dd($request->all());

   
    	return view('EMS.Services1.add');
    }

    public function editservice($id){

    	$data['service']=Service::find($id);
        


    	return view('EMS.Services1.edit',$data);


    }

    public function postedit(Request $request){
			
         $request->validate([
         
            'servicename' => 'required|max:255',
            'price' => 'required|integer',
          

          
      ]);

    	// dd($request);
    	$service=Service::find($request->id);
    	$service->name = $request->servicename;
    	$service->price = $request->price;
    	$service->update();

    	return redirect()->route('admin.service.list');


    }

     public function deleteservice($id){

    	// dd($request);
    	$service=Service::find($id);
    	
    	$service->delete();

    	return redirect()->route('admin.service.list')->with('flash_message', ' Your Service Has Been Deleted');;


    }
}
