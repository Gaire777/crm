<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\Enquiry;
use App\Cashout;
use App\Cashin;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['app_count']=Applicant::count();
        $data['enq_count']=Enquiry::count();
        $data['cashins']=Cashin::sum('amount_paid');
        $data['cashouts']=Cashout::sum('amount');

        // dd($data);


        return view('dashboard',$data);
    }
}
