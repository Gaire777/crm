<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Booking;
use App\Service;
use App\Customer;

class BookingController extends Controller
{

    // public function booking($s_id)
    // {
    //     return view('front.booking')->with('service_id', $s_id);
    // }

    // public function mybooking()
    // {
    //     $id = auth()->user()->id;
    //     // dd($id);
    //     $bookings = Booking::where('customer_id', $id)->get();

    //     $services = Service::all();
    //     return view('front.profile.my-booking')->withBookings($bookings)->withServices($services);

    // }

    public function create()
    {
        return view('booking.create');
    }

    public function store(Request $request)
    {

        $request->validate([

            'name' => 'required',
            'email' => 'required | email',
            'phone' => 'required | min:10',
            'date' => 'required|date',
            'time' => 'required',

        ]);

        $booking = new Booking;
        $booking->name = $request['name'];
        $booking->email = $request['email'];
        $booking->phone = $request['phone'];
        $booking->date = $request['date'];
        $booking->time = $request['time'];
        $booking->status = "To Be Assign";

        $booking->save();

        // if ($request['role'] == "front") {
        //     return redirect()->route('mybooking')->withStatus(__('Booking successfull.'));
        // } else {
            return redirect()->route('admin.booking.index')->withStatus(__('Booking successfully Created.'));
        // }
    }
    public function edit($id)
    {
        $booking = Booking::find($id);
        // dd($data);
        return view('booking.edit')->withBooking($booking);
    }

    public function index()
    {
        $bookings = Booking::all();
        return view('booking.index')->withBookings($bookings);
    }

    public function update(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'name' => 'required',
            'email' => 'required | email',
            'phone' => 'required | min:10',
            'date' => 'required|date',
            'time' => 'required',
        ]);

        $booking = Booking::find($request->id);
        $booking->name = $request['name'];
        $booking->email = $request['email'];
        $booking->phone = $request['phone'];
        $booking->date = $request['date'];
        $booking->time = $request['time'];
        $booking->status = $request['status'];

        $booking->save();

        return redirect()->route('admin.booking.index')->withStatus(__('Booking successfully Updated.'));
    }

    public function destroy($id)
    {
        $data['booking'] = Booking::find($id);
        // dd($data);
        $data['booking']->delete();

        // if ($request['role'] == "front") {
        //     return redirect()->route('mybooking')->withStatus(__('Booking Canceled successfull.'));
        // } else {
            return redirect()->route('admin.booking.index')->withStatus(__('Booking successfully Deleted.'));
        // }
    }
}
