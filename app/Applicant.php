<?php

namespace App;
use Illuminate\Notifications\Notifiable;


use Illuminate\Database\Eloquent\Model;

class Applicant extends Model 
{
    use Notifiable;
    public function health_license()
{
    return $this->belongsTo(Health_license::class);
}

 public function document_check_lists(){

    	return $this->belongsTo(Document_check_list::class);
    }

public function progressflow(){

	return $this->belongsTo(Progressflow::class);
}
 public function education(){

    	return $this->belongsTo(Education::class);
    }

     public function cashin(){

        return $this->belongsTo(Cashin::class);
    }

     public function cashout(){

        return $this->belongsTo(Cashout::class);
    }

    public function invoice(){

        return $this->belongsTo(Invoice::class);
    }
    
    public function categories(){

    	return $this->hasOne(Category::class,'id','applicant_category');
    }
}