<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashin extends Model
{
     public function applicants()
{
    return $this->hasOne(Applicant::class,'id','paidby');
}

     public function service()
{
    return $this->hasOne(Service::class,'id','service');
}


     public function users()
{
    return $this->hasOne(User::class,'id','receivedby');
}
}
