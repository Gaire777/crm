<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
      public function cashin()
{
    return $this->hasMany(Cashin::class,'service','name');
}

public function invoice()
{
    return $this->hasMany(Invoice::class,'service','name');
}
 public function enquiry(){
    	return $this->hasMany(Enquiry::class);
    }
}
