<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
      
      protected $fillable = ['marked_as'];

 public function service()
{
    return $this->belongsTo(Service::class,'service');
}

    public function applicants()
{
    return $this->hasOne(Applicant::class,'id','applicant');
}

    public function users()
{
    return $this->hasOne(User::class,'id','receivedby');
}
}
