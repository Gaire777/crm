<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressFlow extends Model
{
    protected $table='progressflow';

	    public function applicant(){

    	return $this->hasOne(Applicant::class,'id','applicant_id');
    }
}
