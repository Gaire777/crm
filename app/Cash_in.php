<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash_in extends Model
{
    public function client()
{
    return $this->belongsTo(Client::class,'paidby');
}

 public function service()
{
    return $this->belongsTo(Service::class,'service');
}
}
