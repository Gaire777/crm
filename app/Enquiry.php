<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Enquiry extends Model
{
    use Notifiable;

        public function categories()
        {
    	return $this->belongsTo(Category::class,'category_id','id');
    }

}
