<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'applicants';

    /**
     * Run the migrations.
     * @table applicants
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('first_name', 200)->nullable()->default(null);
            $table->string('middel_name', 200)->nullable()->default(null);
            $table->string('surname', 200)->nullable()->default(null);
            $table->string('maiden_name', 200)->nullable()->default(null);
            $table->string('gender', 200)->nullable()->default(null);
            $table->date('dob')->nullable()->default(null);
            $table->string('identity_type', 200)->nullable()->default(null);
            $table->string('identity_card_no', 200)->nullable()->default(null);
            $table->string('passport_no', 200)->nullable()->default(null);
            $table->string('birth_country', 200)->nullable()->default(null);
            $table->string('country_code', 200)->nullable()->default(null);
            $table->string('mobile_no', 200)->nullable()->default(null);
            $table->string('current_country', 200)->nullable()->default(null);
            $table->string('nationality', 200)->nullable()->default(null);
            $table->string('email', 200)->nullable()->default(null);
            $table->string('passport_docs', 200)->nullable()->default(null);
            $table->unsignedInteger('applicant_category')->nullable()->default(null);
            $table->unsignedInteger('services_id')->nullable()->default(null);
            $table->unsignedInteger('enquired_id')->nullable()->default(null);
            $table->integer('progress_sts')->nullable()->default(null);

            $table->index(["enquired_id"], 'enquiry_idx');

            $table->index(["applicant_category"], 'cat');
            $table->nullableTimestamps();


            $table->foreign('applicant_category', 'cat')
                ->references('id')->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('enquired_id', 'enquiry_idx')
                ->references('id')->on('enquiries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
