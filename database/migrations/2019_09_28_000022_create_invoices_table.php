<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'invoices';

    /**
     * Run the migrations.
     * @table invoices
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('applicant')->nullable()->default(null);
            $table->date('date')->nullable()->default(null);
            $table->string('marked_as', 100)->nullable()->default(null);
            $table->float('price')->nullable()->default(null);
            $table->unsignedInteger('service')->nullable()->default(null);
            $table->unsignedBigInteger('receivedby')->nullable()->default(null);

            $table->index(["service"], 'service_idx');

            $table->index(["receivedby"], 'received_idx');

            $table->index(["applicant"], 'app_idx');
            $table->nullableTimestamps();


            $table->foreign('receivedby', 'received_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('service', 'service_idx')
                ->references('id')->on('services')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
