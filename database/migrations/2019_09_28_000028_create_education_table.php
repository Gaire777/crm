<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'education';

    /**
     * Run the migrations.
     * @table education
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('authority_name', 200)->nullable()->default(null);
            $table->string('authority_address', 200)->nullable()->default(null);
            $table->string('authority_city', 200)->nullable()->default(null);
            $table->string('authority_state', 200)->nullable()->default(null);
            $table->string('authority_country', 200)->nullable()->default(null);
            $table->string('authority_phone_type', 200)->nullable()->default(null);
            $table->string('authority_country_code', 200)->nullable()->default(null);
            $table->string('authority_phone', 200)->nullable()->default(null);
            $table->string('authority_email', 200)->nullable()->default(null);
            $table->string('authority_website', 200)->nullable()->default(null);
            $table->string('qualification', 200)->nullable()->default(null);
            $table->string('institution', 200)->nullable()->default(null);
            $table->string('type', 200)->nullable()->default(null);
            $table->string('mode', 200)->nullable()->default(null);
            $table->string('major_subject', 200)->nullable()->default(null);
            $table->string('minor_subject', 200)->nullable()->default(null);
            $table->string('roll', 200)->nullable()->default(null);
            $table->date('study_from')->nullable()->default(null);
            $table->date('study_to')->nullable()->default(null);
            $table->date('conferred_date')->nullable()->default(null);
            $table->date('degree_issue_date')->nullable()->default(null);
            $table->date('expected_degree_issue_date')->nullable()->default(null);
            $table->string('qualification_certificate', 200)->nullable()->default(null);
            $table->string('marksheet', 200)->nullable()->default(null);
            $table->unsignedInteger('applicant_id')->nullable()->default(null);

            $table->index(["applicant_id"], 'appid');
            $table->nullableTimestamps();


            $table->foreign('applicant_id', 'appid')
                ->references('id')->on('applicants')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
