<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateProgressFlowReportsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'candidate_progress_flow_reports';

    /**
     * Run the migrations.
     * @table candidate_progress_flow_reports
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 200)->nullable()->default(null);
            $table->string('profession', 200)->nullable()->default(null);
            $table->string('email', 200)->nullable()->default(null);
            $table->string('contact_number', 200)->nullable()->default(null);
            $table->date('date_of_birth')->nullable()->default(null);
            $table->string('password_number', 100)->nullable()->default(null);
            $table->integer('service_charge')->nullable()->default(null);
            $table->string('books')->nullable()->default(null);
            $table->string('bls')->nullable()->default(null);
            $table->date('bls_date')->nullable()->default(null);
            $table->date('good_standing_certificate_issue_date')->nullable()->default(null);
            $table->string('equivalent_certificate', 200)->nullable()->default(null);
            $table->integer('dhamcq_charge')->nullable()->default(null);
            $table->string('dhamcq_provided', 200)->nullable()->default(null);
            $table->string('dhamcq_performance', 200)->nullable()->default(null);
            $table->date('preferred_exam_date')->nullable()->default(null);
            $table->string('email_account')->nullable()->default(null);
            $table->string('dha_account', 200)->nullable()->default(null);
            $table->string('dha_username', 200)->nullable()->default(null);
            $table->string('dha_ref_number', 200)->nullable()->default(null);
            $table->string('dha_fees_first_installment', 200)->nullable()->default(null);
            $table->string('dha_fees_second_installment', 200)->nullable()->default(null);
            $table->date('paid_date')->nullable()->default(null);
            $table->string('document_upload', 200)->nullable()->default(null);
            $table->string('payment', 200)->nullable()->default(null);
            $table->string('approval_for_psv_verification', 200)->nullable()->default(null);
            $table->string('payments', 200)->nullable()->default(null);
            $table->string('data_flow_initiated', 200)->nullable()->default(null);
            $table->string('barcode', 100)->nullable()->default(null);
            $table->string('exam_eligibility_id_generated', 100)->nullable()->default(null);
            $table->string('activated', 100)->nullable()->default(null);
            $table->string('dhamcq_performance_revision', 200)->nullable()->default(null);
            $table->string('selection_of_dha_exam_date', 100)->nullable()->default(null);
            $table->string('send_confirmation_to_candidate_email', 200)->nullable()->default(null);
            $table->string('sms', 200)->nullable()->default(null);
            $table->string('exam_result', 100)->nullable()->default(null);
            $table->string('data_flow_report', 200)->nullable()->default(null);
            $table->string('dha_eligibility', 100)->nullable()->default(null);
            $table->string('updated_cv', 98)->nullable()->default(null);
            $table->string('remarks')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
