<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'menu_items';

    /**
     * Run the migrations.
     * @table menu_items
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('menu_id')->nullable()->default(null);
            $table->string('title', 191);
            $table->string('url', 191);
            $table->string('target', 191)->default('_self');
            $table->string('icon_class', 191)->nullable()->default(null);
            $table->string('color', 191)->nullable()->default(null);
            $table->integer('parent_id')->nullable()->default(null);
            $table->integer('order');
            $table->string('route', 191)->nullable()->default(null);
            $table->text('parameters')->nullable()->default(null);

            $table->index(["menu_id"], 'menu_items_menu_id_foreign');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
