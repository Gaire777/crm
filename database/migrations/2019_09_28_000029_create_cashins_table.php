<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashinsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cashins';

    /**
     * Run the migrations.
     * @table cashins
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('paidby')->nullable()->default(null);
            $table->unsignedInteger('service')->nullable()->default(null);
            $table->unsignedBigInteger('receivedby');
            $table->date('date_paid')->nullable()->default(null);
            $table->string('remarks')->nullable()->default(null);
            $table->integer('amount_paid')->nullable()->default(null);
            $table->unsignedInteger('invoice_id')->nullable()->default(null);

            

            $table->index(["invoice_id"], 'invoice_id');

            $table->index(["paidby"], 'paid_idx');

            $table->index(["receivedby"], 'receivedby_idx');

            

            
            $table->nullableTimestamps();


            $table->foreign('invoice_id', 'invoice_id')
                ->references('id')->on('invoices')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('paidby', 'paid_idx')
                ->references('id')->on('applicants')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('receivedby', 'receivedby_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
