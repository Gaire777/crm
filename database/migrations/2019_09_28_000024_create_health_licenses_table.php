<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthLicensesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'health_licenses';

    /**
     * Run the migrations.
     * @table health_licenses
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('applicant_id')->nullable()->default(null);
            $table->string('professional_designation', 200)->nullable()->default(null);
            $table->string('issuing_authority_name', 200)->nullable()->default(null);
            $table->string('issuing_authority_country', 200)->nullable()->default(null);
            $table->string('issuing_authority_city', 200)->nullable()->default(null);
            $table->date('license_conferred_date')->nullable()->default(null);
            $table->date('license_expiry_date')->nullable()->default(null);
            $table->string('license_type', 200)->nullable()->default(null);
            $table->string('license_number', 200)->nullable()->default(null);
            $table->string('license_status', 200)->nullable()->default(null);
            $table->string('license_attained', 200)->nullable()->default(null);
            $table->string('license_copy', 200)->nullable()->default(null);

            $table->index(["applicant_id"], 'applicant_idx');

            $table->index(["applicant_id"], 'app_idx');
            $table->nullableTimestamps();


            $table->foreign('applicant_id', 'applicant_idx')
                ->references('id')->on('applicants')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
