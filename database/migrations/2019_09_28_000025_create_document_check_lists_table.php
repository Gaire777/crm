<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentCheckListsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'document_check_lists';

    /**
     * Run the migrations.
     * @table document_check_lists
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('applicant_id')->nullable()->default(null);
            $table->string('password_citizenship_certificate', 100)->nullable()->default(null);
            $table->string('slc_certificate', 100)->nullable()->default(null);
            $table->string('plus_two_isc_pcl_certificate', 100)->nullable()->default(null);
            $table->string('diploma_degree_certificate', 100)->nullable()->default(null);
            $table->string('mark_sheet_of_each_year_final_transcript', 100)->nullable()->default(null);
            $table->string('equivalent_certificate', 100)->nullable()->default(null);
            $table->string('council_registration_certificate', 100)->nullable()->default(null);
            $table->string('council_registration_certificate_renew', 100)->nullable()->default(null);
            $table->string('good_standing_letter_from_council', 100)->nullable()->default(null);
            $table->string('work_experience_letter_till_date', 100)->nullable()->default(null);
            $table->string('basic_life_support_for_nurses', 100)->nullable()->default(null);
            $table->string('mrp_size_photo_in_white_background', 100)->nullable()->default(null);

            $table->index(["applicant_id"], 'dc_idx');
            $table->nullableTimestamps();


            $table->foreign('applicant_id', 'dc_idx')
                ->references('id')->on('applicants')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
