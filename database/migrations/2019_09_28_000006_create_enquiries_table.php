<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiriesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'enquiries';

    /**
     * Run the migrations.
     * @table enquiries
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('first_name', 45)->nullable()->default(null);
            $table->string('last_name', 45)->nullable()->default(null);
            $table->string('middle_name', 45)->nullable()->default(null);
            $table->string('email', 45)->nullable()->default(null);
            $table->string('phone', 45)->nullable()->default(null);
            $table->string('address', 45)->nullable()->default(null);
            $table->string('subject', 45)->nullable()->default(null);
            $table->string('qualification_level', 45)->nullable()->default(null);
            $table->string('experience', 45)->nullable()->default(null);
            $table->string('country_intrested', 45)->nullable()->default(null);
            $table->unsignedInteger('service_id')->nullable()->default(null);
            $table->string('enquiry_from', 45)->nullable()->default(null);
            $table->string('source', 45)->nullable()->default(null);
            $table->string('remarks', 45)->nullable()->default(null);
            $table->string('responded_through', 45)->nullable()->default(null);
            $table->string('category_id', 45)->nullable()->default(null);

            $table->index(["service_id"], 'Services_idx');

            $table->unique(["id"], 'id_UNIQUE');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
