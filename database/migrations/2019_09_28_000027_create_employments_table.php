<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'employments';

    /**
     * Run the migrations.
     * @table employments
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('issuing_authority_name', 200)->nullable()->default(null);
            $table->string('issuing_authority_address', 200)->nullable()->default(null);
            $table->string('issuing_authority_country', 200)->nullable()->default(null);
            $table->string('issuing_authority_state', 200)->nullable()->default(null);
            $table->string('issuing_authority_city', 200)->nullable()->default(null);
            $table->string('issuing_authority_country_code', 200)->nullable()->default(null);
            $table->string('issuing_authority_phone', 200)->nullable()->default(null);
            $table->string('reason_for_leaving')->nullable()->default(null);
            $table->string('issuing_authority_email', 200)->nullable()->default(null);
            $table->string('issuing_authority_website', 200)->nullable()->default(null);
            $table->string('nature_of_employment', 200)->nullable()->default(null);
            $table->date('employment_from')->nullable()->default(null);
            $table->date('employment_to')->nullable()->default(null);
            $table->string('designation', 200)->nullable()->default(null);
            $table->string('employee_code', 200)->nullable()->default(null);
            $table->string('department', 200)->nullable()->default(null);
            $table->string('experience_letter', 200)->nullable()->default(null);
            $table->unsignedInteger('applicant_id')->nullable()->default(null);

            $table->index(["applicant_id"], 'emp_idx');
            $table->nullableTimestamps();


            $table->foreign('applicant_id', 'emp_idx')
                ->references('id')->on('applicants')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
