<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTypesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'data_types';

    /**
     * Run the migrations.
     * @table data_types
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('slug', 191);
            $table->string('display_name_singular', 191);
            $table->string('display_name_plural', 191);
            $table->string('icon', 191)->nullable()->default(null);
            $table->string('model_name', 191)->nullable()->default(null);
            $table->string('policy_name', 191)->nullable()->default(null);
            $table->string('controller', 191)->nullable()->default(null);
            $table->string('description', 191)->nullable()->default(null);
            $table->tinyInteger('generate_permissions')->default('0');
            $table->tinyInteger('server_side')->default('0');
            $table->text('details')->nullable()->default(null);

            $table->unique(["name"], 'data_types_name_unique');

            $table->unique(["slug"], 'data_types_slug_unique');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
